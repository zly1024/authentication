package com.zly.common.constants;
/**
 * 用户类型
 * @author zly
 * @date
 */
public class UserType {
    /**
     * 商家
     */
    public static final String COMPANY = "C";
    /**
     * 门店
     */
    public static final String STORE = "S";
    /**
     * 门店员工
     */
    public static final String EMP = "EMP";
    /**
     * 普通系统用户
     */
    public static final String USER = "USER";
    /**
     * 普通系统用户-小程序用户
     */
    public static final String APPLET_USER = "APPLET_USER";
    /**
     * 商家注册-临时用户
     */
    public static final String COMP_REGISTER_TEMP_USER = "REG_TEMP";
}
