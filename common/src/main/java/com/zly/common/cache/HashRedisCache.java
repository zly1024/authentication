/*
 * #{copyright}#
 */
package com.zly.common.cache;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisOperations;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * zly
 * @author redis緩存配置類
 */
public class HashRedisCache<T> implements Cache {

    private String name;

    private long expiration;

    private RedisOperations redisOperations;

    public HashRedisCache(String name, RedisOperations redisOperations) {
        this.name = name;
        this.redisOperations = redisOperations;
    }

    public HashRedisCache(String name, RedisOperations redisOperations, long expiration) {
        this.name = name;
        this.redisOperations = redisOperations;
        this.expiration = expiration;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getNativeCache() {
        return this.redisOperations;
    }

    @Override
    public ValueWrapper get(Object key) {
        HashOperations hashOperations = redisOperations.opsForHash();
        Object t = hashOperations.get(this.getName(), key);
        redisOperations.expire(this.name, this.expiration, TimeUnit.SECONDS);
        return new SimpleValueWrapper(t);
    }

    @Override
    public <T> T get(Object key, Class<T> aClass) {
        return (T) get(key).get();
    }

    public List<T> getAll() {
        HashOperations hashOperations = redisOperations.opsForHash();
        Map map = hashOperations.entries(this.getName());
        List<T> result = new ArrayList<>();
        if (map.size() > 0 ) {
            result.addAll(map.values());
        }
        redisOperations.expire(this.name, this.expiration, TimeUnit.SECONDS);
        return  result;
    }

    @Override
    public <T> T get(Object key, Callable<T> callable) {
        return (T) get(key).get();
    }

    @Override
    public void put(Object key, Object value) {
        HashOperations hashOperations = redisOperations.opsForHash();
        hashOperations.put(this.getName(), key, value);
        redisOperations.expire(this.name, this.expiration, TimeUnit.SECONDS);
    }

    public void putAll(Map<Object, Object> values) {
        final byte[] rawKey = redisOperations.getKeySerializer().serialize(this.getName());
        final long expiration = this.expiration;
        redisOperations.executePipelined(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                Set<Object> keySet = values.keySet();
                Iterator<Object> it = keySet.iterator();
                while (it.hasNext()) {
                    Object key = it.next();
                    connection.hSet(rawKey,
                                    redisOperations.getKeySerializer().serialize(key),
                                    redisOperations.getValueSerializer().serialize(values.get(key)));
                }
                connection.expire(rawKey, expiration);
                return null;
            }
        });
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        put(key, value);
        return get(key);
    }

    @Override
    public void evict(Object key) {
        HashOperations hashOperations = redisOperations.opsForHash();
        hashOperations.delete(this.getName(), key);
    }

    @Override
    public void clear() {
        HashOperations hashOperations = redisOperations.opsForHash();
        Set keys = hashOperations.keys(this.getName());
        for(Object key : keys) {
            hashOperations.delete(this.getName(), key);
        }
    }
}
