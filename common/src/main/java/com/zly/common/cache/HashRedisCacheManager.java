/*
 * #{copyright}#
 */
package com.zly.common.cache;

import org.springframework.cache.Cache;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisOperations;

import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Hash结构Redis Cache Manager.
 * @author zly
 */
public class HashRedisCacheManager extends RedisCacheManager {

    private RedisOperations redisOperations;

    private Map<String, Long> expires;

    private long defaultExpiration = 0;

    public HashRedisCacheManager(RedisOperations redisOperations) {
        super(redisOperations);
        this.redisOperations = redisOperations;
    }

    public HashRedisCacheManager(RedisOperations redisOperations, Collection<String> cacheNames) {
        super(redisOperations, cacheNames);
        this.redisOperations = redisOperations;
    }

    @Override
    protected Cache getMissingCache(String name) {
        return new HashRedisCache<>(name, redisOperations, computeExpiration(name));
    }

    @Override
    protected long computeExpiration(String name) {
        final Long[] expiration = {null};
        if (expires != null) {

            if (expires.get(name) != null) {
                expiration[0] = expires.get(name);
            } else {
                expires.forEach((key, value) -> {
                    if (Pattern.compile(key).matcher(name).find()) {
                        expiration[0] = value;
                        return;
                    }
                });
            }
        }
        return (expiration[0] != null ? expiration[0].longValue() : defaultExpiration);
    }

    @Override
    public void setDefaultExpiration(long defaultExpireTime) {
        super.setDefaultExpiration(defaultExpireTime);
        this.defaultExpiration = defaultExpireTime;
    }

    @Override
    public void setExpires(Map<String, Long> expires) {
        super.setExpires(expires);
        this.expires = expires;
    }
}
