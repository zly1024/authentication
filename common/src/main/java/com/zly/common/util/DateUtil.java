package com.zly.common.util;

import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author zly
 */
public class DateUtil {

    public static final String NUMBER_PATTERN = "^[1-9]\\d*$";

    public static final String YMD_PATTERN = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
    public static final String YMDHMS_PATTERN = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}";
    public static final String YMD_PATTERN2 = "[0-9]{4}/[0-9]{2}/[0-9]{2}";
    public static final String YMDHMS_PATTERN2 = "[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}";

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT2 = "yyyy/MM/dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT2 = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_SEQ = "yyyyMMddHHmmss";
    //UTC日期格式相关
    /**
     * 如 2015-12-7T16:00:00Z
     */
    public static final String UTC_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss Z";
    /**
     * 如 2015-12-7T16:00:00.000Z
     */
    public static final String UTC_DATETIME_FORMAT_SSS = "yyyy-MM-dd'T'HH:mm:ss.SSS Z";

    public static String getCurrentDate() {
        return convertDate2Str(new Date(), DATE_FORMAT);
    }

    public static String getCurrentDateTime() {
        return convertDate2Str(new Date(), DATE_TIME_FORMAT);
    }

    public static String getCurrentDateTimeSeq() {
        return convertDate2Str(new Date(), DATE_TIME_FORMAT_SEQ);
    }


    /**
     * 日期=>字符串
     *
     * @param date
     * @param dateFormat
     * @return
     */
    public static String convertDate2Str(Date date, String dateFormat) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat).format(date);
    }

    /**
     * 字符串=>日期
     *
     * @param text
     * @return
     * @throws ParseException
     */
    public static Date convertStr2Date(String text) throws ParseException {
        if (StringUtils.isEmpty(text)) {
            return (Date) null;
        } else if (Pattern.compile(NUMBER_PATTERN).matcher(text).find()) {
            Long value = Long.parseLong(text);
            return new Date(value);
        } else if (Pattern.compile(YMDHMS_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
            return simpleDateFormat.parse(text);
        } else if (Pattern.compile(YMDHMS_PATTERN2).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT2);
            return simpleDateFormat.parse(text);
        } else if (Pattern.compile(YMD_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            return simpleDateFormat.parse(text);
        } else if (Pattern.compile(YMD_PATTERN2).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT2);
            return simpleDateFormat.parse(text);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            return simpleDateFormat.parse(text);
        }
    }

    /**
     * 阿里云令牌:  UTC 时间转 本地时间 +8:00
     *
     * @param utcDate
     * @return
     * @throws ParseException
     */
    public static Date utcDateToLocalDate(String utcDate) throws ParseException {
        if (utcDate == null || "".equals(utcDate)) {
            return null;
        }
        return utcDateToLocalDate(utcDate, UTC_DATETIME_FORMAT);
    }

    public static Date utcDateToLocalDate(String utcDate, String utcFormat) throws ParseException {
        if (utcDate == null || "".equals(utcDate)) {
            return null;
        }
        //注意是空格+UTC
        String dateStr = utcDate.replace("Z", " UTC");
        //注意格式化的表达式
        SimpleDateFormat format = new SimpleDateFormat(utcFormat);
        Date date = format.parse(dateStr);
        return date;
    }

    public static String utcDateToLocalDateStr(String utcDate) throws ParseException {
        if (utcDate == null || "".equals(utcDate)) {
            return null;
        }
        Date date = utcDateToLocalDate(utcDate);
        return convertDate2Str(date, DATE_TIME_FORMAT);
    }

    /**
     * 校验时间段有效性，有效则返回true，否则返回false
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean checkDateEffective(Date startDate, Date endDate) {
        Date now = new Date();

        if (startDate != null && now.before(startDate)) {
            return false;
        }
        if (endDate != null && now.after(endDate)) {
            return false;
        }
        if (startDate != null && endDate != null &&
                startDate.after(endDate)) {
            return false;
        }

        return true;
    }

    /**
     * 获取当天00:00:00的时间
     * @return
     */
    public static Date getNowStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取当天23:59:59的时间
     * @return
     */
    public static Date getNowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * 获得某天最小时间 2017-10-15 00:00:00
     * @param date
     * @return
     */
    public static Date getStartOfDay(Date date) {
        if (date == null) {
            return null;
        }
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
        return Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 获得某天最大时间 2017-10-15 23:59:59
     * @param date
     * @return
     */
    public static Date getEndOfDay(Date date) {
        if (date == null) {
            return null;
        }
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());;
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
        return Date.from(endOfDay.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 获取多少天--->diffDays 前（负数）/后（正数）的日期
     * @param diffDays
     * @return
     */
    public static Date getCurrentDateBefore(int diffDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, diffDays);
        return calendar.getTime();
    }



    // test
//    public static void main(String[] args) {
//        //获取3天前的时间
//        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(sdf1.format(getCurrentDateBefore(-3)));
//        System.out.println(getCurrentDateBefore(-3));
//        System.out.println(getStartOfDay(getCurrentDateBefore(-3)));
//    }

}
