package com.zly.common.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author : zly
 * @date : 2017/3/30.
 * @description :反射帮助类
 */
public class RefectUtil {
    public static <T> T setField(String fieldName, T t, Object value) {
        try {
            Method m = getSetMethod(fieldName, t.getClass());
            m.invoke(t, value);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return t;
    }

    public static Method getSetMethod(String fieldName, Class c) throws NoSuchMethodException {
        return c.getMethod("set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1), getType(fieldName, c));
    }

    public static Class getType(String fileName, Class c) throws NoSuchMethodException {
        return getGetMethod(fileName, c).getReturnType();

    }

    public static Method getGetMethod(String fieldName, Class c) throws NoSuchMethodException {
        return c.getMethod("get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
    }
}
