package com.zly.common.util;

/**
 * @author : zly
 * @date   : 2017/8/25.
 * @description : 校验码工具类
 */
public class CheckNumUtil {

    private final static char[] PWD = new char[]{'0', '2', '1', '8', '7', '6', '4', '3', '9', '5'};

    /**
     * 获得指定位数校验码
     *
     * @param s           源码
     * @param checkNumLen 校验码长度
     * @return 校验码
     */
    public static String createCheckNum(String s, int checkNumLen) {
        StringBuffer sb = new StringBuffer(s);
        StringBuffer result = new StringBuffer();
        int mod = s.length() % checkNumLen;
        int divisor = s.length() / checkNumLen + (mod == 0 ? mod : 1);
        if (mod != 0) {
            for (int i = 0; i < checkNumLen - mod; i++) {
                sb.insert(0, '0');
            }
        }
        for (int i = 0; i < sb.length(); i += divisor) {
            String t = sb.substring(i, i + divisor);
            int sum = 0;
            for (int j = 0; j < t.length(); j++) {
                sum += t.charAt(j);
            }
            result.append(PWD[sum % 10]);
        }
        return result.toString();
    }

    /**
     * 校验校验码
     *
     * @param s        源码
     * @param checkNum 校验码
     * @return boolean true:通过,false:校验不通过
     */
    public static boolean validateCheckNum(String s, String checkNum) {
        return checkNum.equals(createCheckNum(s, checkNum.length()));
    }

    /**
     * 根据来源字符串,直接生成最终码
     *
     * @param s           来源字符串
     * @param numLen      压缩后长度
     * @param checkNumLen 校验码长度
     * @return 压缩后的字符串+校验码(长度为 numLen+checkNumLen)
     */
    public static String createNumWithCheckNum(String s, int numLen, int checkNumLen) {
        String tmp = createCheckNum(s, numLen);
        return tmp + createCheckNum(tmp, checkNumLen);
    }
}
