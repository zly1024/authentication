/*
 * #{copyright}#
 */
package com.zly.common.util;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * @author zly
 */
public class BeanUtil {

    private static Logger logger = LoggerFactory.getLogger(BeanUtil.class);

    public static Map<String, Object> bean2Map(Object bean) {

        Map<String, Object> map = new HashMap<String, Object>(10);

        if(bean == null){
            return map;
        }
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                // 过滤class属性
                if (!"class".equals(key)) {
                    // 得到property对应的getter方法
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(bean);
                    if (value != null) {
                        map.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("bean2Map Error " + e);
        }

        return map;
    }

    public static <T> T map2Bean(Map<String, Object> map, Class<T> targetClass) {
        if (map == null) {
            return null;
        }
        if (Map.class.isAssignableFrom(targetClass)) {
            return (T) map;
        }
        return JSONObject.parseObject(JSONObject.toJSONString(map), targetClass);

    }

    public static <T> T copyBean(Object source, Class<T> targetClass) {

        try {
            T result = targetClass.newInstance();
            BeanUtils.copyProperties(source, result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static List<Field> getAllField(List<Field> fields, Class clazz) {
        List<Field> newField = java.util.Arrays.asList(clazz.getDeclaredFields());
        if (fields == null) {
            fields = new ArrayList<Field>();
        }
        fields.addAll(newField);
        if (clazz.getSuperclass() != null) {
            getAllField(fields, clazz.getSuperclass());
        }
        return fields;
    }
    /**
     * 将第一个对象的非空属性赋予第二个对象
     *
     * @param origin      第一个对象
     * @param destination 第二个对象
     * @param <T>         泛型
     */
    public static <T> void mergeObject(T origin, T destination, String... exclude) {
        if (origin == null || destination == null) {
            return;
        }
        if (!origin.getClass().equals(destination.getClass())) {
            return;
        }
        List<Field> fields = getAllField(new ArrayList<>(), origin.getClass());
        List<String> excludeList = Arrays.asList(exclude);
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            if (excludeList != null && !excludeList.isEmpty()) {
                if (excludeList.contains(field.getName())) {
                    continue;
                }
            }
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            try {
                field.setAccessible(true);
                Object value = field.get(origin);
                if (null != value) {
                    field.set(destination, value);
                }
                field.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static <T> PageInfo<T> pageMap2pageBean(Map<String, Object> pageInfo, Class<T> tClass) {
        PageInfo<T> result = new PageInfo<T>();
        result.setPageNum((int) pageInfo.get("pageNum"));
        result.setPageSize((int) pageInfo.get("pageSize"));
        result.setSize((int) pageInfo.get("size"));
        result.setOrderBy((String) pageInfo.get("orderBy"));
        result.setStartRow((int) pageInfo.get("startRow"));
        result.setEndRow((int) pageInfo.get("endRow"));
        result.setTotal(new Long(pageInfo.get("total").toString()));
        result.setPages((int) pageInfo.get("pages"));
        result.setFirstPage((int) pageInfo.get("firstPage"));
        result.setPrePage((int) pageInfo.get("prePage"));
        result.setNextPage((int) pageInfo.get("nextPage"));
        result.setLastPage((int) pageInfo.get("lastPage"));
        result.setIsFirstPage((boolean) pageInfo.get("isFirstPage"));
        result.setIsLastPage((boolean) pageInfo.get("isLastPage"));
        result.setHasPreviousPage((boolean) pageInfo.get("hasPreviousPage"));
        result.setHasNextPage((boolean) pageInfo.get("hasNextPage"));
        result.setNavigatePages((int) pageInfo.get("navigatePages"));
        List<Integer> navigatepageNums = (List<Integer>) pageInfo.get("navigatepageNums");
        if (navigatepageNums != null && !navigatepageNums.isEmpty()) {
            int[] p = new int[navigatepageNums.size()];
            for (int i = 0; i < navigatepageNums.size(); i++) {
                p[i] = navigatepageNums.get(i);
            }
            result.setNavigatepageNums(p);
        }
        result.setList(listMap2listBean((List<Map<String, Object>>) pageInfo.get("list"), tClass));
        return result;
    }

    public static <T> List<T> listMap2listBean(List<Map<String, Object>> listMap, Class<T> tClass) {
        if (listMap == null || listMap.isEmpty()) {
            return null;
        }
        List<T> result = new ArrayList<T>();
        for (Map<String, Object> map : listMap) {
            result.add(map2Bean(map, tClass));
        }
        return result;
    }

    public static <T> Map<String, Object> bean2UrlMap(T obj) {
        if (obj == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<>(10);
        try {
            List<Field> fields = getAllField(new ArrayList<Field>(), obj.getClass());
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                    continue;
                }
                field.setAccessible(true);
                Object o = field.get(obj);
                if (o == null) {
                    continue;
                }
                Class clazz = field.getType();
                if (clazz.equals(String.class) ||
                        clazz.equals(Integer.class) ||
                        clazz.equals(Byte.class) ||
                        clazz.equals(Long.class) ||
                        clazz.equals(Double.class) ||
                        clazz.equals(Float.class) ||
                        clazz.equals(Character.class) ||
                        clazz.equals(Short.class) ||
                        clazz.equals(BigDecimal.class) ||
                        clazz.equals(BigInteger.class) ||
                        clazz.equals(Boolean.class) || clazz.isPrimitive()) {
                    map.put(field.getName(), o);
                } else if (clazz.equals(Date.class)) {
                    map.put(field.getName(), ((Date) o).getTime());
                } else {
                    //当知道是个对象的时候,将对象内部属性转为Map
                    Map<String, Object> temp = bean2UrlMap(o);
                    Iterator<String> iter = temp.keySet().iterator();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        map.put(field.getName() + "." + key, temp.get(key));
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return map;
    }
}
