/*
 * #{copyright}#
 */
package com.zly.common.util;

import java.math.BigDecimal;

/**
 * 计算类gongju
 * @author zly
 */
public class BigDecimalUtil {

    public static BigDecimal add(BigDecimal...numbers) {
        BigDecimal result = BigDecimal.ZERO;
        for(BigDecimal a : numbers) {
            initBigDecimal(a);
            result = result.add(a);
        }
        return result;
    }

    public static BigDecimal subtract(BigDecimal...numbers) {
        BigDecimal result = null;
        for(BigDecimal a : numbers) {
            initBigDecimal(a);

            if (result == null) {
                result = a;
            } else {
                result = result.subtract(a);
            }
        }
        return result;
    }

    public static BigDecimal multiply(BigDecimal...numbers) {
        BigDecimal result = BigDecimal.ONE;
        for(BigDecimal a : numbers) {
            initBigDecimal(a);
            result = result.multiply(a);
        }
        return result;
    }

    public static BigDecimal divide(BigDecimal...numbers) {
        BigDecimal result = null;
        for(BigDecimal a : numbers) {
            initBigDecimal(a);

            if (result == null) {
                result = a;
            } else {
                result = result.divide(a);
            }
        }
        return result;
    }

    private static BigDecimal initBigDecimal(BigDecimal a) {
        if (a == null) {
            a = BigDecimal.ZERO;
        }
        return a;
    }


}
