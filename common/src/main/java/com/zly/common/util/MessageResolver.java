/*
 * #{copyright}#
 */
package com.zly.common.util;

import org.springframework.context.MessageSource;
import org.springframework.util.StringUtils;

import java.util.Locale;

/**
 * 多语言消息处理.
 *
 * @author zly
 */
public class MessageResolver {


    private MessageSource messageSource;

    private Locale locale;

    public MessageResolver(MessageSource messageSource, Locale locale) {
        this.messageSource = messageSource;
        this.locale = locale;
    }

    public String getMessage(String messageCode, Object[] args) {
        if (!match(messageCode)) {
            return messageCode;
        }
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                String arg = args[i].toString();
                if (match(arg.toString())) {
                    arg = messageSource.getMessage(arg, new Object[]{}, locale);
                    args[i] = arg;
                }
            }
        }

        String msg = messageSource.getMessage(messageCode, args, locale);
        return StringUtils.isEmpty(msg) ? messageCode : msg;
    }


    private boolean match(String messageCode) {
        return messageCode.startsWith("msg.") || messageCode.startsWith("label") || messageCode.startsWith("btn");
    }
}
