/*
 * #{copyright}#
 */
package com.zly.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * @author zly
 */
public class SimpleDateEditor extends PropertyEditorSupport {
    private static final Logger logger = LoggerFactory.getLogger(SimpleDateEditor.class);
    public static final String NUMBER_PATTERN = "^[1-9]\\d*$";

    /**
     * YYYY-MM-dd
     */
    public static final String YMD_PATTERN = "[0-9]{4}-[0-9]{2}-[0-9]{2}";

    public static final String YMD_FORMAT = "yyyy-MM-dd";

    /**
     * YYYY-MM-dd HH:mm:ss
     */
    public static final String YMDHMS_PATTERN = "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}";

    public static final String YMDHMS_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * YYYY/MM/dd
     */
    public static final String YMD_PATTERN2 = "[0-9]{4}/[0-9]{2}/[0-9]{2}";

    public static final String YMD_FORMAT2 = "yyyy/MM/dd";

    /**
     * YYYY/MM/dd HH:mm:ss
     */
    public static final String YMDHMS_PATTERN2 = "[0-9]{4}/[0-9]{2}/[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}";

    public static final String YMDHMS_FORMAT2 = "yyyy/MM/dd HH:mm:ss";

    /**
     * [Tue Dec 05 23:59:59 GMT+08:00 2017]
     */
    public static final String GMT_PATTERN = "[a-zA-Z]{3} [a-zA-Z]{3} [0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} GMT[+-]{1}[0-9]{2}:[0-9]{2} [0-9]{4}";

    public static final String GMT_FORMAT = "EEE MMM dd HH:mm:ss zzz yyyy";

    /**
     * [Sat Dec 16 16:16:16 CST 2017]
     */
    public static final String CST_PATTERN = "[a-zA-Z]{3} [a-zA-Z]{3} [0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} CST [0-9]{4}";

    public static final String CST_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";


    @Override
    public String getAsText() {
        Date value = (Date) this.getValue();
        return value.getTime() + "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        logger.debug("text  is {}", text);
        if (StringUtils.isEmpty(text)) {
            logger.debug("进入分支 :{}", "isEmpty");
            this.setValue((Object) null);
        } else if (Pattern.compile(NUMBER_PATTERN).matcher(text).find()) {
            Long value = Long.parseLong(text);
            logger.debug("进入分支 :{} long value is {}", NUMBER_PATTERN, value);
            this.setValue(new Date(value));
        } else if (Pattern.compile(YMDHMS_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YMDHMS_FORMAT);
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", YMDHMS_PATTERN, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Pattern.compile(YMDHMS_PATTERN2).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YMDHMS_FORMAT2);
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", YMDHMS_PATTERN2, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Pattern.compile(YMD_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YMD_FORMAT);
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", YMD_PATTERN, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (Pattern.compile(YMD_PATTERN2).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YMD_FORMAT2);
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", YMD_FORMAT2, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (Pattern.compile(GMT_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GMT_FORMAT, Locale.US);
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", GMT_PATTERN, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (Pattern.compile(CST_PATTERN).matcher(text).find()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CST_FORMAT, Locale.US);  //参数传递的时候报错 java.text.ParseException: Unparseable date: "Fri Mar 23 00:00:00 CST 2018"
            try {
                Date value = simpleDateFormat.parse(text);
                logger.debug("进入分支 :{} Date value is {}", CST_FORMAT, value);
                this.setValue(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            logger.debug("进入分支 :{}  text is {}", "else", text);
            this.setValue(text);
        }

    }


//    //test case
//    public static void main(String[] args) {
//        String text = "Tue Dec 05 23:59:59 GMT+08:00 2017";
//        if (Pattern.compile(GMT_PATTERN).matcher(text).find()) {
//            System.out.println("matched");
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(GMT_FORMAT, Locale.US);
//            try {
//                Date value = simpleDateFormat.parse(text);
//
//                System.out.println(value);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//
//        //CST
//        String text1 = "Tue Dec 05 21:55:54 CST 2017";
//        if (Pattern.compile(CST_PATTERN).matcher(text1).find()) {
//            System.out.println("matched CST");
//
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CST_FORMAT, Locale.US);
//            try {
//                Date value1 = simpleDateFormat.parse(text1);
//
//                System.out.println(value1);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//    }

}
