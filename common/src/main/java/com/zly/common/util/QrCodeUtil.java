package com.zly.common.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : zly
 * @date : 2017/10/17.
 * @description :
 */
public class QrCodeUtil {
    private static final int QR_COLOR = 0xFF000000;
    private static final int BG_COLOR = 0xFFFFFFFF;

    private static final Logger logger = LoggerFactory.getLogger(QrCodeUtil.class);

    /**
     * @param url         完整Url
     * @param logo        logo 图片输入流
     * @param showContent 下方显示文字
     * @param size        大小
     * @return
     */
    public static String generalQRCode(String url, BufferedImage logo, String showContent, int size) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        // 参数顺序分别为：编码内容，编码类型，生成图片宽度，生成图片高度，设置参数
        BitMatrix bm = null;
        try {
            bm = multiFormatWriter.encode(url, BarcodeFormat.QR_CODE, size, size, getDecodeHintType());
        } catch (WriterException e) {
            e.printStackTrace();
        }
        BufferedImage image = new BufferedImage(bm.getWidth(), bm.getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < bm.getWidth(); x++) {
            for (int y = 0; y < bm.getHeight(); y++) {
                image.setRGB(x, y, bm.get(x, y) ? QR_COLOR : BG_COLOR);
            }
        }
        return addLogoQRCode(image, logo, showContent);
    }

    /**
     * 设置二维码的格式参数
     *
     * @return
     */
    public static Map<EncodeHintType, Object> getDecodeHintType() {
        // 用于设置QR二维码参数
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>(5);
        // 设置QR二维码的纠错级别（H为最高级别）具体级别信息
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        // 设置编码方式
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.MARGIN, 0);
        hints.put(EncodeHintType.MAX_SIZE, 350);
        hints.put(EncodeHintType.MIN_SIZE, 100);
        return hints;
    }


    /**
     * 给二维码图片添加Logo
     *
     * @param bim
     * @param showContent
     * @return
     */
    public static String addLogoQRCode(BufferedImage bim, BufferedImage logo, String showContent) {
        try {
            //读取二维码图片，并构建绘图对象
            BufferedImage image = bim;
            Graphics2D g = image.createGraphics();
            //设置logo的大小,设置为二维码图片的30%,因为过大会盖掉二维码
            int widthLogo = logo.getWidth(null) > image.getWidth() * 3 / 10 ? (image.getWidth() * 3 / 10) : logo.getWidth(null),
                    heightLogo = logo.getHeight(null) > image.getHeight() * 3 / 10 ? (image.getHeight() * 3 / 10) : logo.getWidth(null);
            //logo放在中心
            int x = (image.getWidth() - widthLogo) / 2;
            int y = (image.getHeight() - heightLogo) / 2;
            //开始绘制图片
            g.drawImage(logo, x, y, widthLogo, heightLogo, null);
            g.dispose();
            //把显示内容添加上去，显示不要太长，这里最多支持两行。太长就会自动截取
            if (!StringUtils.isEmpty(showContent)) {
                //新的图片，把带logo的二维码下面加上文字
                BufferedImage outImage = new BufferedImage(image.getWidth(), image.getHeight() + 45, BufferedImage.TYPE_4BYTE_ABGR);
                Graphics graphics = outImage.getGraphics();
                //画二维码到新的面板
                graphics.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
                //画文字到新的面板
                graphics.setColor(Color.BLACK);
                //字体、字型、字号
                Font font = new Font("宋体", Font.BOLD, 15);
                graphics.setFont(font);
                int strWidth = graphics.getFontMetrics(font).stringWidth(showContent);
                int two = 2;
                if (strWidth > (image.getWidth() / two - 1)) {
                    //长度过长就截取前面部分
                    String showContent1 = showContent.substring(0, showContent.length() / 2);
                    String showContent2 = showContent.substring(showContent.length() / 2, showContent.length());
                    graphics.drawString(
                            showContent1, (image.getWidth() - strWidth) / 2,
                            image.getHeight() + (outImage.getHeight() - image.getHeight()) / 2 + 12
                    );
                    BufferedImage outImage2 = new BufferedImage(image.getWidth(), image.getHeight() + 85, BufferedImage.TYPE_4BYTE_ABGR);
                    Graphics graphics2 = outImage2.getGraphics();
                    graphics2.drawImage(outImage, 0, 0, outImage.getWidth(), outImage.getHeight(), null);
                    graphics2.setColor(Color.BLACK);
                    //字体、字型、字号
                    graphics2.setFont(new Font("宋体", Font.BOLD, 30));
                    graphics2.drawString(showContent2, (image.getWidth() - strWidth) / 2, outImage.getHeight() + (outImage2.getHeight() - outImage.getHeight()) / 2 + 5);
                    graphics2.dispose();
                    outImage2.flush();
                    outImage = outImage2;
                } else {
                    //画文字
                    graphics.drawString(showContent, (image.getWidth() - strWidth) / 2, image.getHeight() + (outImage.getHeight() - image.getHeight()) / 2 + 12);
                }
                graphics.dispose();
                outImage.flush();
                image = outImage;
            }
            logo.flush();
            image.flush();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.flush();
            ImageIO.write(image, "png", byteArrayOutputStream);
            //返回这个二维码的imageBase64字符串
            BASE64Encoder encoder = new BASE64Encoder();
            String imageBase64QRCode = encoder.encodeBuffer(byteArrayOutputStream.toByteArray()).trim();
            byteArrayOutputStream.close();
            return imageBase64QRCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
