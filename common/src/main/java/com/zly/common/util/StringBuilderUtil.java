/*
 * #{copyright}#
 */
package com.zly.common.util;

/**
 * @author zly
 */
public class StringBuilderUtil {

    public static final String EMPTY_STRING = "";

    public static String append(String...strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for(String string : strings) {
            if(string != null) {
                stringBuilder.append(string);
            }
        }
        return stringBuilder.toString();
    }

    public static String appendWithSplitor(String splitor, String...strings) {
        if(splitor == null) {
            splitor = EMPTY_STRING;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for(String string : strings) {
            stringBuilder.append(string).append(splitor);
        }
        return stringBuilder.substring(0,stringBuilder.length() - splitor.length());
    }
}
