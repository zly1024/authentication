/*
 * #{copyright}#
 */
package com.zly.common.util;

/**
 * @author zly
 */
public class RandomUtil {
    /**
     * 默认验证码长度为4位
     */
    private static final int DEFAULT_RANDOMLEN = 4;

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int TEN = 10;

    public RandomUtil() {
    }


    /**
     * 生成4位纯数字随机数（用于做验证码）
     *
     * @param len
     * @return
     */
    public static String generateRandom(Integer len) {
        if (len == null) {
            len = DEFAULT_RANDOMLEN;
        }
        //产生1000-9999的随机数
        int rand1 = (int) (Math.random() * (9999 - 1000 + 1)) + 1000;
        //产生1000-9999的随机数
        int rand2 = (int) (Math.random() * (9999 - 1000 + 1)) + 1000;
        int rand = (rand1 * rand2);
        // 字符串反转
        return reverseByCharBuffer(Integer.toString(rand)).substring(0, len);
    }


    /**
     * 字符串反转（扰乱字符串的顺序，让随机数更安全）
     *
     * @param original
     * @return
     */
    public static String reverseByCharBuffer(String original) {
        char[] c = original.toCharArray();
        int len = original.length();
        for (int i = 0; i < len / TWO; i++) {
            char t = c[i];
            c[i] = c[len - i - 1];
            c[len - i - 1] = t;
        }
        return new String(c);
    }

    /**
     * 随机字符串的随机字符库
     */
    private static final String KEY_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * 获取指定位数的随机字符串(包含小写字母、大写字母、数字, length>0)
     *
     * @param length
     * @return
     */
    public static String getRandomString(int length) {
        if (length <= 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        int len = KEY_STRING.length();
        for (int i = 0; i < length; i++) {
            sb.append(KEY_STRING.charAt((int) Math.round(Math.random() * (len - 1))));
        }
        return sb.toString();
    }

    /**
     * 随机数字的随机数字库
     */
    private static final String KEY_NUMBER = "0123456789";

    /**
     * 获取指定位数的随机数字字符串(包含小写字母、大写字母、数字, length>0)
     *
     * @param length
     * @return
     */
    public static String getRandomNumber(int length) {
        if (length <= 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        int len = KEY_NUMBER.length();
        for (int i = 0; i < length; i++) {
            sb.append(KEY_NUMBER.charAt((int) Math.round(Math.random() * (len - 1))));
        }
        return sb.toString();
    }


    // TEST
    public static void main(String[] args) {
        System.out.println("Test===>" + RandomUtil.getRandomNumber(10));
    }

}
