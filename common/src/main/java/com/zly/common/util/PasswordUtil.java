/*
 * #{copyright}#
 */
package com.zly.common.util;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

/**
 * @author zly
 */
public class PasswordUtil {

    private static Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();

    public static String encodePassword(String password, String salt) {
        return passwordEncoder.encodePassword(password, salt);
    }

}
