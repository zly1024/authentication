package com.zly.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author zly
 * @date 2017/4/13.
 */
public class HashUtils {

    final private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    /**
     * 摘要算法: MD5, SHA-1, SHA-256, SHA-512
     */
    private static final String DEFAULT_HASH_TYPE = "SHA-256";
    private static final String DEFAULT_CHAR_SET = "UTF8";

    public static String getHash(String source) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return getHash(source, DEFAULT_HASH_TYPE);
    }

    public static String getMD5(String source) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return getHash(source, "MD5");
    }

    public static String getHash(String source, String hashType) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest messageDigest = MessageDigest.getInstance(hashType);
        //添加要进行计算摘要的信息
        messageDigest.update(source.getBytes(DEFAULT_CHAR_SET));
        //计算摘要
        byte[] digest = messageDigest.digest();

        return bytesToHex(digest);
    }

    /**
     * 转16进制
     *
     * @param bytes
     * @return
     */
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
