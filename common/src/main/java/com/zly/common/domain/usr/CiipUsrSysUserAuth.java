package com.zly.common.domain.usr;
import com.zly.common.domain.BaseDomain;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * ciip_usr.ciip_usr_sys_user_auth
**/
public class CiipUsrSysUserAuth extends BaseDomain { 
    /**
     * <pre>
     * 用户ID
     * 表字段 : ciip_usr_sys_user_auth.user_id
     * </pre>
     */
    @NotNull
    @Max(50)
    private String userId;

    /**
     * <pre>
     * 权限来源
     * 表字段 : ciip_usr_sys_user_auth.source_ref_code
     * </pre>
     */
    @NotNull
    @Max(30)
    private String sourceRefCode;

    /**
     * <pre>
     * 权限来源ID
     * 表字段 : ciip_usr_sys_user_auth.source_ref_id
     * </pre>
     */
    @NotNull
    @Max(50)
    private String sourceRefId;

    /**
     * <pre>
     * 是否排除项（黑名单）
     * 表字段 : ciip_usr_sys_user_auth.is_exclude_flag
     * </pre>
     */
    @NotNull
    @Max(1)
    private String isExcludeFlag;

    /**
     * <pre>
     * 所属模块
     * 表字段 : ciip_usr_sys_user_auth.application_module
     * </pre>
     */
    @Max(30)
    private String applicationModule;

    /**
     * <pre>
     * 来源系统ID
     * 表字段 : ciip_usr_sys_user_auth.application_id
     * </pre>
     */
    @Max(50)
    private String applicationId;

    /**
     * <pre>
     * 启用
     * 表字段 : ciip_usr_sys_user_auth.enabled_flag
     * </pre>
     */
    @NotNull
    @Max(1)
    private String enabledFlag;

    /**
     * <pre>
     * 生效日期
     * 表字段 : ciip_usr_sys_user_auth.start_date
     * </pre>
     */
    @NotNull
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startDate;

    /**
     * <pre>
     * 失效日期
     * 表字段 : ciip_usr_sys_user_auth.end_date
     * </pre>
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    /**
     * <pre>
     * 描述
     * 表字段 : ciip_usr_sys_user_auth.description
     * </pre>
     */
    @Max(240)
    private String description;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute_catrgory
     * </pre>
     */
    @Max(30)
    private String attributeCatrgory;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute1
     * </pre>
     */
    @Max(240)
    private String attribute1;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute2
     * </pre>
     */
    @Max(240)
    private String attribute2;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute3
     * </pre>
     */
    @Max(240)
    private String attribute3;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute4
     * </pre>
     */
    @Max(240)
    private String attribute4;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute5
     * </pre>
     */
    @Max(240)
    private String attribute5;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute6
     * </pre>
     */
    @Max(240)
    private String attribute6;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute7
     * </pre>
     */
    @Max(240)
    private String attribute7;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute8
     * </pre>
     */
    @Max(240)
    private String attribute8;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute9
     * </pre>
     */
    @Max(240)
    private String attribute9;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute10
     * </pre>
     */
    @Max(240)
    private String attribute10;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute11
     * </pre>
     */
    @Max(240)
    private String attribute11;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute12
     * </pre>
     */
    @Max(240)
    private String attribute12;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute13
     * </pre>
     */
    @Max(240)
    private String attribute13;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute14
     * </pre>
     */
    @Max(240)
    private String attribute14;

    /**
     * <pre>
     * 
     * 表字段 : ciip_usr_sys_user_auth.attribute15
     * </pre>
     */
    @Max(240)
    private String attribute15;

    /**
     * <pre>
     * 获取：用户ID
     * 表字段：ciip_usr_sys_user_auth.user_id
     * </pre>
     *
     * @return userId
     * 
     * ciip_usr_sys_user_auth.user_id:用户ID
     */
    public String getUserId() { 
        return userId;
    }

    /**
     * <pre>
     * 设置: 用户ID
     * 表字段：ciip_usr_sys_user_auth.user_id
     * </pre>
     *
     * @param userId ciip_usr_sys_user_auth.user_id:用户ID
     */
    public void setUserId(String userId) { 
        this.userId = userId == null ? null : userId.trim();
    }
    /**
     * <pre>
     * 获取：权限来源
     * 表字段：ciip_usr_sys_user_auth.source_ref_code
     * </pre>
     *
     * @return sourceRefCode
     * 
     * ciip_usr_sys_user_auth.source_ref_code:权限来源
     */
    public String getSourceRefCode() { 
        return sourceRefCode;
    }

    /**
     * <pre>
     * 设置: 权限来源
     * 表字段：ciip_usr_sys_user_auth.source_ref_code
     * </pre>
     *
     * @param sourceRefCode ciip_usr_sys_user_auth.source_ref_code:权限来源
     */
    public void setSourceRefCode(String sourceRefCode) { 
        this.sourceRefCode = sourceRefCode == null ? null : sourceRefCode.trim();
    }
    /**
     * <pre>
     * 获取：权限来源ID
     * 表字段：ciip_usr_sys_user_auth.source_ref_id
     * </pre>
     *
     * @return sourceRefId
     * 
     * ciip_usr_sys_user_auth.source_ref_id:权限来源ID
     */
    public String getSourceRefId() { 
        return sourceRefId;
    }

    /**
     * <pre>
     * 设置: 权限来源ID
     * 表字段：ciip_usr_sys_user_auth.source_ref_id
     * </pre>
     *
     * @param sourceRefId ciip_usr_sys_user_auth.source_ref_id:权限来源ID
     */
    public void setSourceRefId(String sourceRefId) { 
        this.sourceRefId = sourceRefId == null ? null : sourceRefId.trim();
    }
    /**
     * <pre>
     * 获取：是否排除项（黑名单）
     * 表字段：ciip_usr_sys_user_auth.is_exclude_flag
     * </pre>
     *
     * @return isExcludeFlag
     * 
     * ciip_usr_sys_user_auth.is_exclude_flag:是否排除项（黑名单）
     */
    public String getIsExcludeFlag() { 
        return isExcludeFlag;
    }

    /**
     * <pre>
     * 设置: 是否排除项（黑名单）
     * 表字段：ciip_usr_sys_user_auth.is_exclude_flag
     * </pre>
     *
     * @param isExcludeFlag ciip_usr_sys_user_auth.is_exclude_flag:是否排除项（黑名单）
     */
    public void setIsExcludeFlag(String isExcludeFlag) { 
        this.isExcludeFlag = isExcludeFlag == null ? null : isExcludeFlag.trim();
    }
    /**
     * <pre>
     * 获取：所属模块
     * 表字段：ciip_usr_sys_user_auth.application_module
     * </pre>
     *
     * @return applicationModule
     * 
     * ciip_usr_sys_user_auth.application_module:所属模块
     */
    public String getApplicationModule() { 
        return applicationModule;
    }

    /**
     * <pre>
     * 设置: 所属模块
     * 表字段：ciip_usr_sys_user_auth.application_module
     * </pre>
     *
     * @param applicationModule ciip_usr_sys_user_auth.application_module:所属模块
     */
    public void setApplicationModule(String applicationModule) { 
        this.applicationModule = applicationModule == null ? null : applicationModule.trim();
    }
    /**
     * <pre>
     * 获取：来源系统ID
     * 表字段：ciip_usr_sys_user_auth.application_id
     * </pre>
     *
     * @return applicationId
     * 
     * ciip_usr_sys_user_auth.application_id:来源系统ID
     */
    public String getApplicationId() { 
        return applicationId;
    }

    /**
     * <pre>
     * 设置: 来源系统ID
     * 表字段：ciip_usr_sys_user_auth.application_id
     * </pre>
     *
     * @param applicationId ciip_usr_sys_user_auth.application_id:来源系统ID
     */
    public void setApplicationId(String applicationId) { 
        this.applicationId = applicationId == null ? null : applicationId.trim();
    }
    /**
     * <pre>
     * 获取：启用
     * 表字段：ciip_usr_sys_user_auth.enabled_flag
     * </pre>
     *
     * @return enabledFlag
     * 
     * ciip_usr_sys_user_auth.enabled_flag:启用
     */
    public String getEnabledFlag() { 
        return enabledFlag;
    }

    /**
     * <pre>
     * 设置: 启用
     * 表字段：ciip_usr_sys_user_auth.enabled_flag
     * </pre>
     *
     * @param enabledFlag ciip_usr_sys_user_auth.enabled_flag:启用
     */
    public void setEnabledFlag(String enabledFlag) { 
        this.enabledFlag = enabledFlag == null ? null : enabledFlag.trim();
    }
    /**
     * <pre>
     * 获取：生效日期
     * 表字段：ciip_usr_sys_user_auth.start_date
     * </pre>
     *
     * @return startDate
     * 
     * ciip_usr_sys_user_auth.start_date:生效日期
     */
    public Date getStartDate() { 
        return startDate;
    }

    /**
     * <pre>
     * 设置: 生效日期
     * 表字段：ciip_usr_sys_user_auth.start_date
     * </pre>
     *
     * @param startDate ciip_usr_sys_user_auth.start_date:生效日期
     */
    public void setStartDate(Date startDate) { 
        this.startDate = startDate;
    }
    /**
     * <pre>
     * 获取：失效日期
     * 表字段：ciip_usr_sys_user_auth.end_date
     * </pre>
     *
     * @return endDate
     * 
     * ciip_usr_sys_user_auth.end_date:失效日期
     */
    public Date getEndDate() { 
        return endDate;
    }

    /**
     * <pre>
     * 设置: 失效日期
     * 表字段：ciip_usr_sys_user_auth.end_date
     * </pre>
     *
     * @param endDate ciip_usr_sys_user_auth.end_date:失效日期
     */
    public void setEndDate(Date endDate) { 
        this.endDate = endDate;
    }
    /**
     * <pre>
     * 获取：描述
     * 表字段：ciip_usr_sys_user_auth.description
     * </pre>
     *
     * @return description
     * 
     * ciip_usr_sys_user_auth.description:描述
     */
    public String getDescription() { 
        return description;
    }

    /**
     * <pre>
     * 设置: 描述
     * 表字段：ciip_usr_sys_user_auth.description
     * </pre>
     *
     * @param description ciip_usr_sys_user_auth.description:描述
     */
    public void setDescription(String description) { 
        this.description = description == null ? null : description.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute_catrgory
     * </pre>
     *
     * @return attributeCatrgory
     * 
     * ciip_usr_sys_user_auth.attribute_catrgory:
     */
    public String getAttributeCatrgory() { 
        return attributeCatrgory;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute_catrgory
     * </pre>
     *
     * @param attributeCatrgory ciip_usr_sys_user_auth.attribute_catrgory:
     */
    public void setAttributeCatrgory(String attributeCatrgory) { 
        this.attributeCatrgory = attributeCatrgory == null ? null : attributeCatrgory.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute1
     * </pre>
     *
     * @return attribute1
     * 
     * ciip_usr_sys_user_auth.attribute1:
     */
    public String getAttribute1() { 
        return attribute1;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute1
     * </pre>
     *
     * @param attribute1 ciip_usr_sys_user_auth.attribute1:
     */
    public void setAttribute1(String attribute1) { 
        this.attribute1 = attribute1 == null ? null : attribute1.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute2
     * </pre>
     *
     * @return attribute2
     * 
     * ciip_usr_sys_user_auth.attribute2:
     */
    public String getAttribute2() { 
        return attribute2;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute2
     * </pre>
     *
     * @param attribute2 ciip_usr_sys_user_auth.attribute2:
     */
    public void setAttribute2(String attribute2) { 
        this.attribute2 = attribute2 == null ? null : attribute2.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute3
     * </pre>
     *
     * @return attribute3
     * 
     * ciip_usr_sys_user_auth.attribute3:
     */
    public String getAttribute3() { 
        return attribute3;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute3
     * </pre>
     *
     * @param attribute3 ciip_usr_sys_user_auth.attribute3:
     */
    public void setAttribute3(String attribute3) { 
        this.attribute3 = attribute3 == null ? null : attribute3.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute4
     * </pre>
     *
     * @return attribute4
     * 
     * ciip_usr_sys_user_auth.attribute4:
     */
    public String getAttribute4() { 
        return attribute4;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute4
     * </pre>
     *
     * @param attribute4 ciip_usr_sys_user_auth.attribute4:
     */
    public void setAttribute4(String attribute4) { 
        this.attribute4 = attribute4 == null ? null : attribute4.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute5
     * </pre>
     *
     * @return attribute5
     * 
     * ciip_usr_sys_user_auth.attribute5:
     */
    public String getAttribute5() { 
        return attribute5;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute5
     * </pre>
     *
     * @param attribute5 ciip_usr_sys_user_auth.attribute5:
     */
    public void setAttribute5(String attribute5) { 
        this.attribute5 = attribute5 == null ? null : attribute5.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute6
     * </pre>
     *
     * @return attribute6
     * 
     * ciip_usr_sys_user_auth.attribute6:
     */
    public String getAttribute6() { 
        return attribute6;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute6
     * </pre>
     *
     * @param attribute6 ciip_usr_sys_user_auth.attribute6:
     */
    public void setAttribute6(String attribute6) { 
        this.attribute6 = attribute6 == null ? null : attribute6.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute7
     * </pre>
     *
     * @return attribute7
     * 
     * ciip_usr_sys_user_auth.attribute7:
     */
    public String getAttribute7() { 
        return attribute7;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute7
     * </pre>
     *
     * @param attribute7 ciip_usr_sys_user_auth.attribute7:
     */
    public void setAttribute7(String attribute7) { 
        this.attribute7 = attribute7 == null ? null : attribute7.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute8
     * </pre>
     *
     * @return attribute8
     * 
     * ciip_usr_sys_user_auth.attribute8:
     */
    public String getAttribute8() { 
        return attribute8;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute8
     * </pre>
     *
     * @param attribute8 ciip_usr_sys_user_auth.attribute8:
     */
    public void setAttribute8(String attribute8) { 
        this.attribute8 = attribute8 == null ? null : attribute8.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute9
     * </pre>
     *
     * @return attribute9
     * 
     * ciip_usr_sys_user_auth.attribute9:
     */
    public String getAttribute9() { 
        return attribute9;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute9
     * </pre>
     *
     * @param attribute9 ciip_usr_sys_user_auth.attribute9:
     */
    public void setAttribute9(String attribute9) { 
        this.attribute9 = attribute9 == null ? null : attribute9.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute10
     * </pre>
     *
     * @return attribute10
     * 
     * ciip_usr_sys_user_auth.attribute10:
     */
    public String getAttribute10() { 
        return attribute10;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute10
     * </pre>
     *
     * @param attribute10 ciip_usr_sys_user_auth.attribute10:
     */
    public void setAttribute10(String attribute10) { 
        this.attribute10 = attribute10 == null ? null : attribute10.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute11
     * </pre>
     *
     * @return attribute11
     * 
     * ciip_usr_sys_user_auth.attribute11:
     */
    public String getAttribute11() { 
        return attribute11;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute11
     * </pre>
     *
     * @param attribute11 ciip_usr_sys_user_auth.attribute11:
     */
    public void setAttribute11(String attribute11) { 
        this.attribute11 = attribute11 == null ? null : attribute11.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute12
     * </pre>
     *
     * @return attribute12
     * 
     * ciip_usr_sys_user_auth.attribute12:
     */
    public String getAttribute12() { 
        return attribute12;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute12
     * </pre>
     *
     * @param attribute12 ciip_usr_sys_user_auth.attribute12:
     */
    public void setAttribute12(String attribute12) { 
        this.attribute12 = attribute12 == null ? null : attribute12.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute13
     * </pre>
     *
     * @return attribute13
     * 
     * ciip_usr_sys_user_auth.attribute13:
     */
    public String getAttribute13() { 
        return attribute13;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute13
     * </pre>
     *
     * @param attribute13 ciip_usr_sys_user_auth.attribute13:
     */
    public void setAttribute13(String attribute13) { 
        this.attribute13 = attribute13 == null ? null : attribute13.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute14
     * </pre>
     *
     * @return attribute14
     * 
     * ciip_usr_sys_user_auth.attribute14:
     */
    public String getAttribute14() { 
        return attribute14;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute14
     * </pre>
     *
     * @param attribute14 ciip_usr_sys_user_auth.attribute14:
     */
    public void setAttribute14(String attribute14) { 
        this.attribute14 = attribute14 == null ? null : attribute14.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_sys_user_auth.attribute15
     * </pre>
     *
     * @return attribute15
     * 
     * ciip_usr_sys_user_auth.attribute15:
     */
    public String getAttribute15() { 
        return attribute15;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_usr_sys_user_auth.attribute15
     * </pre>
     *
     * @param attribute15 ciip_usr_sys_user_auth.attribute15:
     */
    public void setAttribute15(String attribute15) { 
        this.attribute15 = attribute15 == null ? null : attribute15.trim();
    }

}