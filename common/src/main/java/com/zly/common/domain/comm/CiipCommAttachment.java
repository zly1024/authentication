package com.zly.common.domain.comm;
import com.zly.common.domain.BaseDomain;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * ciip_comm.ciip_comm_attachment
**/
public class CiipCommAttachment extends BaseDomain {
    /**
     * <pre>
     * 消息ID
     * 表字段 : ciip_comm_attachment.message_id
     * </pre>
     */
    @NotNull
    @Max(50)
    private String messageId;

    /**
     * <pre>
     * 附件名称
     * 表字段 : ciip_comm_attachment.attachment_name
     * </pre>
     */
    @NotNull
    @Max(255)
    private String attachmentName;

    /**
     * <pre>
     * 附件地址
     * 表字段 : ciip_comm_attachment.attachment_url
     * </pre>
     */
    @NotNull
    @Max(1000)
    private String attachmentUrl;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute_catrgory
     * </pre>
     */
    @Max(30)
    private String attributeCatrgory;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute1
     * </pre>
     */
    @Max(240)
    private String attribute1;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute2
     * </pre>
     */
    @Max(240)
    private String attribute2;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute3
     * </pre>
     */
    @Max(240)
    private String attribute3;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute4
     * </pre>
     */
    @Max(240)
    private String attribute4;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute5
     * </pre>
     */
    @Max(240)
    private String attribute5;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute6
     * </pre>
     */
    @Max(240)
    private String attribute6;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute7
     * </pre>
     */
    @Max(240)
    private String attribute7;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute8
     * </pre>
     */
    @Max(240)
    private String attribute8;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute9
     * </pre>
     */
    @Max(240)
    private String attribute9;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute10
     * </pre>
     */
    @Max(240)
    private String attribute10;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute11
     * </pre>
     */
    @Max(240)
    private String attribute11;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute12
     * </pre>
     */
    @Max(240)
    private String attribute12;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute13
     * </pre>
     */
    @Max(240)
    private String attribute13;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute14
     * </pre>
     */
    @Max(240)
    private String attribute14;

    /**
     * <pre>
     * 
     * 表字段 : ciip_comm_attachment.attribute15
     * </pre>
     */
    @Max(240)
    private String attribute15;

    /**
     * <pre>
     * 获取：消息ID
     * 表字段：ciip_comm_attachment.message_id
     * </pre>
     *
     * @return messageId
     * 
     * ciip_comm_attachment.message_id:消息ID
     */
    public String getMessageId() { 
        return messageId;
    }

    /**
     * <pre>
     * 设置: 消息ID
     * 表字段：ciip_comm_attachment.message_id
     * </pre>
     *
     * @param messageId ciip_comm_attachment.message_id:消息ID
     */
    public void setMessageId(String messageId) { 
        this.messageId = messageId == null ? null : messageId.trim();
    }
    /**
     * <pre>
     * 获取：附件名称
     * 表字段：ciip_comm_attachment.attachment_name
     * </pre>
     *
     * @return attachmentName
     * 
     * ciip_comm_attachment.attachment_name:附件名称
     */
    public String getAttachmentName() { 
        return attachmentName;
    }

    /**
     * <pre>
     * 设置: 附件名称
     * 表字段：ciip_comm_attachment.attachment_name
     * </pre>
     *
     * @param attachmentName ciip_comm_attachment.attachment_name:附件名称
     */
    public void setAttachmentName(String attachmentName) { 
        this.attachmentName = attachmentName == null ? null : attachmentName.trim();
    }
    /**
     * <pre>
     * 获取：附件地址
     * 表字段：ciip_comm_attachment.attachment_url
     * </pre>
     *
     * @return attachmentUrl
     * 
     * ciip_comm_attachment.attachment_url:附件地址
     */
    public String getAttachmentUrl() { 
        return attachmentUrl;
    }

    /**
     * <pre>
     * 设置: 附件地址
     * 表字段：ciip_comm_attachment.attachment_url
     * </pre>
     *
     * @param attachmentUrl ciip_comm_attachment.attachment_url:附件地址
     */
    public void setAttachmentUrl(String attachmentUrl) { 
        this.attachmentUrl = attachmentUrl == null ? null : attachmentUrl.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute_catrgory
     * </pre>
     *
     * @return attributeCatrgory
     * 
     * ciip_comm_attachment.attribute_catrgory:
     */
    public String getAttributeCatrgory() { 
        return attributeCatrgory;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute_catrgory
     * </pre>
     *
     * @param attributeCatrgory ciip_comm_attachment.attribute_catrgory:
     */
    public void setAttributeCatrgory(String attributeCatrgory) { 
        this.attributeCatrgory = attributeCatrgory == null ? null : attributeCatrgory.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute1
     * </pre>
     *
     * @return attribute1
     * 
     * ciip_comm_attachment.attribute1:
     */
    public String getAttribute1() { 
        return attribute1;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute1
     * </pre>
     *
     * @param attribute1 ciip_comm_attachment.attribute1:
     */
    public void setAttribute1(String attribute1) { 
        this.attribute1 = attribute1 == null ? null : attribute1.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute2
     * </pre>
     *
     * @return attribute2
     * 
     * ciip_comm_attachment.attribute2:
     */
    public String getAttribute2() { 
        return attribute2;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute2
     * </pre>
     *
     * @param attribute2 ciip_comm_attachment.attribute2:
     */
    public void setAttribute2(String attribute2) { 
        this.attribute2 = attribute2 == null ? null : attribute2.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute3
     * </pre>
     *
     * @return attribute3
     * 
     * ciip_comm_attachment.attribute3:
     */
    public String getAttribute3() { 
        return attribute3;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute3
     * </pre>
     *
     * @param attribute3 ciip_comm_attachment.attribute3:
     */
    public void setAttribute3(String attribute3) { 
        this.attribute3 = attribute3 == null ? null : attribute3.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute4
     * </pre>
     *
     * @return attribute4
     * 
     * ciip_comm_attachment.attribute4:
     */
    public String getAttribute4() { 
        return attribute4;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute4
     * </pre>
     *
     * @param attribute4 ciip_comm_attachment.attribute4:
     */
    public void setAttribute4(String attribute4) { 
        this.attribute4 = attribute4 == null ? null : attribute4.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute5
     * </pre>
     *
     * @return attribute5
     * 
     * ciip_comm_attachment.attribute5:
     */
    public String getAttribute5() { 
        return attribute5;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute5
     * </pre>
     *
     * @param attribute5 ciip_comm_attachment.attribute5:
     */
    public void setAttribute5(String attribute5) { 
        this.attribute5 = attribute5 == null ? null : attribute5.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute6
     * </pre>
     *
     * @return attribute6
     * 
     * ciip_comm_attachment.attribute6:
     */
    public String getAttribute6() { 
        return attribute6;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute6
     * </pre>
     *
     * @param attribute6 ciip_comm_attachment.attribute6:
     */
    public void setAttribute6(String attribute6) { 
        this.attribute6 = attribute6 == null ? null : attribute6.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute7
     * </pre>
     *
     * @return attribute7
     * 
     * ciip_comm_attachment.attribute7:
     */
    public String getAttribute7() { 
        return attribute7;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute7
     * </pre>
     *
     * @param attribute7 ciip_comm_attachment.attribute7:
     */
    public void setAttribute7(String attribute7) { 
        this.attribute7 = attribute7 == null ? null : attribute7.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute8
     * </pre>
     *
     * @return attribute8
     * 
     * ciip_comm_attachment.attribute8:
     */
    public String getAttribute8() { 
        return attribute8;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute8
     * </pre>
     *
     * @param attribute8 ciip_comm_attachment.attribute8:
     */
    public void setAttribute8(String attribute8) { 
        this.attribute8 = attribute8 == null ? null : attribute8.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute9
     * </pre>
     *
     * @return attribute9
     * 
     * ciip_comm_attachment.attribute9:
     */
    public String getAttribute9() { 
        return attribute9;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute9
     * </pre>
     *
     * @param attribute9 ciip_comm_attachment.attribute9:
     */
    public void setAttribute9(String attribute9) { 
        this.attribute9 = attribute9 == null ? null : attribute9.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute10
     * </pre>
     *
     * @return attribute10
     * 
     * ciip_comm_attachment.attribute10:
     */
    public String getAttribute10() { 
        return attribute10;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute10
     * </pre>
     *
     * @param attribute10 ciip_comm_attachment.attribute10:
     */
    public void setAttribute10(String attribute10) { 
        this.attribute10 = attribute10 == null ? null : attribute10.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute11
     * </pre>
     *
     * @return attribute11
     * 
     * ciip_comm_attachment.attribute11:
     */
    public String getAttribute11() { 
        return attribute11;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute11
     * </pre>
     *
     * @param attribute11 ciip_comm_attachment.attribute11:
     */
    public void setAttribute11(String attribute11) { 
        this.attribute11 = attribute11 == null ? null : attribute11.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute12
     * </pre>
     *
     * @return attribute12
     * 
     * ciip_comm_attachment.attribute12:
     */
    public String getAttribute12() { 
        return attribute12;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute12
     * </pre>
     *
     * @param attribute12 ciip_comm_attachment.attribute12:
     */
    public void setAttribute12(String attribute12) { 
        this.attribute12 = attribute12 == null ? null : attribute12.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute13
     * </pre>
     *
     * @return attribute13
     * 
     * ciip_comm_attachment.attribute13:
     */
    public String getAttribute13() { 
        return attribute13;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute13
     * </pre>
     *
     * @param attribute13 ciip_comm_attachment.attribute13:
     */
    public void setAttribute13(String attribute13) { 
        this.attribute13 = attribute13 == null ? null : attribute13.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute14
     * </pre>
     *
     * @return attribute14
     * 
     * ciip_comm_attachment.attribute14:
     */
    public String getAttribute14() { 
        return attribute14;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute14
     * </pre>
     *
     * @param attribute14 ciip_comm_attachment.attribute14:
     */
    public void setAttribute14(String attribute14) { 
        this.attribute14 = attribute14 == null ? null : attribute14.trim();
    }
    /**
     * <pre>
     * 获取：
     * 表字段：ciip_comm_attachment.attribute15
     * </pre>
     *
     * @return attribute15
     * 
     * ciip_comm_attachment.attribute15:
     */
    public String getAttribute15() { 
        return attribute15;
    }

    /**
     * <pre>
     * 设置: 
     * 表字段：ciip_comm_attachment.attribute15
     * </pre>
     *
     * @param attribute15 ciip_comm_attachment.attribute15:
     */
    public void setAttribute15(String attribute15) { 
        this.attribute15 = attribute15 == null ? null : attribute15.trim();
    }

}