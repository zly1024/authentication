package com.zly.common.domain.comm;
import com.zly.common.domain.BaseDomain;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * ciip_comm.ciip_comm_message_template
**/
public class CiipCommMessageTemplate extends BaseDomain { 
    /**
     * <pre>
     * 消息类型: MAIL,SMS
     * 表字段 : ciip_comm_message_template.message_type
     * </pre>
     */
    @NotNull
    @Max(10)
    private String messageType;

    /**
     * <pre>
     * 消息模版代码CODE，系统自定义的模版代码
     * 表字段 : ciip_comm_message_template.message_template_code
     * </pre>
     */
    @NotNull
    @Max(50)
    private String messageTemplateCode;

    /**
     * <pre>
     * 模版名称
     * 表字段 : ciip_comm_message_template.template_name
     * </pre>
     */
    @NotNull
    @Max(240)
    private String templateName;

    /**
     * <pre>
     * 模版使用分类，如：验证码、系统通知等
     * 表字段 : ciip_comm_message_template.template_usage_catg
     * </pre>
     */
    @Max(80)
    private String templateUsageCatg;

    /**
     * <pre>
     * 模版类型： SMS 或 标记html还是txt的邮件等
     * 表字段 : ciip_comm_message_template.template_type
     * </pre>
     */
    @Max(80)
    private String templateType;

    /**
     * <pre>
     * 阿里云-短信模版定义code(当类型为SMS时必填)
     * 表字段 : ciip_comm_message_template.sms_template_code
     * </pre>
     */
    @Max(50)
    private String smsTemplateCode;

    /**
     * <pre>
     * 邮件主题(当类型为MAIL时有值)
     * 表字段 : ciip_comm_message_template.mail_subject
     * </pre>
     */
    @Max(255)
    private String mailSubject;

    /**
     * <pre>
     * 内容(当类型为MAIL时有值)
     * 表字段 : ciip_comm_message_template.mail_content
     * </pre>
     */
    private String mailContent;

    /**
     * <pre>
     * 启用
     * 表字段 : ciip_comm_message_template.enabled_flag
     * </pre>
     */
    @Max(1)
    private String enabledFlag;

    /**
     * <pre>
     * 生效日期
     * 表字段 : ciip_comm_message_template.start_date
     * </pre>
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startDate;

    /**
     * <pre>
     * 失效日期
     * 表字段 : ciip_comm_message_template.end_date
     * </pre>
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    /**
     * <pre>
     * 描述
     * 表字段 : ciip_comm_message_template.description
     * </pre>
     */
    @Max(240)
    private String description;

    /**
     * <pre>
     * 获取：消息类型: MAIL,SMS
     * 表字段：ciip_comm_message_template.message_type
     * </pre>
     *
     * @return messageType
     * 
     * ciip_comm_message_template.message_type:消息类型: MAIL,SMS
     */
    public String getMessageType() { 
        return messageType;
    }

    /**
     * <pre>
     * 设置: 消息类型: MAIL,SMS
     * 表字段：ciip_comm_message_template.message_type
     * </pre>
     *
     * @param messageType ciip_comm_message_template.message_type:消息类型: MAIL,SMS
     */
    public void setMessageType(String messageType) { 
        this.messageType = messageType == null ? null : messageType.trim();
    }
    /**
     * <pre>
     * 获取：消息模版代码CODE，系统自定义的模版代码
     * 表字段：ciip_comm_message_template.message_template_code
     * </pre>
     *
     * @return messageTemplateCode
     * 
     * ciip_comm_message_template.message_template_code:消息模版代码CODE，系统自定义的模版代码
     */
    public String getMessageTemplateCode() { 
        return messageTemplateCode;
    }

    /**
     * <pre>
     * 设置: 消息模版代码CODE，系统自定义的模版代码
     * 表字段：ciip_comm_message_template.message_template_code
     * </pre>
     *
     * @param messageTemplateCode ciip_comm_message_template.message_template_code:消息模版代码CODE，系统自定义的模版代码
     */
    public void setMessageTemplateCode(String messageTemplateCode) { 
        this.messageTemplateCode = messageTemplateCode == null ? null : messageTemplateCode.trim();
    }
    /**
     * <pre>
     * 获取：模版名称
     * 表字段：ciip_comm_message_template.template_name
     * </pre>
     *
     * @return templateName
     * 
     * ciip_comm_message_template.template_name:模版名称
     */
    public String getTemplateName() { 
        return templateName;
    }

    /**
     * <pre>
     * 设置: 模版名称
     * 表字段：ciip_comm_message_template.template_name
     * </pre>
     *
     * @param templateName ciip_comm_message_template.template_name:模版名称
     */
    public void setTemplateName(String templateName) { 
        this.templateName = templateName == null ? null : templateName.trim();
    }
    /**
     * <pre>
     * 获取：模版使用分类，如：验证码、系统通知等
     * 表字段：ciip_comm_message_template.template_usage_catg
     * </pre>
     *
     * @return templateUsageCatg
     * 
     * ciip_comm_message_template.template_usage_catg:模版使用分类，如：验证码、系统通知等
     */
    public String getTemplateUsageCatg() { 
        return templateUsageCatg;
    }

    /**
     * <pre>
     * 设置: 模版使用分类，如：验证码、系统通知等
     * 表字段：ciip_comm_message_template.template_usage_catg
     * </pre>
     *
     * @param templateUsageCatg ciip_comm_message_template.template_usage_catg:模版使用分类，如：验证码、系统通知等
     */
    public void setTemplateUsageCatg(String templateUsageCatg) { 
        this.templateUsageCatg = templateUsageCatg == null ? null : templateUsageCatg.trim();
    }
    /**
     * <pre>
     * 获取：模版类型： SMS 或 标记html还是txt的邮件等
     * 表字段：ciip_comm_message_template.template_type
     * </pre>
     *
     * @return templateType
     * 
     * ciip_comm_message_template.template_type:模版类型： SMS 或 标记html还是txt的邮件等
     */
    public String getTemplateType() { 
        return templateType;
    }

    /**
     * <pre>
     * 设置: 模版类型： SMS 或 标记html还是txt的邮件等
     * 表字段：ciip_comm_message_template.template_type
     * </pre>
     *
     * @param templateType ciip_comm_message_template.template_type:模版类型： SMS 或 标记html还是txt的邮件等
     */
    public void setTemplateType(String templateType) { 
        this.templateType = templateType == null ? null : templateType.trim();
    }
    /**
     * <pre>
     * 获取：阿里云-短信模版定义code(当类型为SMS时必填)
     * 表字段：ciip_comm_message_template.sms_template_code
     * </pre>
     *
     * @return smsTemplateCode
     * 
     * ciip_comm_message_template.sms_template_code:阿里云-短信模版定义code(当类型为SMS时必填)
     */
    public String getSmsTemplateCode() { 
        return smsTemplateCode;
    }

    /**
     * <pre>
     * 设置: 阿里云-短信模版定义code(当类型为SMS时必填)
     * 表字段：ciip_comm_message_template.sms_template_code
     * </pre>
     *
     * @param smsTemplateCode ciip_comm_message_template.sms_template_code:阿里云-短信模版定义code(当类型为SMS时必填)
     */
    public void setSmsTemplateCode(String smsTemplateCode) { 
        this.smsTemplateCode = smsTemplateCode == null ? null : smsTemplateCode.trim();
    }
    /**
     * <pre>
     * 获取：邮件主题(当类型为MAIL时有值)
     * 表字段：ciip_comm_message_template.mail_subject
     * </pre>
     *
     * @return mailSubject
     * 
     * ciip_comm_message_template.mail_subject:邮件主题(当类型为MAIL时有值)
     */
    public String getMailSubject() { 
        return mailSubject;
    }

    /**
     * <pre>
     * 设置: 邮件主题(当类型为MAIL时有值)
     * 表字段：ciip_comm_message_template.mail_subject
     * </pre>
     *
     * @param mailSubject ciip_comm_message_template.mail_subject:邮件主题(当类型为MAIL时有值)
     */
    public void setMailSubject(String mailSubject) { 
        this.mailSubject = mailSubject == null ? null : mailSubject.trim();
    }
    /**
     * <pre>
     * 获取：内容(当类型为MAIL时有值)
     * 表字段：ciip_comm_message_template.mail_content
     * </pre>
     *
     * @return mailContent
     * 
     * ciip_comm_message_template.mail_content:内容(当类型为MAIL时有值)
     */
    public String getMailContent() { 
        return mailContent;
    }

    /**
     * <pre>
     * 设置: 内容(当类型为MAIL时有值)
     * 表字段：ciip_comm_message_template.mail_content
     * </pre>
     *
     * @param mailContent ciip_comm_message_template.mail_content:内容(当类型为MAIL时有值)
     */
    public void setMailContent(String mailContent) { 
        this.mailContent = mailContent == null ? null : mailContent.trim();
    }
    /**
     * <pre>
     * 获取：启用
     * 表字段：ciip_comm_message_template.enabled_flag
     * </pre>
     *
     * @return enabledFlag
     * 
     * ciip_comm_message_template.enabled_flag:启用
     */
    public String getEnabledFlag() { 
        return enabledFlag;
    }

    /**
     * <pre>
     * 设置: 启用
     * 表字段：ciip_comm_message_template.enabled_flag
     * </pre>
     *
     * @param enabledFlag ciip_comm_message_template.enabled_flag:启用
     */
    public void setEnabledFlag(String enabledFlag) { 
        this.enabledFlag = enabledFlag == null ? null : enabledFlag.trim();
    }
    /**
     * <pre>
     * 获取：生效日期
     * 表字段：ciip_comm_message_template.start_date
     * </pre>
     *
     * @return startDate
     * 
     * ciip_comm_message_template.start_date:生效日期
     */
    public Date getStartDate() { 
        return startDate;
    }

    /**
     * <pre>
     * 设置: 生效日期
     * 表字段：ciip_comm_message_template.start_date
     * </pre>
     *
     * @param startDate ciip_comm_message_template.start_date:生效日期
     */
    public void setStartDate(Date startDate) { 
        this.startDate = startDate;
    }
    /**
     * <pre>
     * 获取：失效日期
     * 表字段：ciip_comm_message_template.end_date
     * </pre>
     *
     * @return endDate
     * 
     * ciip_comm_message_template.end_date:失效日期
     */
    public Date getEndDate() { 
        return endDate;
    }

    /**
     * <pre>
     * 设置: 失效日期
     * 表字段：ciip_comm_message_template.end_date
     * </pre>
     *
     * @param endDate ciip_comm_message_template.end_date:失效日期
     */
    public void setEndDate(Date endDate) { 
        this.endDate = endDate;
    }
    /**
     * <pre>
     * 获取：描述
     * 表字段：ciip_comm_message_template.description
     * </pre>
     *
     * @return description
     * 
     * ciip_comm_message_template.description:描述
     */
    public String getDescription() { 
        return description;
    }

    /**
     * <pre>
     * 设置: 描述
     * 表字段：ciip_comm_message_template.description
     * </pre>
     *
     * @param description ciip_comm_message_template.description:描述
     */
    public void setDescription(String description) { 
        this.description = description == null ? null : description.trim();
    }

}