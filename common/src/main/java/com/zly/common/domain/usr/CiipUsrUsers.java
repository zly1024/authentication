package com.zly.common.domain.usr;

import com.zly.common.domain.BaseDomain;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class CiipUsrUsers extends BaseDomain {
    /**
     * <pre>
     * 用户名
     * 表字段 : ciip_usr_users.user_name
     * </pre>
     */
    @NotNull
    private String userName;

    /**
     * <pre>
     * 密码
     * 表字段 : ciip_usr_users.password
     * </pre>
     */
    @NotNull
    private String password;

    /**
     * <pre>
     * 昵称
     * 表字段 : ciip_usr_users.nick_name
     * </pre>
     */
    private String nickName;

    /**
     * <pre>
     * 姓名（全名）
     * 表字段 : ciip_usr_users.full_name
     * </pre>
     */
    private String fullName;

    /**
     * <pre>
     * 区号
     * 表字段 : ciip_usr_users.area_code
     * </pre>
     */
    private String areaCode;

    /**
     * <pre>
     * 手机号
     * 表字段 : ciip_usr_users.mobile_phone_num
     * </pre>
     */
    private String mobilePhoneNum;

    /**
     * <pre>
     * 固定电话
     * 表字段 : ciip_usr_users.telephone_num
     * </pre>
     */
    private String telephoneNum;

    /**
     * <pre>
     * 邮箱
     * 表字段 : ciip_usr_users.email
     * </pre>
     */
    private String email;

    /**
     * <pre>
     * 用户角色CODE(保留字段，暂留空，根据角色表统一记录）
     * 表字段 : ciip_usr_users.user_role_code
     * </pre>
     */
    private String userRoleCode;

    /**
     * <pre>
     * 商家ID
     * 表字段 : ciip_usr_users.company_id
     * </pre>
     */
    private String companyId;

    /**
     * <pre>
     * 门店ID
     * 表字段 : ciip_usr_users.store_id
     * </pre>
     */
    private String storeId;

    /**
     * <pre>
     * 来源系统标识: USER,EMP,S,C,PLATFORM
     * 表字段 : ciip_usr_users.source_sys_ref_code
     * </pre>
     */
    private String sourceSysRefCode;

    /**
     * <pre>
     * 来源系统ID
     * 表字段 : ciip_usr_users.source_sys_ref_id
     * </pre>
     */
    private String sourceSysRefId;

    /**
     * <pre>
     * 性别(男M/女F/未知X）
     * 表字段 : ciip_usr_users.gender
     * </pre>
     */
    private String gender;

    /**
     * <pre>
     * 生日
     * 表字段 : ciip_usr_users.birthday
     * </pre>
     */

    private Date birthday;

    /**
     * <pre>
     * 语言
     * 表字段 : ciip_usr_users.language
     * </pre>
     */
    private String language;

    /**
     * <pre>
     * 城市
     * 表字段 : ciip_usr_users.city
     * </pre>
     */
    private String city;

    /**
     * <pre>
     * 省
     * 表字段 : ciip_usr_users.province
     * </pre>
     */
    private String province;

    /**
     * <pre>
     * 国家
     * 表字段 : ciip_usr_users.country
     * </pre>
     */
    private String country;

    /**
     * <pre>
     * 用户头像
     * 表字段 : ciip_usr_users.user_head_img_url
     * </pre>
     */
    private String userHeadImgUrl;

    /**
     * <pre>
     * 等级
     * 表字段 : ciip_usr_users.rank
     * </pre>
     */
    private Integer rank;

    /**
     * <pre>
     * 账号授权类型
     * 表字段 : ciip_usr_users.source_account_type
     * </pre>
     */
    private String sourceAccountType;

    /**
     * <pre>
     * 来源账号
     * 表字段 : ciip_usr_users.source_account_num
     * </pre>
     */
    private String sourceAccountNum;

    /**
     * <pre>
     * 微信unionid
     * 表字段 : ciip_usr_users.wx_unionid
     * </pre>
     */
    private String wxUnionid;

    /**
     * <pre>
     * 账号授权类型2（支付宝）
     * 表字段 : ciip_usr_users.source_account_type_zfb
     * </pre>
     */
    private String sourceAccountTypeZfb;

    /**
     * <pre>
     * 来源账号2（支付宝）
     * 表字段 : ciip_usr_users.source_account_num_zfb
     * </pre>
     */
    private String sourceAccountNumZfb;

    /**
     * <pre>
     * 账号注册日期
     * 表字段 : ciip_usr_users.registration_date
     * </pre>
     */
    private Date registrationDate;

    /**
     * <pre>
     * 账户当前状态
     * 表字段 : ciip_usr_users.account_status_code
     * </pre>
     */
    private String accountStatusCode;

    /**
     * <pre>
     * 生效日期
     * 表字段 : ciip_usr_users.start_date
     * </pre>
     */
    private Date startDate;

    /**
     * <pre>
     * 失效日期
     * 表字段 : ciip_usr_users.end_date
     * </pre>
     */
    private Date endDate;

    /**
     * <pre>
     * 描述
     * 表字段 : ciip_usr_users.description
     * </pre>
     */
    private String description;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute_catrgory
     * </pre>
     */
    private String attributeCatrgory;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute1
     * </pre>
     */
    private String attribute1;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute2
     * </pre>
     */
    private String attribute2;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute3
     * </pre>
     */
    private String attribute3;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute4
     * </pre>
     */
    private String attribute4;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute5
     * </pre>
     */
    private String attribute5;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute6
     * </pre>
     */
    private String attribute6;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute7
     * </pre>
     */
    private String attribute7;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute8
     * </pre>
     */
    private String attribute8;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute9
     * </pre>
     */
    private String attribute9;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute10
     * </pre>
     */
    private String attribute10;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute11
     * </pre>
     */
    private String attribute11;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute12
     * </pre>
     */
    private String attribute12;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute13
     * </pre>
     */
    private String attribute13;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute14
     * </pre>
     */
    private String attribute14;

    /**
     * <pre>
     *
     * 表字段 : ciip_usr_users.attribute15
     * </pre>
     */
    private String attribute15;

    /**
     * <pre>
     * 获取：用户名
     * 表字段：ciip_usr_users.user_name
     * </pre>
     *
     * @return ciip_usr_users.user_name：用户名
     */
    public String getUserName() {
        return userName;
    }

    /**
     * <pre>
     * 设置：用户名
     * 表字段：ciip_usr_users.user_name
     * </pre>
     *
     * @param userName ciip_usr_users.user_name：用户名
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * <pre>
     * 获取：密码
     * 表字段：ciip_usr_users.password
     * </pre>
     *
     * @return ciip_usr_users.password：密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * <pre>
     * 设置：密码
     * 表字段：ciip_usr_users.password
     * </pre>
     *
     * @param password ciip_usr_users.password：密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /**
     * <pre>
     * 获取：姓名（全名）
     * 表字段：ciip_usr_users.full_name
     * </pre>
     *
     * @return ciip_usr_users.full_name：姓名（全名）
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * <pre>
     * 设置：姓名（全名）
     * 表字段：ciip_usr_users.full_name
     * </pre>
     *
     * @param fullName ciip_usr_users.full_name：姓名（全名）
     */
    public void setFullName(String fullName) {
        this.fullName = fullName == null ? null : fullName.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    /**
     * <pre>
     * 获取：手机号
     * 表字段：ciip_usr_users.mobile_phone_num
     * </pre>
     *
     * @return ciip_usr_users.mobile_phone_num：手机号
     */
    public String getMobilePhoneNum() {
        return mobilePhoneNum;
    }

    /**
     * <pre>
     * 设置：手机号
     * 表字段：ciip_usr_users.mobile_phone_num
     * </pre>
     *
     * @param mobilePhoneNum ciip_usr_users.mobile_phone_num：手机号
     */
    public void setMobilePhoneNum(String mobilePhoneNum) {
        this.mobilePhoneNum = mobilePhoneNum == null ? null : mobilePhoneNum.trim();
    }

    /**
     * <pre>
     * 获取：固定电话
     * 表字段：ciip_usr_users.telephone_num
     * </pre>
     *
     * @return ciip_usr_users.telephone_num：固定电话
     */
    public String getTelephoneNum() {
        return telephoneNum;
    }

    /**
     * <pre>
     * 设置：固定电话
     * 表字段：ciip_usr_users.telephone_num
     * </pre>
     *
     * @param telephoneNum ciip_usr_users.telephone_num：固定电话
     */
    public void setTelephoneNum(String telephoneNum) {
        this.telephoneNum = telephoneNum == null ? null : telephoneNum.trim();
    }

    /**
     * <pre>
     * 获取：邮箱
     * 表字段：ciip_usr_users.email
     * </pre>
     *
     * @return ciip_usr_users.email：邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * <pre>
     * 设置：邮箱
     * 表字段：ciip_usr_users.email
     * </pre>
     *
     * @param email ciip_usr_users.email：邮箱
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * <pre>
     * 获取：用户类型／来源系统
     * 表字段：ciip_usr_users.user_role_code
     * </pre>
     *
     * @return ciip_usr_users.user_role_code：用户类型／来源系统
     */
    public String getUserRoleCode() {
        return userRoleCode;
    }

    /**
     * <pre>
     * 设置：用户类型／来源系统
     * 表字段：ciip_usr_users.user_role_code
     * </pre>
     *
     * @param userRoleCode ciip_usr_users.user_role_code：用户类型／来源系统
     */
    public void setUserRoleCode(String userRoleCode) {
        this.userRoleCode = userRoleCode == null ? null : userRoleCode.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId == null ? null : storeId.trim();
    }

    /**
     * <pre>
     * 获取：来源系统标识: USER,ORG,PLATFORM
     * 表字段：ciip_usr_users.source_sys_ref_code
     * </pre>
     *
     * @return ciip_usr_users.source_sys_ref_code：来源系统标识: USER,ORG,PLATFORM
     */
    public String getSourceSysRefCode() {
        return sourceSysRefCode;
    }

    /**
     * <pre>
     * 设置：来源系统标识: USER,ORG,PLATFORM
     * 表字段：ciip_usr_users.source_sys_ref_code
     * </pre>
     *
     * @param sourceSysRefCode ciip_usr_users.source_sys_ref_code：来源系统标识: USER,ORG,PLATFORM
     */
    public void setSourceSysRefCode(String sourceSysRefCode) {
        this.sourceSysRefCode = sourceSysRefCode == null ? null : sourceSysRefCode.trim();
    }

    /**
     * <pre>
     * 获取：来源系统ID
     * 表字段：ciip_usr_users.source_sys_ref_id
     * </pre>
     *
     * @return ciip_usr_users.source_sys_ref_id：来源系统ID
     */
    public String getSourceSysRefId() {
        return sourceSysRefId;
    }

    /**
     * <pre>
     * 设置：来源系统ID
     * 表字段：ciip_usr_users.source_sys_ref_id
     * </pre>
     *
     * @param sourceSysRefId ciip_usr_users.source_sys_ref_id：来源系统ID
     */
    public void setSourceSysRefId(String sourceSysRefId) {
        this.sourceSysRefId = sourceSysRefId == null ? null : sourceSysRefId.trim();
    }

    /**
     * <pre>
     * 获取：性别
     * 表字段：ciip_usr_users.gender
     * </pre>
     *
     * @return ciip_usr_users.gender：性别
     */
    public String getGender() {
        return gender;
    }

    /**
     * <pre>
     * 设置：性别
     * 表字段：ciip_usr_users.gender
     * </pre>
     *
     * @param gender ciip_usr_users.gender：性别
     */
    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    /**
     * <pre>
     * 获取：生日
     * 表字段：ciip_usr_users.birthday
     * </pre>
     *
     * @return ciip_usr_users.birthday：生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * <pre>
     * 设置：生日
     * 表字段：ciip_usr_users.birthday
     * </pre>
     *
     * @param birthday ciip_usr_users.birthday：生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getUserHeadImgUrl() {
        return userHeadImgUrl;
    }

    public void setUserHeadImgUrl(String userHeadImgUrl) {
        this.userHeadImgUrl = userHeadImgUrl == null ? null : userHeadImgUrl.trim();
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    /**
     * <pre>
     * 获取：账号授权类型
     * 表字段：ciip_usr_users.source_account_type
     * </pre>
     *
     * @return ciip_usr_users.source_account_type：账号授权类型
     */
    public String getSourceAccountType() {
        return sourceAccountType;
    }

    /**
     * <pre>
     * 设置：账号授权类型
     * 表字段：ciip_usr_users.source_account_type
     * </pre>
     *
     * @param sourceAccountType ciip_usr_users.source_account_type：账号授权类型
     */
    public void setSourceAccountType(String sourceAccountType) {
        this.sourceAccountType = sourceAccountType == null ? null : sourceAccountType.trim();
    }

    /**
     * <pre>
     * 获取：来源账号
     * 表字段：ciip_usr_users.source_account_num
     * </pre>
     *
     * @return ciip_usr_users.source_account_num：来源账号
     */
    public String getSourceAccountNum() {
        return sourceAccountNum;
    }

    /**
     * <pre>
     * 设置：来源账号
     * 表字段：ciip_usr_users.source_account_num
     * </pre>
     *
     * @param sourceAccountNum ciip_usr_users.source_account_num：来源账号
     */
    public void setSourceAccountNum(String sourceAccountNum) {
        this.sourceAccountNum = sourceAccountNum == null ? null : sourceAccountNum.trim();
    }

    public String getWxUnionid() {
        return wxUnionid;
    }

    public void setWxUnionid(String wxUnionid) {
        this.wxUnionid = wxUnionid == null ? null : wxUnionid.trim();
    }

    public String getSourceAccountTypeZfb() {
        return sourceAccountTypeZfb;
    }

    public void setSourceAccountTypeZfb(String sourceAccountTypeZfb) {
        this.sourceAccountTypeZfb = sourceAccountTypeZfb == null ? null : sourceAccountTypeZfb.trim();
    }

    public String getSourceAccountNumZfb() {
        return sourceAccountNumZfb;
    }

    public void setSourceAccountNumZfb(String sourceAccountNumZfb) {
        this.sourceAccountNumZfb = sourceAccountNumZfb == null ? null : sourceAccountNumZfb.trim();
    }

    /**
     * <pre>
     * 获取：账号注册日期
     * 表字段：ciip_usr_users.registration_date
     * </pre>
     *
     * @return ciip_usr_users.registration_date：账号注册日期
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * <pre>
     * 设置：账号注册日期
     * 表字段：ciip_usr_users.registration_date
     * </pre>
     *
     * @param registrationDate ciip_usr_users.registration_date：账号注册日期
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * <pre>
     * 获取：账户当前状态
     * 表字段：ciip_usr_users.account_status_code
     * </pre>
     *
     * @return ciip_usr_users.account_status_code：账户当前状态
     */
    public String getAccountStatusCode() {
        return accountStatusCode;
    }

    /**
     * <pre>
     * 设置：账户当前状态
     * 表字段：ciip_usr_users.account_status_code
     * </pre>
     *
     * @param accountStatusCode ciip_usr_users.account_status_code：账户当前状态
     */
    public void setAccountStatusCode(String accountStatusCode) {
        this.accountStatusCode = accountStatusCode == null ? null : accountStatusCode.trim();
    }

    /**
     * <pre>
     * 获取：生效日期
     * 表字段：ciip_usr_users.start_date
     * </pre>
     *
     * @return ciip_usr_users.start_date：生效日期
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * <pre>
     * 设置：生效日期
     * 表字段：ciip_usr_users.start_date
     * </pre>
     *
     * @param startDate ciip_usr_users.start_date：生效日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * <pre>
     * 获取：失效日期
     * 表字段：ciip_usr_users.end_date
     * </pre>
     *
     * @return ciip_usr_users.end_date：失效日期
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * <pre>
     * 设置：失效日期
     * 表字段：ciip_usr_users.end_date
     * </pre>
     *
     * @param endDate ciip_usr_users.end_date：失效日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * <pre>
     * 获取：描述
     * 表字段：ciip_usr_users.description
     * </pre>
     *
     * @return ciip_usr_users.description：描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * <pre>
     * 设置：描述
     * 表字段：ciip_usr_users.description
     * </pre>
     *
     * @param description ciip_usr_users.description：描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute_catrgory
     * </pre>
     *
     * @return ciip_usr_users.attribute_catrgory：
     */
    public String getAttributeCatrgory() {
        return attributeCatrgory;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute_catrgory
     * </pre>
     *
     * @param attributeCatrgory ciip_usr_users.attribute_catrgory：
     */
    public void setAttributeCatrgory(String attributeCatrgory) {
        this.attributeCatrgory = attributeCatrgory == null ? null : attributeCatrgory.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute1
     * </pre>
     *
     * @return ciip_usr_users.attribute1：
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute1
     * </pre>
     *
     * @param attribute1 ciip_usr_users.attribute1：
     */
    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1 == null ? null : attribute1.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute2
     * </pre>
     *
     * @return ciip_usr_users.attribute2：
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute2
     * </pre>
     *
     * @param attribute2 ciip_usr_users.attribute2：
     */
    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2 == null ? null : attribute2.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute3
     * </pre>
     *
     * @return ciip_usr_users.attribute3：
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute3
     * </pre>
     *
     * @param attribute3 ciip_usr_users.attribute3：
     */
    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3 == null ? null : attribute3.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute4
     * </pre>
     *
     * @return ciip_usr_users.attribute4：
     */
    public String getAttribute4() {
        return attribute4;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute4
     * </pre>
     *
     * @param attribute4 ciip_usr_users.attribute4：
     */
    public void setAttribute4(String attribute4) {
        this.attribute4 = attribute4 == null ? null : attribute4.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute5
     * </pre>
     *
     * @return ciip_usr_users.attribute5：
     */
    public String getAttribute5() {
        return attribute5;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute5
     * </pre>
     *
     * @param attribute5 ciip_usr_users.attribute5：
     */
    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5 == null ? null : attribute5.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute6
     * </pre>
     *
     * @return ciip_usr_users.attribute6：
     */
    public String getAttribute6() {
        return attribute6;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute6
     * </pre>
     *
     * @param attribute6 ciip_usr_users.attribute6：
     */
    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6 == null ? null : attribute6.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute7
     * </pre>
     *
     * @return ciip_usr_users.attribute7：
     */
    public String getAttribute7() {
        return attribute7;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute7
     * </pre>
     *
     * @param attribute7 ciip_usr_users.attribute7：
     */
    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7 == null ? null : attribute7.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute8
     * </pre>
     *
     * @return ciip_usr_users.attribute8：
     */
    public String getAttribute8() {
        return attribute8;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute8
     * </pre>
     *
     * @param attribute8 ciip_usr_users.attribute8：
     */
    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8 == null ? null : attribute8.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute9
     * </pre>
     *
     * @return ciip_usr_users.attribute9：
     */
    public String getAttribute9() {
        return attribute9;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute9
     * </pre>
     *
     * @param attribute9 ciip_usr_users.attribute9：
     */
    public void setAttribute9(String attribute9) {
        this.attribute9 = attribute9 == null ? null : attribute9.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute10
     * </pre>
     *
     * @return ciip_usr_users.attribute10：
     */
    public String getAttribute10() {
        return attribute10;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute10
     * </pre>
     *
     * @param attribute10 ciip_usr_users.attribute10：
     */
    public void setAttribute10(String attribute10) {
        this.attribute10 = attribute10 == null ? null : attribute10.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute11
     * </pre>
     *
     * @return ciip_usr_users.attribute11：
     */
    public String getAttribute11() {
        return attribute11;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute11
     * </pre>
     *
     * @param attribute11 ciip_usr_users.attribute11：
     */
    public void setAttribute11(String attribute11) {
        this.attribute11 = attribute11 == null ? null : attribute11.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute12
     * </pre>
     *
     * @return ciip_usr_users.attribute12：
     */
    public String getAttribute12() {
        return attribute12;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute12
     * </pre>
     *
     * @param attribute12 ciip_usr_users.attribute12：
     */
    public void setAttribute12(String attribute12) {
        this.attribute12 = attribute12 == null ? null : attribute12.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute13
     * </pre>
     *
     * @return ciip_usr_users.attribute13：
     */
    public String getAttribute13() {
        return attribute13;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute13
     * </pre>
     *
     * @param attribute13 ciip_usr_users.attribute13：
     */
    public void setAttribute13(String attribute13) {
        this.attribute13 = attribute13 == null ? null : attribute13.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute14
     * </pre>
     *
     * @return ciip_usr_users.attribute14：
     */
    public String getAttribute14() {
        return attribute14;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute14
     * </pre>
     *
     * @param attribute14 ciip_usr_users.attribute14：
     */
    public void setAttribute14(String attribute14) {
        this.attribute14 = attribute14 == null ? null : attribute14.trim();
    }

    /**
     * <pre>
     * 获取：
     * 表字段：ciip_usr_users.attribute15
     * </pre>
     *
     * @return ciip_usr_users.attribute15：
     */
    public String getAttribute15() {
        return attribute15;
    }

    /**
     * <pre>
     * 设置：
     * 表字段：ciip_usr_users.attribute15
     * </pre>
     *
     * @param attribute15 ciip_usr_users.attribute15：
     */
    public void setAttribute15(String attribute15) {
        this.attribute15 = attribute15 == null ? null : attribute15.trim();
    }

}