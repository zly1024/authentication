package com.zly.common.redis.lock;

import com.zly.common.exception.BaseErrorsEnum;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.util.concurrent.TimeUnit;

/**
 * @author : dezhi.shen
 * date   : 2017/6/16.
 * description :
 */
public class RedissonLockHelper {

    private RedissonClient redisson;

    public RedissonLockHelper(RedissonClient redisson) {
        this.redisson = redisson;
    }

    public void setRedisson(RedissonClient redisson) {
        this.redisson = redisson;
    }

    /**
     * 根据 key 获得配置文件中RedissonClient中的RLock对象
     *
     * @param key 锁的key
     * @return RLock对象
     */
    public RLock getRLock(String key) {
        return redisson.getLock(key);
    }

    /**
     * 增加 不等待加锁
     *
     * @param key       锁key
     * @param leaseTime 锁失效时间
     * @param timeUnit  时间单位 {@link TimeUnit}
     */
    public void tryLock(String key, long leaseTime, TimeUnit timeUnit) {
        try {
            getRLock(key).tryLock(leaseTime, timeUnit);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw BaseErrorsEnum.RLOCK_TRY_LOCK_ERROR.exception;
        }
    }

    /**
     * 加锁
     *
     * @param key       锁id
     * @param waitTime  等待时间
     * @param leaseTime 锁失效时间
     * @param timeUnit  时间单位 {@link TimeUnit}
     */
    public void tryLock(String key, long waitTime, long leaseTime, TimeUnit timeUnit) {
        try {
            getRLock(key).tryLock(waitTime, leaseTime, timeUnit);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw BaseErrorsEnum.RLOCK_TRY_LOCK_ERROR.exception;
        }
    }

    /**
     * 判断是否被锁
     *
     * @param key key
     * @return true:被锁,false:未被锁
     */
    public boolean isLock(String key) {
        return getRLock(key).isLocked();
    }

    /**
     * 判断是否被锁,如果被锁throw {@link BaseErrorsEnum}.RLOCK_HAS_LOCKED.exception
     *
     * @param key key
     */
    public void isLockThrowException(String key) {
        if (getRLock(key).isLocked()) {
            throw BaseErrorsEnum.RLOCK_HAS_LOCKED.exception;
        }
    }

    /**
     * 根据key解锁
     *
     * @param key
     */
    public void unLock(String key) {
        getRLock(key).unlock();
    }
}
