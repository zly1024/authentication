/*
 * #{copyright}#
 */
package com.zly.common.redis;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author zly
 */
public class RedisTemplateHolder {

    private static RedisTemplate<String, String> redisTemplate;

    public static void holdRedisTemplate(RedisTemplate redisTemplate) {
        RedisTemplateHolder.redisTemplate = redisTemplate;
    }

    public static RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

}
