/*
 * #{copyright}#
 */
package com.zly.common.redis;

import com.fasterxml.jackson.databind.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * 在序列化前进行压缩，
 * 在反序列化时进行解压
 * @author zly
 */
public class CompressJackson2JsonRedisSerializer<T> extends Jackson2JsonRedisSerializer<T>{

    private static Logger logger = LoggerFactory.getLogger(CompressJackson2JsonRedisSerializer.class);

    public CompressJackson2JsonRedisSerializer(Class<T> type) {
        super(type);
    }

    public CompressJackson2JsonRedisSerializer(JavaType javaType) {
        super(javaType);
    }

    @Override
    public T deserialize(byte[] bytes) throws SerializationException {
        byte[] trueBytes = uncompress(bytes);
        return super.deserialize(trueBytes);
    }

    @Override
    public byte[] serialize(Object t) throws SerializationException {
        byte[] jsonBytes = super.serialize(t);
        return compress(jsonBytes);
    }

    public byte[] compress(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(bytes);
            gzip.close();
        } catch (IOException e) {
            logger.error("gzip compress error.", e);
        }
        return out.toByteArray();
    }

    public byte[] uncompress(byte[] bytes) {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        try {
            GZIPInputStream ungzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int n;
            while ((n = ungzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }
        } catch (IOException e) {
            logger.error("gzip uncompress error.", e);
        }

        return out.toByteArray();
    }
}
