package com.zly.common.dto;

/**
 *
 * @author zly
 * @date
 */
public class FileBean {
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 字节流
     */
    private byte[] stream;

    /**
     * 获得文件名
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置文件名
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 获得字节流
     *
     * @return
     */
    public byte[] getStream() {
        return stream;
    }

    /**
     * 设置字节流
     *
     * @param stream
     */
    public void setStream(byte[] stream) {
        this.stream = stream;
    }
}
