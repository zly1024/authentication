/*
 * #{copyright}#
 */
package com.zly.common.dto;

import com.zly.common.domain.comm.CiipCommAttachment;
import com.zly.common.domain.comm.CiipCommMessage;
import com.zly.common.domain.comm.CiipCommReceiver;

import javax.persistence.Transient;
import java.util.List;

/**
 * 消息dto
 * @author zly
 * @date
 */
public class MessageDto extends CiipCommMessage {

    @Transient
    private List<CiipCommReceiver> receivers;

    @Transient
    private List<CiipCommAttachment> attachments;

    private String saveMsgFlag;

    public List<CiipCommReceiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<CiipCommReceiver> receivers) {
        this.receivers = receivers;
    }

    public List<CiipCommAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<CiipCommAttachment> attachments) {
        this.attachments = attachments;
    }

    public String getSaveMsgFlag() {
        return saveMsgFlag;
    }

    public void setSaveMsgFlag(String saveMsgFlag) {
        this.saveMsgFlag = saveMsgFlag;
    }
}
