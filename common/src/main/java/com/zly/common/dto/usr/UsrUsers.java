package com.zly.common.dto.usr;

import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.common.domain.usr.CiipUsrUsers;

import java.util.List;
/**
 *
 * @author zly
 * @date
 */
public class UsrUsers extends CiipUsrUsers {

    private List<CiipUsrSysUserAuth> ciipUsrSysUserAuth;

    public List<CiipUsrSysUserAuth> getCiipUsrSysUserAuth() {
        return ciipUsrSysUserAuth;
    }

    public void setCiipUsrSysUserAuth(List<CiipUsrSysUserAuth> ciipUsrSysUserAuth) {
        this.ciipUsrSysUserAuth = ciipUsrSysUserAuth;
    }
}
