/*
 * #{copyright}#
 */
package com.zly.common.dto;

import com.zly.common.domain.comm.CiipCommFile;
import org.springframework.beans.BeanUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author zly
 * @date
 */
public class FileDto extends CiipCommFile {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public void copyProperties(CiipCommFile ciipCommFile) {
        BeanUtils.copyProperties(ciipCommFile, this);
    }
}
