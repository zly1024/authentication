package com.zly.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.StringUtils;

import java.util.Date;
/**
 *
 * @author zly
 * @date
 */
public class VerifyCodeDto {
    private String verifyCode;
    private String sender;
    private String application;
    private String event;
    private Date sendTime;

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    //获取key
    @JsonIgnore
    public String getKey() {
        String key = sender;
        if (!StringUtils.isEmpty(event)) {
            key = event + "_" + key;
        }
        if (!StringUtils.isEmpty(application)) {
            key = application + "_" + key;
        }
        return key;
    }
}
