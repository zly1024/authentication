package com.zly.common.dto.usr;

/**
 *
 * @author zly
 * @date
 */
public class UserRoleCode {
    private String userId;
    private String roleCodes;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleCodes() {
        return roleCodes;
    }

    public void setRoleCodes(String roleCodes) {
        this.roleCodes = roleCodes;
    }
}
