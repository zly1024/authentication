package com.zly.common.dto;
/**
 *
 * @author zly
 * @date
 */
public class BooleanObject {
    private boolean result;

    public BooleanObject() {
    }

    public BooleanObject(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
