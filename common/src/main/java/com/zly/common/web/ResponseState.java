/*
 * #{copyright}#
 */
package com.zly.common.web;

/**
 * 响应代码
 * @author zly
 */
public enum ResponseState {


    /**
     *  错误.
     */
    ERROR("error"),

    /**
     *  成功.
     */
    SUCCESS("success");

    private String value;

    ResponseState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
