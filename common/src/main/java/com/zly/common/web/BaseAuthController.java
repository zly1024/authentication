/*
 * #{copyright}#
 */
package com.zly.common.web;

import com.zly.common.security.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Set;

/**
 * 可以获取用户信息的控制器基类.
 * @author zly
 */
public class BaseAuthController extends BaseController {


    public User getUser() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user;
    }

    public String getUserId() {
        return getUser().getUserId();
    }

    public Set<GrantedAuthority> getAuthority() {
        return (Set<GrantedAuthority>) getUser().getAuthorities();
    }

}
