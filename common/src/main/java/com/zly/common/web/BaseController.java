/*
 * #{copyright}#
 */
package com.zly.common.web;

import com.zly.common.util.SimpleDateEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import java.util.Date;

/**
 * @author zly
 */
public class BaseController {

    private static Logger logger = LoggerFactory.getLogger(BaseController.class);


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new SimpleDateEditor());
    }

    public ResponseEntity success(Object data) {
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setData(data);
        responseEntity.setCode(ResponseState.SUCCESS.getValue());
        return responseEntity;
    }

    public ResponseEntity fail(String msg) {
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setCode(ResponseState.ERROR.getValue());
        responseEntity.setMsg(msg);
        return responseEntity;
    }

}
