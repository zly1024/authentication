/*
 * #{copyright}#
 */
package com.zly.common.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author zly
 */
public class User implements UserDetails, CredentialsContainer {

    private String userId;
    private String password;
    private String username;
    private Set<String> authoritiesSet;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private Map<String, Object> additionalAttributes = new ConcurrentHashMap<>();

    public User() {
    }

    @JsonCreator
    public User(@JsonProperty("userId") String userId,
                @JsonProperty("password") String password,
                @JsonProperty("username") String username,
                @JsonProperty("authoritiesSet") Set<String> authoritiesSet,
                @JsonProperty("accountNonExpired") boolean accountNonExpired,
                @JsonProperty("accountNonLocked") boolean accountNonLocked,
                @JsonProperty("credentialsNonExpired") boolean credentialsNonExpired,
                @JsonProperty("enabled") boolean enabled,
                @JsonProperty("additionalAttributes") Map<String, Object> additionalAttributes) {
        this.userId = userId;
        this.password = password;
        this.username = username;
        this.authoritiesSet = authoritiesSet;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        this.additionalAttributes = additionalAttributes;
    }

    public User(String userId, String username, String password,
                Collection<? extends GrantedAuthority> authorities) {
        this(userId, username, password, true, true, true, true, authorities);
    }

    public User(String userId, String username, String password, boolean enabled,
                boolean accountNonExpired, boolean credentialsNonExpired,
                boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {

        if (StringUtils.isEmpty(username) || password == null) {
            throw new IllegalArgumentException(
                    "Cannot pass null or empty values to constructor");
        }
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.credentialsNonExpired = credentialsNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.authoritiesSet = authoritiesToSet(authorities);
    }


    private Set<String> authoritiesToSet(Collection<? extends GrantedAuthority> authorities) {
        return authorities.stream().map(authority -> {
            return authority.getAuthority();
        }).collect(Collectors.toSet());
    }

    @JsonIgnore
    @Override
    public Set<GrantedAuthority> getAuthorities() {
        return this.authoritiesSet.stream().map(value -> {
            return new SimpleGrantedAuthority(value);
        }).collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public void eraseCredentials() {
        password = null;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Set<String> getAuthoritiesSet() {
        return authoritiesSet;
    }

    /**
     * Returns {@code true} if the supplied object is a {@code User} instance with the
     * same {@code username} value.
     * <p>
     * In other words, the objects are equal if they have the same username, representing
     * the same principal.
     */
    @Override
    public boolean equals(Object rhs) {
        if (rhs instanceof User) {
            return username.equals(((User) rhs).username);
        }
        return false;
    }

    /**
     * Returns the hashcode of the {@code username}.
     */
    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append(": ");
        sb.append("UserId: ").append(this.userId).append("; ");
        sb.append("Username: ").append(this.username).append("; ");
        sb.append("Password: [PROTECTED]; ");
        sb.append("Enabled: ").append(this.enabled).append("; ");
        sb.append("AccountNonExpired: ").append(this.accountNonExpired).append("; ");
        sb.append("credentialsNonExpired: ").append(this.credentialsNonExpired)
                .append("; ");
        sb.append("AccountNonLocked: ").append(this.accountNonLocked).append("; ");

        if (!authoritiesSet.isEmpty()) {
            sb.append("Granted Authorities: ");

            boolean first = true;
            for (String auth : authoritiesSet) {
                if (!first) {
                    sb.append(",");
                }
                first = false;

                sb.append(auth);
            }
        } else {
            sb.append("Not granted any authorities");
        }

        return sb.toString();
    }

    private static class AuthorityComparator implements Comparator<GrantedAuthority>,
            Serializable {
        private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

        @Override
        public int compare(GrantedAuthority g1, GrantedAuthority g2) {
            // Neither should ever be null as each entry is checked before adding it to
            // the set.
            // If the authority is null, it is a custom authority and should precede
            // others.
            if (g2.getAuthority() == null) {
                return -1;
            }

            if (g1.getAuthority() == null) {
                return 1;
            }

            return g1.getAuthority().compareTo(g2.getAuthority());
        }
    }

    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }

    public void setAdditionalAttributes(Map<String, Object> additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    public void addAttribute(String key, Object attr) {
        if (attr != null) {
            this.additionalAttributes.put(key, attr);
        }
    }

    public void addAttributes(Map<String, Object> maps) {
        if (maps != null && !maps.isEmpty()) {
            this.additionalAttributes.putAll(maps);
        }
    }

    public Object getAttribute(String key) {
        return this.additionalAttributes.get(key);
    }
}
