/*
 * #{copyright}#
 */
package com.zly.common.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;

/**
 *  权限上下文.
 * @author zly
 */
public class SecurityContext {

    public static User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getDetails();
    }

    public Collection<GrantedAuthority> getRoles() {
        return getCurrentUser().getAuthorities();
    }
}
