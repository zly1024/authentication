package com.zly.common.exception;

/**
 *
 * @author zly
 * @date
 */
public enum BaseErrorsEnum{

    /**
     * 微信异常.
     */
    WX_ERROR("1001", ""),

    /**
     * 授权异常.
     */
    AUTH_ERROR("", ""),

    /**
     *  服务不通.
     */
    SERVICE_ERROR("9999", "msg.error.common.service.not.reacheable"),

    /**
     *  已被锁.
     */
    RLOCK_HAS_LOCKED("1003", "msg.error.common.rlock_has_locked"),

    /**
     *  获取锁失败.
     */
    RLOCK_TRY_LOCK_ERROR("1004", "msg.error.common.rlock_try_lock_error"),

    /**
     *  获取锁配置失败.
     */
    RLOCK_GET_CONFIG_ERROR("1005", "msg.error.common.rlock_get_config_error");

    private String errorCode;
    private String messageCode;

    public MultiLangException exception;

    BaseErrorsEnum(String errorCode, String messageCode) {
        this.errorCode = errorCode;
        this.messageCode = messageCode;
        this.exception = new MultiLangException(errorCode, messageCode);
    }


    public String getErrorCode() {
        return errorCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

}
