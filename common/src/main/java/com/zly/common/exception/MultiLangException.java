package com.zly.common.exception;


/**
 * @author zly
 */
public class MultiLangException extends RuntimeException {

    private String errorCode;
    private String messageCode;

    private String[] args;

    public MultiLangException(String errorCode, String messageCode) {
        super(messageCode);
        this.errorCode = errorCode;
        this.messageCode = messageCode;
    }

    public MultiLangException(String errorCode, String messageCode, String[] args) {
        super(messageCode);
        this.errorCode = errorCode;
        this.messageCode = messageCode;
        this.args = args;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
