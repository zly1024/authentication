package com.zly.user.service;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.usr.CiipUsrSysRoles;

import java.util.List;

/**
 * date 2017-12-340
 * description CiipUsrSysRolesservice
 */
public interface CiipUsrSysRolesService {
    /**
     * 根据条件查询一个
     *
     * @param ciipUsrSysRoles
     * @return CiipUsrSysRoles
     */
    public CiipUsrSysRoles selectOne(CiipUsrSysRoles ciipUsrSysRoles);

    /**
     * 根据id查询
     *
     * @param id
     * @return CiipUsrSysRoles
     */
    public CiipUsrSysRoles get(String id);

    /**
     * 根据roleCode查询
     *
     * @param roleCode
     * @return CiipUsrSysRoles
     */
    public CiipUsrSysRoles getByRoleCode(String roleCode);

    /**
     * 查询符合条件的数据,不分页
     *
     * @param ciipUsrSysRoles
     * @return List<CiipUsrSysRoles>
     */
    public List<CiipUsrSysRoles> findList(CiipUsrSysRoles ciipUsrSysRoles);

    /**
     * 查询所有数据
     *
     * @return List<CiipUsrSysRoles>
     */
    public List<CiipUsrSysRoles> findAll();

    /**
     * 查询符合条件的数据,并且分页,默认pageNum=1,pageSize=100
     *
     * @param ciipUsrSysRoles
     * @return PageInfo<CiipUsrSysRoles>
     */
    public PageInfo<CiipUsrSysRoles> find(CiipUsrSysRoles ciipUsrSysRoles);

    /**
     * 保存
     *
     * @param ciipUsrSysRoles
     * @return CiipUsrSysRoles
     */
    public CiipUsrSysRoles save(CiipUsrSysRoles ciipUsrSysRoles);

    /**
     * 删除符合条件的数据
     *
     * @param ciipUsrSysRoles
     * @return int 删除条数
     */
    public int delete(CiipUsrSysRoles ciipUsrSysRoles);

}