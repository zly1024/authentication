package com.zly.user.service.impl;

import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zly.common.domain.BaseDomain;
import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.user.mappers.CiipUsrSysRolesMapper;
import com.zly.user.service.CiipUsrSysRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * date 2017-12-340
 * description CiipUsrSysRolesservice
 */
@Service
public class CiipUsrSysRolesServiceImpl implements CiipUsrSysRolesService {
    @Autowired
    private CiipUsrSysRolesMapper ciipUsrSysRolesMapper;

    @Override
    public List<CiipUsrSysRoles> findList(CiipUsrSysRoles ciipUsrSysRoles) {
        return ciipUsrSysRolesMapper.select(ciipUsrSysRoles);
    }

    @Override
    public CiipUsrSysRoles selectOne(CiipUsrSysRoles ciipUsrSysRoles) {
        return ciipUsrSysRolesMapper.selectOne(ciipUsrSysRoles);
    }

    @Override
    public CiipUsrSysRoles get(String id) {
        CiipUsrSysRoles ciipUsrSysRoles = new CiipUsrSysRoles();
        ciipUsrSysRoles.setId(id);
        return ciipUsrSysRolesMapper.selectByPrimaryKey(ciipUsrSysRoles);
    }

    @Override
    public CiipUsrSysRoles getByRoleCode(String roleCode) {
        CiipUsrSysRoles ciipUsrSysRoles = new CiipUsrSysRoles();
        ciipUsrSysRoles.setRoleCode(roleCode);
        return ciipUsrSysRolesMapper.selectOne(ciipUsrSysRoles);
    }

    @Override
    public List<CiipUsrSysRoles> findAll() {
        return ciipUsrSysRolesMapper.selectAll();
    }

    @Override
    public PageInfo<CiipUsrSysRoles> find(final CiipUsrSysRoles ciipUsrSysRoles) {
        if (ciipUsrSysRoles.getPageNum() == null) {
            ciipUsrSysRoles.setPageNum(BaseDomain.DEFALUT_PAGE_NUM);
        }
        if (ciipUsrSysRoles.getPageSize() == null) {
            ciipUsrSysRoles.setPageSize(BaseDomain.DEFALUT_PAGE_SIZE);
        }
        return PageHelper.startPage(ciipUsrSysRoles.getPageNum(), ciipUsrSysRoles.getPageSize()).setOrderBy(ciipUsrSysRoles.getOrderBy()).doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                ciipUsrSysRolesMapper.select(ciipUsrSysRoles);
            }
        });
    }

    @Override
    public CiipUsrSysRoles save(CiipUsrSysRoles ciipUsrSysRoles) {
        if (StringUtils.isEmpty(ciipUsrSysRoles.getId())) {
            ciipUsrSysRolesMapper.insert(ciipUsrSysRoles);
        } else {
            ciipUsrSysRolesMapper.updateByPrimaryKeySelective(ciipUsrSysRoles);
        }
        return ciipUsrSysRoles;
    }

    @Override
    public int delete(CiipUsrSysRoles ciipUsrSysRoles) {
        return ciipUsrSysRolesMapper.delete(ciipUsrSysRoles);
    }
}