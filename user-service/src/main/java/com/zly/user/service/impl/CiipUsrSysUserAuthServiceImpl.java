package com.zly.user.service.impl;

import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zly.common.domain.BaseDomain;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.user.mappers.CiipUsrSysUserAuthMapper;
import com.zly.user.service.CiipUsrSysUserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * date 2017-12-340
 * description CiipUsrSysUserAuthservice
 */
@Service
public class CiipUsrSysUserAuthServiceImpl implements CiipUsrSysUserAuthService {
    @Autowired
    private CiipUsrSysUserAuthMapper ciipUsrSysUserAuthMapper;

    @Override
    public List<CiipUsrSysUserAuth> findList(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return ciipUsrSysUserAuthMapper.select(ciipUsrSysUserAuth);
    }

    @Override
    public CiipUsrSysUserAuth selectOne(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return ciipUsrSysUserAuthMapper.selectOne(ciipUsrSysUserAuth);
    }

    @Override
    public CiipUsrSysUserAuth get(String id) {
        CiipUsrSysUserAuth ciipUsrSysUserAuth = new CiipUsrSysUserAuth();
        ciipUsrSysUserAuth.setId(id);
        return ciipUsrSysUserAuthMapper.selectByPrimaryKey(ciipUsrSysUserAuth);
    }

    @Override
    public List<CiipUsrSysUserAuth> findAll() {
        return ciipUsrSysUserAuthMapper.selectAll();
    }

    @Override
    public PageInfo<CiipUsrSysUserAuth> find(final CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        if (ciipUsrSysUserAuth.getPageNum() == null)
            ciipUsrSysUserAuth.setPageNum(BaseDomain.DEFALUT_PAGE_NUM);
        if (ciipUsrSysUserAuth.getPageSize() == null)
            ciipUsrSysUserAuth.setPageSize(BaseDomain.DEFALUT_PAGE_SIZE);
        return PageHelper.startPage(ciipUsrSysUserAuth.getPageNum(), ciipUsrSysUserAuth.getPageSize()).setOrderBy(ciipUsrSysUserAuth.getOrderBy()).doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                ciipUsrSysUserAuthMapper.select(ciipUsrSysUserAuth);
            }
        });
    }

    @Override
    public CiipUsrSysUserAuth save(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        if (StringUtils.isEmpty(ciipUsrSysUserAuth.getId())) {
            ciipUsrSysUserAuthMapper.insert(ciipUsrSysUserAuth);
        } else {
            ciipUsrSysUserAuthMapper.updateByPrimaryKeySelective(ciipUsrSysUserAuth);
        }
        return ciipUsrSysUserAuth;
    }

    @Override
    public int delete(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return ciipUsrSysUserAuthMapper.delete(ciipUsrSysUserAuth);
    }
}