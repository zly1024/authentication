package com.zly.user.service.impl;

import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.user.mappers.CiipUsrPrivilegesMapper;
import com.zly.user.service.CiipUsrPrivilegesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author : zly
 * date   : 2017/7/19.
 * description :
 */
@Service
public class CiipUsrPrivilegesServiceImpl implements CiipUsrPrivilegesService {
    @Autowired
    private CiipUsrPrivilegesMapper ciipUsrPrivilegesMapper;

    @Override
    public List<CiipUsrSysRoles> findUsrSysRoles(String userId) {
        return ciipUsrPrivilegesMapper.findUsrSysRoles(userId);
    }

    @Override
    public List<CiipUsrSysUserAuth> findUsrSysUserAuth(String userId) {
        return ciipUsrPrivilegesMapper.findUsrSysUserAuth(userId);
    }
}
