package com.zly.user.service;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;

import java.util.List;

/**
 * date 2017-12-340
 * description CiipUsrSysUserAuthservice
 */
public interface CiipUsrSysUserAuthService {
    /**
     * 根据条件查询一个
     *
     * @param ciipUsrSysUserAuth
     * @return CiipUsrSysUserAuth
     */
    public CiipUsrSysUserAuth selectOne(CiipUsrSysUserAuth ciipUsrSysUserAuth);

    /**
     * 根据id查询
     *
     * @param id
     * @return CiipUsrSysUserAuth
     */
    public CiipUsrSysUserAuth get(String id);

    /**
     * 查询符合条件的数据,不分页
     *
     * @param ciipUsrSysUserAuth
     * @return List<CiipUsrSysUserAuth>
     */
    public List<CiipUsrSysUserAuth> findList(CiipUsrSysUserAuth ciipUsrSysUserAuth);

    /**
     * 查询所有数据
     *
     * @return List<CiipUsrSysUserAuth>
     */
    public List<CiipUsrSysUserAuth> findAll();

    /**
     * 查询符合条件的数据,并且分页,默认pageNum=1,pageSize=100
     *
     * @param ciipUsrSysUserAuth
     * @return PageInfo<CiipUsrSysUserAuth>
     */
    public PageInfo<CiipUsrSysUserAuth> find(CiipUsrSysUserAuth ciipUsrSysUserAuth);

    /**
     * 保存
     *
     * @param ciipUsrSysUserAuth
     * @return CiipUsrSysUserAuth
     */
    public CiipUsrSysUserAuth save(CiipUsrSysUserAuth ciipUsrSysUserAuth);

    /**
     * 删除符合条件的数据
     *
     * @param ciipUsrSysUserAuth
     * @return int 删除条数
     */
    public int delete(CiipUsrSysUserAuth ciipUsrSysUserAuth);
}