/*
 * #{copyright}#
 */
package com.zly.user.service.impl;

import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zly.common.constants.Constant;
import com.zly.common.constants.UserRoleType;
import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.usr.UsrUsers;
import com.zly.common.util.PasswordUtil;
import com.zly.user.exception.UserException;
import com.zly.user.mappers.CiipUsrSysRolesMapper;
import com.zly.user.mappers.CiipUsrSysUserAuthMapper;
import com.zly.user.mappers.CiipUsrUsersMapper;
import com.zly.user.service.CiipUsrUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @author zly
 */
@SuppressWarnings("ALL")
@Service
public class CiipUsrUserServiceImpl implements CiipUsrUserService {

    @Autowired
    private CiipUsrUsersMapper ciipUsrUsersMapper;

    @Autowired
    private CiipUsrSysRolesMapper ciipUsrSysRolesMapper;

    @Autowired
    private CiipUsrSysUserAuthMapper ciipUsrSysUserAuthMapper;

    @Override
    public CiipUsrUsers getCiipUsrUser(CiipUsrUsers ciipUsrUsers) {
        return ciipUsrUsersMapper.selectOne(ciipUsrUsers);
    }

    @Override
    public CiipUsrUsers getUserById(String id) {
        CiipUsrUsers query = new CiipUsrUsers();
        query.setId(id);
        return ciipUsrUsersMapper.selectByPrimaryKey(query);
    }

    @Override
    public CiipUsrUsers insert(CiipUsrUsers ciipUsrUsers) {

        if (!validate(ciipUsrUsers)) {
            throw new UserException(UserException.EXCEPTION_USER_DUPLICATED, "usr.service.validate.user.duplicated");
        }

        // for test
        //System.out.println("ciipUsrUsers.getId(): " + ciipUsrUsers.getId());
        //System.out.println("ciipUsrUsers.getUserName(): " + ciipUsrUsers.getUserName());
        //System.out.println("ciipUsrUsers.getPassword(): " + ciipUsrUsers.getPassword());

        // 对密码进行加密
        ciipUsrUsers.setPassword(PasswordUtil.encodePassword(ciipUsrUsers.getPassword(), ciipUsrUsers.getUserName()));
        ciipUsrUsers.setRegistrationDate(new Date());
        ciipUsrUsersMapper.insert(ciipUsrUsers);
        return ciipUsrUsers;
    }
//
//    @Override
//    public List<CiipUserConnInf> selectUserConnInfo(CiipUserConnInf ciipUserConnInf) {
//        return ciipUsrUsersMapper.selectUserConnInfo(ciipUserConnInf);
//    }
//
//    @Override
//    public boolean addShopUser(CiipUsrUsers ciipUsrUsers) {
//        int i = ciipUsrUsersMapper.validateUser(ciipUsrUsers);
//        if (i > 1) {
//            throw new UserException(UserException.EXCEPTION_USER_DUPLICATED, "usr.service.validate.user.duplicated");
//        }
//        ciipUsrUsers.setPassword(PasswordUtil.encodePassword(ciipUsrUsers.getPassword(), ciipUsrUsers.getUserName()));
//        return ciipUsrUsersMapper.insert(ciipUsrUsers) > 1;
//    }
//
//    @Override
//    public boolean approvalShopUser(CiipUsrUsers ciipUsrUsers) {
//        CiipUsrUsers tmp = new CiipUsrUsers();
//        tmp.setId(ciipUsrUsers.getId());
//        tmp.setAccountStatusCode(ciipUsrUsers.getAccountStatusCode());
//        return ciipUsrUsersMapper.updateByPrimaryKeySelective(tmp) > 1;
//    }

    @Override
    public PageInfo<CiipUsrUsers> query(final CiipUsrUsers ciipUsrUsers) {
        return PageHelper.startPage(ciipUsrUsers.getPageNum(), ciipUsrUsers.getPageSize()).setOrderBy(ciipUsrUsers.getOrderBy()).doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                ciipUsrUsersMapper.select(ciipUsrUsers);
            }
        });
    }

    @Override
    public CiipUsrUsers save(CiipUsrUsers usrUsers) {
        if (StringUtils.isEmpty(usrUsers.getId())) {
            if (!validate(usrUsers)) {
                throw new UserException(UserException.EXCEPTION_USER_DUPLICATED, "usr.service.validate.user.duplicated");
            }
            // 对密码进行加密
            usrUsers.setPassword(PasswordUtil.encodePassword(usrUsers.getPassword(), usrUsers.getUserName()));
            usrUsers.setRegistrationDate(new Date());
            ciipUsrUsersMapper.insert(usrUsers);
        } else {
            ciipUsrUsersMapper.updateByPrimaryKeySelective(usrUsers);
        }
        return usrUsers;
    }


    // 校验用户是否重复?
    private boolean validate(CiipUsrUsers ciipUsrUsers) {
        int count = ciipUsrUsersMapper.validateUser(ciipUsrUsers);
        return count == 0;
    }


    /**
     * 保存用户以及用户权限,角色等相关数据
     *
     * @param usrUsers
     * @return
     */
    @Override
    public UsrUsers saveSummary(UsrUsers usrUsers) {
        if (StringUtils.isEmpty(usrUsers.getId())) {
            if (!validate(usrUsers)) {
                throw new UserException(UserException.EXCEPTION_USER_DUPLICATED, "usr.service.validate.user.duplicated");
            }
            // 对密码进行加密
            usrUsers.setPassword(PasswordUtil.encodePassword(usrUsers.getPassword(), usrUsers.getUserName()));
            usrUsers.setRegistrationDate(new Date());
            ciipUsrUsersMapper.insert(usrUsers);
        } else {
            usrUsers.setPassword(PasswordUtil.encodePassword(usrUsers.getPassword(), usrUsers.getUserName()));
            ciipUsrUsersMapper.updateByPrimaryKeySelective(usrUsers);
        }
        if (!usrUsers.getCiipUsrSysUserAuth().isEmpty()) {
            for (CiipUsrSysUserAuth ciipUsrSysUserAuth : usrUsers.getCiipUsrSysUserAuth()) {
                ciipUsrSysUserAuth.setUserId(usrUsers.getId());
                if (StringUtils.isEmpty(ciipUsrSysUserAuth.getId())) {
                    //ciipUsrSysUserAuth.setSourceRefId(usrUsers.getId());
                    //ciipUsrSysUserAuth.setSourceRefId("");
                    if (StringUtils.isEmpty(ciipUsrSysUserAuth.getIsExcludeFlag())) {
                        ciipUsrSysUserAuth.setIsExcludeFlag("N");
                    }
                    if (StringUtils.isEmpty(ciipUsrSysUserAuth.getEnabledFlag())) {
                        ciipUsrSysUserAuth.setEnabledFlag("Y");
                    }
                    Date date = new Date();
                    if (ciipUsrSysUserAuth.getStartDate() == null) {
                        ciipUsrSysUserAuth.setStartDate(date);
                        ciipUsrSysUserAuth.setCreationDate(date);
                    }
                    ciipUsrSysUserAuth.setLastUpdateDate(date);
                    ciipUsrSysUserAuthMapper.insert(ciipUsrSysUserAuth);
                } else {
                    ciipUsrSysUserAuth.setUserId(usrUsers.getId());
                    ciipUsrSysUserAuthMapper.updateByPrimaryKeySelective(ciipUsrSysUserAuth);
                }
            }
        }
        return usrUsers;
    }

    @Override
    public Integer delete(CiipUsrUsers usrUsers) {
        return ciipUsrUsersMapper.delete(usrUsers);
    }


    /**
     * 用于审核商家通过后更新用户,角色
     *
     * @param ciipUsrUsers
     * @return
     */
    @Override
    @Transactional
    public boolean approvalShopUser(CiipUsrUsers ciipUsrUsers) {
        //更新User
        ciipUsrUsersMapper.updateByPrimaryKeySelective(ciipUsrUsers);

        //查询所有的角色
        List<CiipUsrSysRoles> roles = ciipUsrSysRolesMapper.selectAll();
        String companyIegisterId = "";
        String compayAdminId = "";
        for (CiipUsrSysRoles temp : roles) {
            if (!StringUtils.isEmpty(companyIegisterId) && !StringUtils.isEmpty(companyIegisterId)) {
                break;
            } else {
                if (UserRoleType.REG_TEMP_ROLE.equals(temp.getRoleCode())) {
                    companyIegisterId = temp.getId();
                }
                if (UserRoleType.COMP_ADMIN_ROLE.equals(temp.getRoleCode())) {
                    compayAdminId = temp.getId();
                }
            }
        }
        //失效临时角色
        CiipUsrSysUserAuth ciipUsrSysUserAuth = new CiipUsrSysUserAuth();
        ciipUsrSysUserAuth.setUserId(ciipUsrUsers.getId());
        ciipUsrSysUserAuth.setSourceRefId(companyIegisterId);
        //获取id
        ciipUsrSysUserAuth = ciipUsrSysUserAuthMapper.selectOne(ciipUsrSysUserAuth);
        ciipUsrSysUserAuth.setEnabledFlag("N");
        ciipUsrSysUserAuth.setEndDate(new Date());
        int result = ciipUsrSysUserAuthMapper.updateByPrimaryKeySelective(ciipUsrSysUserAuth);

        //添加COMP_ADMIN角色
        CiipUsrSysUserAuth newAuth = new CiipUsrSysUserAuth();
        newAuth.setUserId(ciipUsrUsers.getId());
        newAuth.setSourceRefCode("ROLE");
        newAuth.setSourceRefId(compayAdminId);
        newAuth.setIsExcludeFlag("N");
        newAuth.setEnabledFlag("Y");
        newAuth.setApplicationId(Constant.DEFAULT_APPLICATION_ID);
        Date date = new Date();
        newAuth.setStartDate(date);
        newAuth.setCreationDate(date);
        newAuth.setLastUpdateDate(date);
        ciipUsrSysUserAuthMapper.insert(newAuth);
        return true;
    }
}
