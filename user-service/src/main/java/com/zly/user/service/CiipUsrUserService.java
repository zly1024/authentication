/*
 * #{copyright}#
 */
package com.zly.user.service;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.usr.UsrUsers;

/**
 * @author zly
 */
public interface CiipUsrUserService {
    /**
     * @param ciipUsrUsers
     * @return
     */
    CiipUsrUsers getCiipUsrUser(CiipUsrUsers ciipUsrUsers);

    CiipUsrUsers getUserById(String id);

    CiipUsrUsers insert(CiipUsrUsers ciipUsrUsers);
//
//    /**
//     * 查询用户关联信息
//     * 查询id,user_role_code,source_sys_ref_id,source_sys_ref_code
//     *
//     * @param ciipUserConnInf 查询条件,根据是否为空进行条件拼接
//     * @return
//     */
//    List<CiipUserConnInf> selectUserConnInfo(CiipUserConnInf ciipUserConnInf);
//
//    /**
//     * 注册新商家用户
//     *
//     * @param ciipUsrUsers
//     * @return
//     */
//    boolean addShopUser(CiipUsrUsers ciipUsrUsers);
//
//    /**
//     * 审核商家用户
//     *
//     * @param ciipUsrUsers
//     * @return
//     */
//    boolean approvalShopUser(CiipUsrUsers ciipUsrUsers);

    PageInfo<CiipUsrUsers> query(CiipUsrUsers ciipUsrUsers);

    CiipUsrUsers save(CiipUsrUsers usrUsers);

    UsrUsers saveSummary(UsrUsers usrUsers);

    Integer delete(CiipUsrUsers usrUsers);

    boolean approvalShopUser(CiipUsrUsers ciipUsrUsers);
}
