package com.zly.user.service;

import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;

import java.util.List;

/**
 * author : zly
 * date   : 2017/7/19.
 * description :
 */
public interface CiipUsrPrivilegesService {
    List<CiipUsrSysRoles> findUsrSysRoles(String userId);

    List<CiipUsrSysUserAuth> findUsrSysUserAuth(String userId);
}
