package com.zly.user.mappers;


import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * author : zly
 * date   : 2017/7/19.
 * description :
 */
public interface CiipUsrPrivilegesMapper {

    List<CiipUsrSysRoles> findUsrSysRoles(@Param(value = "userId") String userId);

    List<CiipUsrSysUserAuth> findUsrSysUserAuth(@Param(value = "userId") String userId);

}
