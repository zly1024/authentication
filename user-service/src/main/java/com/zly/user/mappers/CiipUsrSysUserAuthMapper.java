package com.zly.user.mappers;


import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import tk.mybatis.mapper.common.Mapper;

public interface CiipUsrSysUserAuthMapper extends Mapper<CiipUsrSysUserAuth> {
}