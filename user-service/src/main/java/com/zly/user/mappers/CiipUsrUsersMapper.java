package com.zly.user.mappers;

import com.zly.common.domain.usr.CiipUsrUsers;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author dezhi.shen
 */
public interface CiipUsrUsersMapper extends Mapper<CiipUsrUsers> {

    Integer validateUser(CiipUsrUsers ciipUsrUsers);
}