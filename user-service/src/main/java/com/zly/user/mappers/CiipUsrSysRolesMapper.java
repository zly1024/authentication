package com.zly.user.mappers;

import com.zly.common.domain.usr.CiipUsrSysRoles;
import tk.mybatis.mapper.common.Mapper;

public interface CiipUsrSysRolesMapper extends Mapper<CiipUsrSysRoles> {
}