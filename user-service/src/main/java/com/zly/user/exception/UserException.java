/*
 * #{copyright}#
 */
package com.zly.user.exception;


import com.zly.common.exception.MultiLangException;

/**
 * @author zly
 */
public class UserException extends MultiLangException {

    public static final String EXCEPTION_USER_DUPLICATED = "com.zly.user.duplicated";

    public UserException(String code, String message) {
        super(code, message);
    }
}
