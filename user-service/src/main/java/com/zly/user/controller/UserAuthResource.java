package com.zly.user.controller;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.common.dto.BooleanObject;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.user.service.CiipUsrSysUserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/userAuth")
public class UserAuthResource extends BaseController {
    @Autowired
    private CiipUsrSysUserAuthService ciipUsrSysUserAuthService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<CiipUsrSysUserAuth> get(String id) {
        return success(ciipUsrSysUserAuthService.get(id));
    }

    @RequestMapping(value = "/selectOne", method = RequestMethod.GET)
    public ResponseEntity<CiipUsrSysUserAuth> selectOne(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return success(ciipUsrSysUserAuthService.selectOne(ciipUsrSysUserAuth));
    }

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ResponseEntity<PageInfo<CiipUsrSysUserAuth>> query(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return success(ciipUsrSysUserAuthService.find(ciipUsrSysUserAuth));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<CiipUsrSysUserAuth>> list(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return success(ciipUsrSysUserAuthService.findList(ciipUsrSysUserAuth));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<CiipUsrSysUserAuth> save(@RequestBody CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return success(ciipUsrSysUserAuthService.save(ciipUsrSysUserAuth));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ResponseEntity<BooleanObject> delete(String id) {
        CiipUsrSysUserAuth userAuth = new CiipUsrSysUserAuth();
        userAuth.setId(id);
        return success(new BooleanObject(ciipUsrSysUserAuthService.delete(userAuth) > 0));
    }
}