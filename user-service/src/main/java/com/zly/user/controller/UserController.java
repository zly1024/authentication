/*
 * #{copyright}#
 */
package com.zly.user.controller;

import com.github.pagehelper.StringUtil;
import com.zly.client.constants.UserErrorsEnum;
import com.zly.client.usr.UserResourceClient;
import com.zly.client.util.AuthHelper;
import com.zly.common.dto.AuthRespDto;
import com.zly.common.dto.BooleanObject;
import com.zly.common.exception.MultiLangException;
import com.zly.common.web.BaseAuthController;
import com.zly.common.web.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zly
 */
@RestController
@RequestMapping("/usr")
public class UserController extends BaseAuthController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserResourceClient userResourceClient;
    @Autowired
    private AuthHelper authHelper;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ResponseEntity login(String code, String redirect_uri) {
        if (StringUtil.isEmpty(code)) {
            throw UserErrorsEnum.ERROR_USER_LOGIN_CODE_IS_NULL.exception();
        }

        Map<String, Object> result = new HashMap<>();
        AuthRespDto authRespDto = authHelper.getAccessTokenByCode(code, redirect_uri);
        result.put("access_token", authRespDto.getAccess_token());
        result.put("token_type", authRespDto.getToken_type());
        result.put("refresh_token", authRespDto.getRefresh_token());
        result.put("expires_in", authRespDto.getExpires_in());
        result.put("scope", authRespDto.getScope());
        result.put("jti", authRespDto.getJti());
        return success(result);
    }

    @RequestMapping(value = "/refreshToken")
    public ResponseEntity refreshToken(@RequestParam(value = "refresh_token")String refreshToken) {
        if (StringUtil.isEmpty(refreshToken)) {
            throw UserErrorsEnum.ERROR_USER_REFRESH_TOKEN_IS_NULL.exception();
        }

        Map<String, Object> result = new HashMap<>();
        AuthRespDto authRespDto = authHelper.refreshAccessToken(refreshToken);
        if (!StringUtils.isEmpty(authRespDto.getError()) && !StringUtils.isEmpty(authRespDto.getError_description())) {
            throw new MultiLangException(UserErrorsEnum.ERROR_USER_REFRESH_TOKEN_ERR.getErrorCode(),
                    UserErrorsEnum.ERROR_USER_REFRESH_TOKEN_ERR.getMessageCode(),
                    new String[]{authRespDto.getError()});
        }
        result.put("access_token", authRespDto.getAccess_token());
        result.put("token_type", authRespDto.getToken_type());
        result.put("refresh_token", authRespDto.getRefresh_token());
        result.put("expires_in", authRespDto.getExpires_in());
        result.put("scope", authRespDto.getScope());
        result.put("jti", authRespDto.getJti());
        return success(result);
    }

    @RequestMapping(value="/logout")
    public ResponseEntity logout(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        return success(authHelper.revokeAccessToken(authorization));
    }

    @RequestMapping(value = "/now/get")
    public ResponseEntity getNowUser() {
        return success(userResourceClient.get(getUserId()));
    }

    @RequestMapping(value = "/store/change")
    public ResponseEntity changeStore(String storeId) {
        return success(new BooleanObject(true));
    }
}
