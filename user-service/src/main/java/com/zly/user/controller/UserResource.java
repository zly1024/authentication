/*
 * #{copyright}#
 */
package com.zly.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.zly.common.constants.Constant;
import com.zly.common.constants.UserType;
import com.zly.common.domain.BaseDomain;
import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.BooleanObject;
import com.zly.common.dto.usr.UserRoleCode;
import com.zly.common.dto.usr.UsrUsers;
import com.zly.common.security.User;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.user.service.CiipUsrPrivilegesService;
import com.zly.user.service.CiipUsrSysRolesService;
import com.zly.user.service.CiipUsrSysUserAuthService;
import com.zly.user.service.CiipUsrUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author zly
 */
@SuppressWarnings("ALL")
@RestController
@RequestMapping("/user")
public class UserResource extends BaseController {
    @Autowired
    private CiipUsrUserService userService;

    @Autowired
    private CiipUsrPrivilegesService ciipUsrPrivilegesService;
    @Autowired
    private CiipUsrSysRolesService ciipUsrSysRolesService;

    @Autowired
    private CiipUsrSysUserAuthService ciipUsrSysUserAuthService;

    @RequestMapping(value = "/oAuth/get", method = RequestMethod.GET)
    public User oAuthGet(CiipUsrUsers user) {
        //从数据库表获取user信息
        CiipUsrUsers ciipUsrUser = userService.getCiipUsrUser(user);
        if (ciipUsrUser == null) {
            return null;
        }
        HashSet<GrantedAuthority> authoritySet = new HashSet<>();
        List<CiipUsrSysRoles> roles = ciipUsrPrivilegesService.findUsrSysRoles(ciipUsrUser.getId());
        if (roles != null) {
            for (CiipUsrSysRoles r : roles) {
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_" + r.getRoleCode());
                authoritySet.add(grantedAuthority);
            }
        }
        User loginUser = new User(ciipUsrUser.getId(), ciipUsrUser.getUserName(), ciipUsrUser.getPassword(), authoritySet);
        //用户类型
        loginUser.addAttribute("userType", ciipUsrUser.getSourceSysRefCode());
        if (UserType.COMPANY.equals(ciipUsrUser.getSourceSysRefCode())) {
            loginUser.addAttribute("companyId", ciipUsrUser.getSourceSysRefId());
        } else if (UserType.STORE.equals(ciipUsrUser.getSourceSysRefCode())) {
            loginUser.addAttribute("storeId", ciipUsrUser.getSourceSysRefId());
        }
        if (UserType.EMP.equals(ciipUsrUser.getSourceSysRefCode())) {
            loginUser.addAttribute("empId", ciipUsrUser.getSourceSysRefId());
        }
        loginUser.addAttribute("storeId", ciipUsrUser.getStoreId());
        loginUser.addAttribute("companyId", ciipUsrUser.getCompanyId());

        //用户有效性控制
        if ("I".equals(ciipUsrUser.getAccountStatusCode())) {
            loginUser.setAccountNonExpired(false);
        } else if ("L".equals(ciipUsrUser.getAccountStatusCode())) {
            loginUser.setAccountNonLocked(false);
        }
        Date now = new Date();
        if ((ciipUsrUser.getStartDate() != null && ciipUsrUser.getStartDate().getTime() > now.getTime()) ||
                (ciipUsrUser.getEndDate() != null && ciipUsrUser.getEndDate().getTime() < now.getTime())) {
            loginUser.setEnabled(false);
        }

        return loginUser;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<CiipUsrUsers> get(String id) {
        return success(userService.getUserById(id));
    }

    @RequestMapping(value = "/selectOne", method = RequestMethod.GET)
    public ResponseEntity<CiipUsrUsers> selectOne(CiipUsrUsers ciipUsrUsers) {
        return success(userService.getCiipUsrUser(ciipUsrUsers));
    }

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ResponseEntity<PageInfo<CiipUsrUsers>> query(CiipUsrUsers ciipUsrUsers) {
        if (ciipUsrUsers.getPageNum() == null) {
            ciipUsrUsers.setPageNum(BaseDomain.DEFALUT_PAGE_NUM);
        }
        if (ciipUsrUsers.getPageSize() == null) {
            ciipUsrUsers.setPageSize(BaseDomain.DEFALUT_PAGE_SIZE);
        }
        PageInfo<CiipUsrUsers> result = userService.query(ciipUsrUsers);
        return success(result);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<CiipUsrUsers> insert(@RequestBody CiipUsrUsers ciipUsrUsers) {
        userService.insert(ciipUsrUsers);
        return success(ciipUsrUsers);
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<CiipUsrUsers> save(@RequestBody CiipUsrUsers usrUsers) {
        return success(userService.save(usrUsers));
    }

//    /**
//     * 查询用户关联信息 查询id,user_role_code,source_sys_ref_id,source_sys_ref_cod
//     *
//     * @param ciipUserConnInf
//     * @return
//     */
//    @RequestMapping(value = "/queryUserConnInfo", method = RequestMethod.GET)
//    public ResponseEntity selectUserConnInfo(CiipUserConnInf ciipUserConnInf) {
//        List<CiipUserConnInf> users = userService.selectUserConnInfo(ciipUserConnInf);
//        return success(users.get(0));
//    }
//
//    /**
//     * 新增商户
//     */
//    @RequestMapping(value = "/addShopUser", method = RequestMethod.POST)
//    public ResponseEntity addShopUser(CiipUsrUsers ciipUsrUsers) {
//        if (userService.addShopUser(ciipUsrUsers))
//            return success(ciipUsrUsers);
//        else {
//            ResponseEntity responseEntity = new ResponseEntity();
//            responseEntity.setCode(ResponseState.ERROR.getValue());
//            responseEntity.setMsg("注册失败!");
//            return responseEntity;
//        }
//    }
//
//    /**
//     * 审核商户
//     */
//    @RequestMapping(value = "/approvalShopUser", method = RequestMethod.POST)
//    public ResponseEntity approvalShopUser(CiipUsrUsers ciipUsrUsers) {
//        ResponseEntity responseEntity = new ResponseEntity();
//        userService.approvalShopUser(ciipUsrUsers);
//        responseEntity.setData(ciipUsrUsers);
//        return responseEntity;
//    }

    @RequestMapping(value = "/delete")
    public ResponseEntity<BooleanObject> delete(CiipUsrUsers usrUsers) {
        return success(new BooleanObject(userService.delete(usrUsers) > 0));
    }

    /**
     * 失效用户，不进行删除
     *
     * @param usrUsers
     * @return
     */
    @RequestMapping(value = "/disable")
    public ResponseEntity<CiipUsrUsers> disable(CiipUsrUsers usrUsers) {
        CiipUsrUsers ciipUsrUsers = userService.getCiipUsrUser(usrUsers);
        ciipUsrUsers.setAccountStatusCode("I");
        ciipUsrUsers.setEndDate(new Date());
        return success(userService.save(ciipUsrUsers));
    }


    @RequestMapping(value = "/saveSummary", method = RequestMethod.POST)
    public ResponseEntity<UsrUsers> saveSummary(@RequestBody UsrUsers usrUsers) {
        return success(userService.saveSummary(usrUsers));
    }

    /**
     * 用户注册时，添加默认的USR角色权限
     *
     * @param userRoleCode
     * @return
     */
    @RequestMapping(value = "/addDefaultAuth", method = RequestMethod.POST)
    public ResponseEntity<UserRoleCode> addDefaultAuth(@RequestBody UserRoleCode userRoleCode) {
        String userId = userRoleCode.getUserId();  //用户ID
        String[] roleCodeList = userRoleCode.getRoleCodes().split(",");
        for (String roleCode : roleCodeList) {
            //根据roleCode获取ciip_usr_sys_roles表的记录
            CiipUsrSysRoles ciipUsrSysRoles = ciipUsrSysRolesService.getByRoleCode(roleCode);
            if (ciipUsrSysRoles == null) {
                return fail("Role code [" + roleCode + "] not exists!");
            }
            CiipUsrSysUserAuth ciipUsrSysUserAuth = new CiipUsrSysUserAuth();
            ciipUsrSysUserAuth.setUserId(userId);
            ciipUsrSysUserAuth.setSourceRefCode("ROLE");
            ciipUsrSysUserAuth.setSourceRefId(ciipUsrSysRoles.getId());  //角色表ID
            ciipUsrSysUserAuth.setIsExcludeFlag("N");
            ciipUsrSysUserAuth.setApplicationId(Constant.DEFAULT_APPLICATION_ID);
            ciipUsrSysUserAuth.setEnabledFlag("Y");
            ciipUsrSysUserAuth.setStartDate(new Date());
            //插入用户权限表
            ciipUsrSysUserAuthService.save(ciipUsrSysUserAuth);
        }

        return success(userRoleCode);
    }


    //用于商家认证审核通过更新User,添加新的角色,失效临时的角色
    @RequestMapping(value = "/approvalShopUser")
    public ResponseEntity approvalShopUser(UsrUsers usrUsers) {
        usrUsers.setSourceSysRefCode(UserType.COMPANY);
        usrUsers.setSourceSysRefId(usrUsers.getCompanyId());
        return success(new BooleanObject(userService.approvalShopUser(usrUsers)));
    }


    @Autowired
    RestTemplate restTemplate;

    //测试 token服务间传递
    @RequestMapping(value = "/test/get")
    public ResponseEntity test(String userId) {
        ResponseEntity result = restTemplate.postForObject("http://localhost:7002/message/test/get",
                null,ResponseEntity.class);
        System.out.println("----result-----------"+ JSONObject.toJSONString(result));
        return success("调用成功");
    }
}
