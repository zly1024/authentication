package com.zly.user.controller;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.user.service.CiipUsrSysRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * author : zly
 * date   : 2017/7/19.
 * description :
 */
@RestController
@SuppressWarnings("ALL")
public class RoleResource extends BaseController {
    @Autowired
    private CiipUsrSysRolesService roleService;

    @PreAuthorize("hasRole('ROLE_SYSADMIN')")
    @RequestMapping(value = "/role/get", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<CiipUsrSysRoles> get(String id) {
        return success(roleService.get(id));
    }

    @RequestMapping(value = "/role/list", method = RequestMethod.GET)
    public ResponseEntity<List<CiipUsrSysRoles>> list(CiipUsrSysRoles ciipUsrSysRoles) {
        return success(roleService.findList(ciipUsrSysRoles));
    }

    @PreAuthorize("hasRole('ROLE_REG_TEMP_ROLE')")
    @RequestMapping(value = "/role/query", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PageInfo<CiipUsrSysRoles>> query(CiipUsrSysRoles ciipUsrSysRoles) {
        return success(roleService.find(ciipUsrSysRoles));
    }

    @RequestMapping(value = "/role/save", method = RequestMethod.POST)
    public ResponseEntity<CiipUsrSysRoles> save(@RequestBody CiipUsrSysRoles ciipUsrSysRoles) {
        return success(roleService.save(ciipUsrSysRoles));
    }

    @RequestMapping(value = "/role/selectOne", method = RequestMethod.GET)
    public ResponseEntity<CiipUsrSysRoles> selectOne(CiipUsrSysRoles ciipUsrSysRoles) {
        return success(roleService.selectOne(ciipUsrSysRoles));
    }

}
