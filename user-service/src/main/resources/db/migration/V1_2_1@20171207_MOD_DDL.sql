alter table ciip_usr.ciip_usr_users add company_id varchar(50) default null comment '商家ID' after user_role_code;
alter table ciip_usr.ciip_usr_users add store_id varchar(50) default null comment '门店ID' after company_id;
alter table ciip_usr.ciip_usr_users add wx_unionid varchar(80) default null comment '微信unionid' after source_account_num;
create index ciip_usr_users_n6 on ciip_usr.ciip_usr_users(wx_unionid);