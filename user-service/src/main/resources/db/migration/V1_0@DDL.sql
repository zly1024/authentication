create database IF NOT EXISTS ciip_usr DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
create table ciip_usr.ciip_usr_users
(
  id varchar(50) not null comment '用户表ID',
  user_name varchar(80) not null comment '用户名（登录的用户名，可以为手机号／邮箱／设定的用户名）',
  password varchar(256) not null comment '密码',
  nick_name varchar(240) default null comment '昵称',
  full_name varchar(120) default null comment '姓名（全名）',
  area_code varchar(30) default null comment '区号',
  mobile_phone_num varchar(30) not null comment '手机号',
  telephone_num varchar(30) default null comment '固定电话',
  email varchar(80) default null comment '邮箱',
  user_role_code varchar(30) default null comment '用户角色CODE (保留字段，暂留空，根据角色表统一记录）',
  source_sys_ref_code varchar(30) default null comment '来源系统标识: USER,ORG,PLATFORM',
  source_sys_ref_id varchar(50) default null comment '来源系统ID',
  gender varchar(30) default null comment '性别(男M/女F/未知X）',
  birthday date default null comment '生日',
  language varchar(30) default null comment '语言',
  city varchar(30) default null comment '城市',
  province varchar(30) default null comment '省',
  country varchar(30) default null comment '国家',
  user_head_img_url varchar(240) default null comment '用户头像',
  rank int(11) comment '等级',
  source_account_type varchar(30) default null comment '账号授权类型（微信WX）',
  source_account_num varchar(80) default null comment '来源账号',
  source_account_type_zfb varchar(30) default null comment '账号授权类型2（支付宝ZFB）',
  source_account_num_zfb varchar(80) default null comment '来源账号2',
  registration_date datetime not null comment '账号注册日期',
  account_status_code varchar(30) not null comment '账户当前状态',
  start_date datetime not null comment '生效日期',
  end_date datetime default null comment '失效日期',
  description varchar(240) default null comment '描述',
  creation_date datetime,
  created_by varchar(50),
  last_update_date datetime,
  last_updated_by varchar(50),
  last_updated_login varchar(50),
  versions int(11),
  attribute_catrgory varchar(30),
  attribute1 varchar(240),
  attribute2 varchar(240),
  attribute3 varchar(240),
  attribute4 varchar(240),
  attribute5 varchar(240),
  attribute6 varchar(240),
  attribute7 varchar(240),
  attribute8 varchar(240),
  attribute9 varchar(240),
  attribute10 varchar(240),
  attribute11 varchar(240),
  attribute12 varchar(240),
  attribute13 varchar(240),
  attribute14 varchar(240),
  attribute15 varchar(240)
) comment '用户基础信息维护表';

alter table ciip_usr.ciip_usr_users add primary key(id);

create unique index ciip_usr_users_u1 on ciip_usr.ciip_usr_users(user_name);
create index ciip_usr_users_n1 on ciip_usr.ciip_usr_users(mobile_phone_num);
create index ciip_usr_users_n2 on ciip_usr.ciip_usr_users(source_sys_ref_id, source_sys_ref_code);
create index ciip_usr_users_n3 on ciip_usr.ciip_usr_users(email);
create index ciip_usr_users_n4 on ciip_usr.ciip_usr_users(source_account_num, source_account_type);
create index ciip_usr_users_n5 on ciip_usr.ciip_usr_users(source_account_num_zfb, source_account_type_zfb);

create table ciip_usr.ciip_usr_login_histories
(
  id varchar(50) not null comment '用户登录历史表ID',
  entered_user_name varchar(80) not null comment '登录所用用户名',
  entered_password varchar(256) default null comment '登录所用密码',
  associated_user_id varchar(50) default null comment '用户表ID',
  login_datetime datetime not null comment '登录时间',
  login_type_code varchar(30) not null comment '登录方式',
  logout_datetime datetime default null comment '退出／注销时间',
  login_IP varchar(50) default null comment '登陆IP',
  login_URL varchar(240) default null comment '登录链接URL',
  login_status_code varchar(30) default null comment '登录状态',
  login_error_message varchar(2000) default null comment '登录错误信息',
  store_id varchar(50) default null comment '门店表ID',
  store_located_city varchar(30) default null comment '门店所属城市',
  store_address varchar(500) default null comment '门店地址',
  store_name varchar(100) default null comment '门店名称',
  dinner_table_id varchar(50) default null comment '餐桌表ID',
  dinner_table_num varchar(50) default null comment '餐桌编号',
  description varchar(240) default null comment '描述',
  creation_date datetime,
  created_by varchar(50),
  last_update_date datetime,
  last_updated_by varchar(50),
  last_updated_login varchar(50),
  versions int(11),
  attribute_catrgory varchar(30),
  attribute1 varchar(240),
  attribute2 varchar(240),
  attribute3 varchar(240),
  attribute4 varchar(240),
  attribute5 varchar(240),
  attribute6 varchar(240),
  attribute7 varchar(240),
  attribute8 varchar(240),
  attribute9 varchar(240),
  attribute10 varchar(240),
  attribute11 varchar(240),
  attribute12 varchar(240),
  attribute13 varchar(240),
  attribute14 varchar(240),
  attribute15 varchar(240)
) comment '用户登录历史表';

alter table ciip_usr.ciip_usr_login_histories add primary key(id);

create index ciip_usr_login_histories_n1 on ciip_usr.ciip_usr_login_histories(entered_user_name, login_datetime);
