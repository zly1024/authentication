package com.zly.oauth.common;


/**
 * @author zly
 */

public enum RegisterTypeEnums {

    PHONE_TYPE("PHONE", "手机号"),
    EMAIL_TYPE("EMAIL", "邮箱"),
    CUSTOM_USER_NAME("USERNAME", "用户名");

    private String type;
    private String name;

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    RegisterTypeEnums(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String getTypeName(String type) {
        if (type.equals(PHONE_TYPE.type)) {
            return PHONE_TYPE.name;
        } else if (type.equals(EMAIL_TYPE.type)) {
            return EMAIL_TYPE.name;
        } else if (type.equals(CUSTOM_USER_NAME.type)) {
            return CUSTOM_USER_NAME.name;
        } else {
            return null;
        }
    }
}
