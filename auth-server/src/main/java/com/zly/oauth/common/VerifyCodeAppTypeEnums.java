package com.zly.oauth.common;


/**
 * @author zly
 */

public enum  VerifyCodeAppTypeEnums {

    REGISTER_TYPE("auth-server", "register"),  //注册
    FP_PASSWORD("auth-server", "fpassword");  //找回密码

    private String application;
    private String event;

    public String getApplication() {
        return application;
    }

    public String getEvent() {
        return event;
    }

    VerifyCodeAppTypeEnums(String application, String event) {
        this.application = application;
        this.event = event;
    }
}
