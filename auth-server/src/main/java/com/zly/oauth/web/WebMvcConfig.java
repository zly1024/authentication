/*
 * #{copyright}#
 */
package com.zly.oauth.web;

import com.zly.common.web.BaseWebMvcConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

/**
 *
 * @author zly
 * @date
 */
@Configuration
public class WebMvcConfig extends BaseWebMvcConfig {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/oauth/confirm_access").setViewName("authorize");
        registry.addViewController("/register").setViewName("register");
        registry.addViewController("/fp_phone").setViewName("fp_phone");
        registry.addViewController("/fp_reset").setViewName("fp_reset");
    }

}
