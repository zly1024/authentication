/*
 * #{copyright}#
 */
package com.zly.oauth.web;

import com.zly.common.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * OAuth2.0登出.
 * @author zly
 */
@FrameworkEndpoint
public class RevokeTokenEndpoint extends BaseController {

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @RequestMapping(value = "/oauth/token", method = RequestMethod.DELETE)
    @ResponseBody
    public void revokeToken(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        if (authorization == null) {
            authorization = request.getParameter("Authorization");
        }

        if (authorization != null && authorization.contains("Bearer")){
            String tokenValue = authorization.substring("Bearer".length()+1);
            OAuth2AccessToken token = jwtAccessTokenConverter.extractAccessToken(tokenValue, new HashMap<>());
            tokenStore.removeAccessToken(token);
        }
    }

}
