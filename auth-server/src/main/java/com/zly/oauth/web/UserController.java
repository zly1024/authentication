package com.zly.oauth.web;

import com.zly.common.constants.UserType;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.BooleanObject;
import com.zly.common.util.RandomUtil;
import com.zly.common.util.StringBuilderUtil;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.common.web.ResponseState;
import com.zly.oauth.common.ActionType;
import com.zly.oauth.common.RegisterTypeEnums;
import com.zly.oauth.common.VerifyCodeAppTypeEnums;
import com.zly.oauth.user.UserCustomResourceClient;
import com.zly.oauth.user.UserLoginHoldUtil;
import com.zly.oauth.user.ValidatePhoneEmail;
import com.zly.oauth.user.VerifyCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 *
 * @author zly
 * @date
 */
@RestController
@RequestMapping(value = "/usr")
public class UserController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserCustomResourceClient userCustomResourceClient;
    @Autowired
    private VerifyCodeUtil verifyCodeUtil;

    //查询用户
    private CiipUsrUsers getUserInfo(String userName) {
        CiipUsrUsers userQuery = new CiipUsrUsers();
        userQuery.setUserName(userName);  //根据用户名进行查询

        return userCustomResourceClient.selectOne(userQuery).getData();
    }

    /**
     * 校验用户是否存在（存在，返回true，不存在返回false）
     */
    private boolean isUserNameExists(String userName) {
        CiipUsrUsers ciipUsrUsers = getUserInfo(userName);
        return isUserNameExists(userName, ciipUsrUsers);
    }

    /**
     * 校验用户是否存在（存在，返回true，不存在返回false）
     * 同时返回user对象
     */
    private boolean isUserNameExists(String userName, CiipUsrUsers ciipUsrUsers) {
        if (ciipUsrUsers != null && !StringUtils.isEmpty(ciipUsrUsers.getId()) &&
                ciipUsrUsers.getUserName().equals(userName)) {
            return true;
        }
        return false;
    }

    /**
     * 验证用户名是否已存在
     *
     * @param userName
     * @param type
     * @param actionFlag 动作(用户存在性检查CHK_EXISTS | 注册REGISTER)
     * @return
     */
    @RequestMapping(value = "/verifyUserName", method = RequestMethod.POST)
    public ResponseEntity verifyUserName(@RequestParam(name = "userName", required = true) String userName,
                                         @RequestParam(name = "type", required = true) String type,
                                         @RequestParam(name = "actionFlag", required = false, defaultValue = ActionType.CHK_EXISTS) String actionFlag) {
        if (StringUtils.isEmpty(type)) {
            return fail("传入的参数[type]不能为空!");
        }
        if (!(type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.CUSTOM_USER_NAME.getType()))) {
            return fail("不支持的注册参数[type=" + type + "]类型!");
        }
        if (StringUtils.isEmpty(userName)) {
            return fail(RegisterTypeEnums.getTypeName(type) + "不能为空!");
        }
        CiipUsrUsers ciipUsrUsers = getUserInfo(userName);
        if (isUserNameExists(userName, ciipUsrUsers)) {
            if (ActionType.REGISTER.equalsIgnoreCase(actionFlag)) {
                return fail(RegisterTypeEnums.getTypeName(type) + "[" + userName + "]已存在或被他人注册!");
            } else {
                if (!UserLoginHoldUtil.canUserLogin(ciipUsrUsers)) {
                    return fail(RegisterTypeEnums.getTypeName(type) + "[" + userName + "]对应的用户状态异常，请联系客服!");
                }
            }
        } else {
            if (!ActionType.REGISTER.equalsIgnoreCase(actionFlag)) {
                return fail(RegisterTypeEnums.getTypeName(type) + "[" + userName + "]不存在!");
            }
        }

        //校验通过，返回true
        return success(new BooleanObject(true));
    }

    /**
     * 发送验证码
     *
     * @param receiver
     * @param type
     * @return
     */
    @RequestMapping(value = "/sendVerifyCode", method = RequestMethod.POST)
    public ResponseEntity sendVerifyCode(@RequestParam(name = "receiver", required = true) String receiver,
                                         @RequestParam(name = "type", required = true) String type,
                                         @RequestParam(name = "actionFlag", required = false, defaultValue = ActionType.CHK_EXISTS) String actionFlag) {
        if (StringUtils.isEmpty(type)) {
            return fail("传入的参数[type]不能为空!");
        }
        if (!(type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()))) {
            return fail("不支持的注册参数[type=" + type + "]类型!");
        }

        if (type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) && !ValidatePhoneEmail.isPhoneNumber(receiver)) {
            return fail("手机号码有误，请输入11位有效的手机号!");
        } else if (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()) && !ValidatePhoneEmail.isEmail(receiver)) {
            return fail("邮箱格式不正确!");
        }

        //校验用户名是否存在?
        if (isUserNameExists(receiver)) {
            if (ActionType.REGISTER.equalsIgnoreCase(actionFlag)) {
                return fail(RegisterTypeEnums.getTypeName(type) + "[" + receiver + "]已被注册，不能发送验证码!");
            }
        } else {
            if (!ActionType.REGISTER.equalsIgnoreCase(actionFlag)) {
                return fail(RegisterTypeEnums.getTypeName(type) + "[" + receiver + "]对应的用户不存在!");
            }
        }

        //发送验证码
        //目前仅有注册 + 修改密码
        String event = ActionType.REGISTER.equalsIgnoreCase(actionFlag) ?
                VerifyCodeAppTypeEnums.REGISTER_TYPE.getEvent() :
                VerifyCodeAppTypeEnums.FP_PASSWORD.getEvent();
        ResponseEntity result = userCustomResourceClient.sendVerifyCode(receiver, type, event);

        return result;
    }

    /**
     * 注册
     *
     * @param userName
     * @param password
     * @param confirmPassword
     * @param phone
     * @param email
     * @param verifyCode
     * @param type
     * @param sign
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestParam(name = "userName", required = false) String userName,
                                   @RequestParam(name = "password", required = true) String password,
                                   @RequestParam(name = "confirmPassword", required = true) String confirmPassword,
                                   @RequestParam(name = "phone", required = false) String phone,
                                   @RequestParam(name = "email", required = false) String email,
                                   @RequestParam(name = "verifyCode", required = true) String verifyCode,
                                   @RequestParam(name = "type", required = true) String type,
                                   @RequestParam(name = "sign", required = false) String sign) {
        logger.debug("register params: userName{}, password{}, confirmPassword{}, " +
                        "phone{}, email{}, verifyCode{}, type{}, sign{}",
                userName, password, confirmPassword, phone, email, verifyCode, type, sign);

        String receiver = null;
        //校验输入的参数
        if (StringUtils.isEmpty(type)) {
            return fail("传入的参数[type]不能为空!");
        }
        if (!(type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.CUSTOM_USER_NAME.getType()))) {
            return fail("不支持的注册参数[type=" + type + "]类型!");
        }
        if (type.equals(RegisterTypeEnums.PHONE_TYPE.getType())) {
            if (StringUtils.isEmpty(phone)) {
                return fail("手机号码不能为空!");
            } else if (!ValidatePhoneEmail.isPhoneNumber(phone)) {
                return fail("手机号码有误，请输入11位有效的手机号!");
            }
            receiver = phone;
        } else if (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType())) {
            if (StringUtils.isEmpty(email)) {
                return fail("邮箱不能为空!");
            } else if (!ValidatePhoneEmail.isEmail(email)) {
                return fail("邮箱格式不正确!");
            }
            receiver = email;
        } else if (type.equals(RegisterTypeEnums.CUSTOM_USER_NAME.getType()) && StringUtils.isEmpty(userName)) {
            return fail("用户名不能为空!");
        }

        //校验用户输入的密码
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(confirmPassword) ||
                !password.equals(confirmPassword)) {
            return fail("密码和确认密码不能为空，且必须保持一致!");
        }
        if (password.length() != 32 || confirmPassword.length() != 32) {
            //MD5加密后是32位
            return fail("传入的密码格式不正确，请刷新页面后重试!");
        }

        //校验验证码
        if (StringUtils.isEmpty(verifyCode)) {
            return fail("验证码不能为空!");
        }
        String redisKey = StringBuilderUtil.appendWithSplitor("_", VerifyCodeAppTypeEnums.REGISTER_TYPE.getApplication(),
                VerifyCodeAppTypeEnums.REGISTER_TYPE.getEvent(), receiver);
        if (receiver != null) {
            if (!verifyCodeUtil.validateVerifyCode(redisKey, receiver, verifyCode)) {
                return fail("验证码不正确，或验证码已过期!");
            }
        }

        //系统用户表用户名字段
        String sysUserName = type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ? phone :
                (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()) ? email : userName);
        //校验用户名是否存在?
        if (isUserNameExists(sysUserName)) {
            return fail(RegisterTypeEnums.getTypeName(type) + "[" + sysUserName + "]已存在或被他人注册!");
        }

        //校验通过，注册商户
        CiipUsrUsers ciipUsrUsers = new CiipUsrUsers();
        ciipUsrUsers.setUserName(sysUserName);
        ciipUsrUsers.setPassword(password);
        ciipUsrUsers.setNickName(sysUserName);
        if (type.equals(RegisterTypeEnums.PHONE_TYPE.getType())) {
            ciipUsrUsers.setMobilePhoneNum(phone);
        } else if (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType())) {
            ciipUsrUsers.setEmail(email);
        }
        ciipUsrUsers.setSourceSysRefCode(UserType.COMP_REGISTER_TEMP_USER);  //商家注册临时用户类型
        ciipUsrUsers.setGender("X");
        ciipUsrUsers.setRegistrationDate(new Date());
        ciipUsrUsers.setAccountStatusCode("A");
        ciipUsrUsers.setStartDate(new Date());
        ciipUsrUsers.setCreationDate(new Date());
        ciipUsrUsers.setLastUpdateDate(new Date());
        ciipUsrUsers.setVersions(1);

        ResponseEntity<CiipUsrUsers> result = userCustomResourceClient.doUserRegister(ciipUsrUsers);
        if(ResponseState.ERROR.getValue().equals(result.getCode())){
            return fail(result.getMsg());
        }

        //注册成功，删除验证码cache
        verifyCodeUtil.deleteVerifyCode(redisKey);

        logger.debug("register() ==> sysUserName{} type{} register successfully!", sysUserName, type);

        return success(new BooleanObject(true));
    }

    /**
     * 找回密码 ---- 校验手机号 + 验证码
     *
     * @param phone
     * @param email
     * @param verifyCode
     * @param type
     * @return
     */
    @RequestMapping(value = "/fp/verifyPhoneEffect", method = RequestMethod.POST)
    public ResponseEntity fpVerifyPhone(@RequestParam(name = "phone", required = false) String phone,
                                        @RequestParam(name = "email", required = false) String email,
                                        @RequestParam(name = "verifyCode", required = true) String verifyCode,
                                        @RequestParam(name = "type", required = true) String type) {
        String receiver = null;
        //校验输入的参数
        if (StringUtils.isEmpty(type)) {
            return fail("传入的参数[type]不能为空!");
        }
        if (!(type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ||
                type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()))) {
            return fail("不支持的注册参数[type=" + type + "]类型!");
        }
        if (type.equals(RegisterTypeEnums.PHONE_TYPE.getType())) {
            if (StringUtils.isEmpty(phone)) {
                return fail("手机号码不能为空!");
            } else if (!ValidatePhoneEmail.isPhoneNumber(phone)) {
                return fail("手机号码有误，请输入11位有效的手机号!");
            }
            receiver = phone;
        } else if (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType())) {
            if (StringUtils.isEmpty(email)) {
                return fail("邮箱不能为空!");
            } else if (!ValidatePhoneEmail.isEmail(email)) {
                return fail("邮箱格式不正确!");
            }
            receiver = email;
        }

        //校验验证码
        if (StringUtils.isEmpty(verifyCode)) {
            return fail("验证码不能为空!");
        }
        String redisKey = StringBuilderUtil.appendWithSplitor("_", VerifyCodeAppTypeEnums.FP_PASSWORD.getApplication(),
                VerifyCodeAppTypeEnums.FP_PASSWORD.getEvent(), receiver);
        if (receiver != null) {
            if (!verifyCodeUtil.validateVerifyCode(redisKey, receiver, verifyCode)) {
                return fail("验证码不正确，或验证码已过期!");
            }
        }

        //系统用户表用户名字段
        String sysUserName = type.equals(RegisterTypeEnums.PHONE_TYPE.getType()) ? phone :
                (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType()) ? email : "");
        //校验用户名是否存在?
        if (!StringUtils.isEmpty(sysUserName)) {
            CiipUsrUsers ciipUsrUsers = getUserInfo(sysUserName);
            if (!isUserNameExists(sysUserName, ciipUsrUsers)) {
                return fail(RegisterTypeEnums.getTypeName(type) + "[" + receiver + "]对应的用户不存在!");
            } else {
                if (!UserLoginHoldUtil.canUserLogin(ciipUsrUsers)) {
                    return fail(RegisterTypeEnums.getTypeName(type) + "[" + receiver + "]对应的用户状态异常，请联系客服!");
                }
            }
        }

        //校验通过，删除验证码，返回一个sign签名
        verifyCodeUtil.deleteVerifyCode(redisKey);
        String sign = RandomUtil.getRandomString(15);
        userCustomResourceClient.cacheSignInfo(sysUserName, type, sign);

        return success(sign);
    }


    /**
     * 密码重置
     *
     * @param sourceAccount
     * @param type
     * @param password
     * @param confirmPassword
     * @param sign
     * @return
     */
    @RequestMapping(value = "/fp/resetPwd", method = RequestMethod.POST)
    public ResponseEntity resetPassword(@RequestParam(name = "sourceAccount", required = true) String sourceAccount,
                                        @RequestParam(name = "type", required = true) String type,
                                        @RequestParam(name = "password", required = true) String password,
                                        @RequestParam(name = "confirmPassword", required = true) String confirmPassword,
                                        @RequestParam(name = "sign", required = true) String sign) {
        logger.debug("resetPassword params: sourceAccount{}, type{}, password{}, confirmPassword{}, sign{}",
                sourceAccount, type, password, confirmPassword, sign);

        //校验用户输入的密码
        if (StringUtils.isEmpty(password) || StringUtils.isEmpty(confirmPassword) ||
                !password.equals(confirmPassword)) {
            return fail("密码和确认密码不能为空，且必须保持一致!");
        }
        if (password.length() != 32 || confirmPassword.length() != 32) {
            //MD5加密后是32位
            return fail("传入的密码格式不正确，请刷新页面后重试!");
        }

        //校验签名值
        if (StringUtils.isEmpty(sign) || StringUtils.isEmpty(sourceAccount) || StringUtils.isEmpty(type)) {
            return fail("页面签名不能为空!");
        }
        String redisKey = StringBuilderUtil.appendWithSplitor("_", VerifyCodeAppTypeEnums.FP_PASSWORD.getApplication(),
                VerifyCodeAppTypeEnums.FP_PASSWORD.getEvent(), type, sourceAccount);
        if (!verifyCodeUtil.validateVerifyCode(redisKey, sourceAccount, sign)) {
            return fail("签名不正确，或签名已过期，请重新进行手机号验证!");
        }

        //校验用户名是否存在?
        CiipUsrUsers ciipUsrUsers = getUserInfo(sourceAccount);
        if (!isUserNameExists(sourceAccount, ciipUsrUsers)) {
            return fail(RegisterTypeEnums.getTypeName(type) + "[" + sourceAccount + "]对应的用户不存在!");
        }
        if (!UserLoginHoldUtil.canUserLogin(ciipUsrUsers)) {
            return fail(RegisterTypeEnums.getTypeName(type) + "[" + sourceAccount + "]对应的用户状态异常，请联系客服!");
        }
        //校验用户是否能够登录
        if (ciipUsrUsers != null && !UserLoginHoldUtil.canUserLogin(ciipUsrUsers)) {
            return fail(RegisterTypeEnums.getTypeName(type) + "[" + sourceAccount + "]对应的用户状态异常，请联系客服!");
        }

        //更新用户密码
        userCustomResourceClient.updateUserPassword(ciipUsrUsers.getId(), ciipUsrUsers.getUserName(), password);

        //修改成功，删除验证码cache
        verifyCodeUtil.deleteVerifyCode(redisKey);

        logger.debug("resetPassword() ===> params: sourceAccount{} type{} reset password successfully!", sourceAccount, type);

        return success(new BooleanObject(true));
    }

}
