package com.zly.oauth.user;

import com.alibaba.fastjson.JSONObject;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.constants.MessageType;
import com.zly.common.constants.UserRoleType;
import com.zly.common.domain.comm.CiipCommReceiver;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.BooleanObject;
import com.zly.common.dto.MessageDto;
import com.zly.common.dto.VerifyCodeDto;
import com.zly.common.dto.usr.UserRoleCode;
import com.zly.common.util.PasswordUtil;
import com.zly.common.util.RandomUtil;
import com.zly.common.web.ResponseEntity;
import com.zly.common.web.ResponseState;
import com.zly.oauth.common.RegisterTypeEnums;
import com.zly.oauth.common.VerifyCodeAppTypeEnums;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author zly
 */
@Component
public class UserCustomResourceClient extends AbstractResourceClient<ResponseEntity> {

    private static Logger logger = LoggerFactory.getLogger(UserCustomResourceClient.class);

    private static final String SERVICE_NAME = "http://localhost:10000/zly/user";

    @Autowired
    private VerifyCodeUtil verifyCodeUtil;

    @Override
    protected String getServiceName() {
        return SERVICE_NAME;
    }

    private ResponseEntity fail(String msg) {
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setCode(ResponseState.ERROR.getValue());
        responseEntity.setMsg(msg);
        return responseEntity;
    }

    /**
     * 查询一个用户
     *
     * @param usrUsers
     * @return
     */
    public ResponseEntity<CiipUsrUsers> selectOne(CiipUsrUsers usrUsers) {
        return successBean((Map<String, Object>) this.getForObject(processGetUrl("/user/selectOne",
                usrUsers), ResponseEntity.class).getData(), CiipUsrUsers.class);
    }


    /**
     * 发送验证码
     * @param receiver
     * @param type
     * @param event
     * @return
     */
    public ResponseEntity sendVerifyCode(String receiver, String type, String event) {

        VerifyCodeDto verifyCodeDto = new VerifyCodeDto();
        verifyCodeDto.setSender(receiver);
        verifyCodeDto.setApplication(VerifyCodeAppTypeEnums.REGISTER_TYPE.getApplication());  //auth-server
        verifyCodeDto.setEvent(event);

        /*//校验验证码是否已发送?
        if (verifyCodeUtil.isSend(verifyCodeDto.getKey())) {
            return fail("验证码已发送无需重复发送，若未收到，请稍后再试!");
        }*/

        String code = RandomUtil.generateRandom(4);  //产生4位随机验证码
        verifyCodeDto.setVerifyCode(code);
        verifyCodeDto.setSendTime(new Date());

        //发送验证码
        MessageDto messageDto = new MessageDto();
        if (type.equals(RegisterTypeEnums.PHONE_TYPE.getType())) {
            messageDto.setMessageType(MessageType.SMS);
            messageDto.setMessageTemplateCode("SMS_SEND_VERIFY_CODE_TEMP");
            messageDto.setSubject("新商户用户注册或改密-短信验证码发送");
            JSONObject json = new JSONObject();
            json.put("code", code);
            messageDto.setContent(json.toJSONString());
            messageDto.setSender("authServer." + event + ".sendVerifyCode");
            messageDto.setAttribute1(event);
        } else if (type.equals(RegisterTypeEnums.EMAIL_TYPE.getType())) {
            messageDto.setMessageType(MessageType.MAIL);
            messageDto.setMessageTemplateCode(null);
            messageDto.setSubject("易速点商户用户注册验证码");
            messageDto.setContent("您本次操作的验证码为: " + code + " ，5分钟内输入有效，请勿泄露于他人，感谢使用易速点云平台!");
            messageDto.setSender(null);  //采用默认的notice_service@esudian.com账户发送
            messageDto.setAttribute1(event);
        }
        List<CiipCommReceiver> receiverList = new ArrayList<>();
        CiipCommReceiver ciipCommReceiver = new CiipCommReceiver();
        ciipCommReceiver.setSendType("TO");
        ciipCommReceiver.setReceiverAddress(receiver);  //发送至
        receiverList.add(ciipCommReceiver);
        messageDto.setReceivers(receiverList);

        boolean result = verifyCodeUtil.sendVerifyCode(verifyCodeDto, messageDto);

        return success(new BooleanObject(result));
    }

    /**
     * 用户（商户）注册
     *
     * @param ciipUsrUsers
     * @return
     */
    public ResponseEntity<CiipUsrUsers> doUserRegister(CiipUsrUsers ciipUsrUsers) {
        //保存用户
        ResponseEntity<CiipUsrUsers> result = successBean((Map<String, Object>) this.postForObject("/user/insert", ciipUsrUsers, ResponseEntity.class).getData(), CiipUsrUsers.class);

        if(ResponseState.SUCCESS.getValue().equals(result.getCode())){
            //添加默认角色
            UserRoleCode urlParams = new UserRoleCode();
            urlParams.setUserId(result.getData().getId());  //用户ID
            urlParams.setRoleCodes(UserRoleType.REG_TEMP_ROLE);  //默认角色，多个角色，逗号分割

            this.postForObject("/user/addDefaultAuth", urlParams, ResponseEntity.class);
            return result;
        }else{
            return fail("注册失败");
        }
    }

    /**
     * 更新用户密码
     *
     * @param userId
     * @param userName
     * @param password
     * @return
     */
    public ResponseEntity<CiipUsrUsers> updateUserPassword(String userId, String userName, String password) {
        CiipUsrUsers ciipUsrUsers = new CiipUsrUsers();
        ciipUsrUsers.setId(userId);
        ciipUsrUsers.setUserName(userName);
        ciipUsrUsers.setPassword(PasswordUtil.encodePassword(password, userName));  //MD5(md5(password), salt) 因user-service更新时不会MD5，所以在此处处理
        ciipUsrUsers.setLastUpdateDate(new Date());

        return successBean((Map<String, Object>) this.postForObject("/user/save", ciipUsrUsers, ResponseEntity.class).getData(), CiipUsrUsers.class);
    }

    /**
     * 缓存签名信息
     *
     * @param receiver
     * @return
     */
    public boolean cacheSignInfo(String receiver, String type, String sign) {
        //缓存签名sign
        VerifyCodeDto verifyCodeDto = new VerifyCodeDto();
        verifyCodeDto.setSender(receiver);
        verifyCodeDto.setApplication(VerifyCodeAppTypeEnums.FP_PASSWORD.getApplication());  //auth-server
        verifyCodeDto.setEvent(VerifyCodeAppTypeEnums.FP_PASSWORD.getEvent() + "_" + type);
        verifyCodeDto.setSendTime(new Date());
        verifyCodeDto.setVerifyCode(sign);

        verifyCodeUtil.cacheVerifyCode(verifyCodeDto);

        return true;
    }


}
