package com.zly.oauth.user;

import com.alibaba.fastjson.JSONObject;
import com.zly.client.constants.CacheConstant;
import com.zly.client.constants.CommonErrorsEnum;
import com.zly.common.constants.MessageType;
import com.zly.common.dto.MessageDto;
import com.zly.common.dto.VerifyCodeDto;
import com.zly.common.web.ResponseEntity;
import com.zly.common.web.ResponseState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author zly
 */
@Component
public class VerifyCodeUtil {

    private Logger logger = LoggerFactory.getLogger(VerifyCodeUtil.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private CacheManager cacheManager;

    /**
     * 校验当前key和sender是否发送
     *
     * @param key key
     * @return
     */
    public boolean isSend(String key) {
        Cache v = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) v.get(key).get();
        if (verifyCodeDto != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验当前key和sender是否发送
     *
     * @param key    key
     * @param sender 发送对象
     * @return
     */
    public boolean isSend(String key, String sender) {
        Cache v = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) v.get(key).get();
        if (verifyCodeDto != null && sender.equals(verifyCodeDto.getSender())) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 校验当前key和sender是否发送
     *
     * @param key key
     * @return
     */
    public boolean isEmailSend(String key) {
        Cache v = cacheManager.getCache(CacheConstant.VERIFY_CODE_EMAIL_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) v.get(key).get();
        if (verifyCodeDto != null) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 校验当前key和sender是否发送
     *
     * @param key    key
     * @param sender 发送对象
     * @return
     */
    public boolean isEmailSend(String key, String sender) {
        Cache v = cacheManager.getCache(CacheConstant.VERIFY_CODE_EMAIL_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) v.get(key).get();
        if (verifyCodeDto != null && sender.equals(verifyCodeDto.getSender())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 校验验证码,发送对象是否正确
     *
     * @param key
     * @param sender
     * @param verifyCode
     * @return
     */
    public boolean validateVerifyCode(String key, String sender, String verifyCode) { // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) verifyCache.get(key).get();
        if (verifyCodeDto == null) {
            return false;
        }
        if (!sender.equals(verifyCodeDto.getSender())) {
            throw CommonErrorsEnum.ERROR_VERIFY_CODE_SENDER.exception();
        }
        if (!verifyCode.equals(verifyCodeDto.getVerifyCode())) {
            return false;
        }
        return true;

    }

    //缓存验证码
    public boolean cacheVerifyCode(VerifyCodeDto verifyCodeDto) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        verifyCache.put(verifyCodeDto.getKey(), verifyCodeDto);
        return true;
    }

    /**
     * 发送验证码---2
     *
     * @param verifyCodeDto 验证码dto
     * @param messageDto    对外发送消息主体
     * @return 是否发送成功
     */
    public boolean sendVerifyCode(VerifyCodeDto verifyCodeDto, MessageDto messageDto) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        messageDto.setMessageType(MessageType.SMS);
        ResponseEntity responseEntity = restTemplate.postForObject("http://localhost:7002/message/send", messageDto, ResponseEntity.class);
        logger.info(JSONObject.toJSONString(responseEntity));
        if(!ResponseState.SUCCESS.getValue().equals(responseEntity.getCode())){
            return false;
        }
        verifyCache.put(verifyCodeDto.getKey(), verifyCodeDto);
        return true;
    }

    /**
     * @param verifyCodeDto
     * @param messageDto
     * @return
     */
    public boolean sendEMailVerifyCode(VerifyCodeDto verifyCodeDto, MessageDto messageDto) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_EMAIL_CACHE);
        messageDto.setMessageType(MessageType.MAIL);
        restTemplate.postForObject("http://localhost:7002/message/send", messageDto, ResponseEntity.class);
        verifyCache.put(verifyCodeDto.getKey(), verifyCodeDto);
        return true;
    }


    /**
     * 验证是否能够发送验证码,验证 CacheConstant.VERIFY_CODE_SEND_BEFORE
     *
     * @param key
     * @return
     */
    public boolean canSend(String key) {
        Cache verifyCodeBefore = cacheManager.getCache(CacheConstant.VERIFY_CODE_SEND_BEFORE);
        if (verifyCodeBefore.get(key).get() == null) {
            return false;
        }
        return true;
    }

    /**
     * 验证是否能够发送验证码,验证 CacheConstant.VERIFY_CODE_SEND_BEFORE
     *
     * @param key
     * @param value
     * @return
     */
    public boolean canSend(String key, String value) {
        Cache verifyCodeBefore = cacheManager.getCache(CacheConstant.VERIFY_CODE_SEND_BEFORE);
        if (verifyCodeBefore.get(key).get() == null
                || !verifyCodeBefore.get(key).get().equals(value)) {
            return false;
        }
        return true;
    }

    /**
     * 发送之前获得发送权限
     *
     * @param key
     * @param value
     */
    public void processSendBefore(String key, String value) {
        Cache verifyCodeBefore = cacheManager.getCache(CacheConstant.VERIFY_CODE_SEND_BEFORE);
        verifyCodeBefore.put(key, value);
    }

    /**
     * 发送之前获得发送权限
     *
     * @param key
     */
    public void processSendBefore(String key) {
        Cache verifyCodeBefore = cacheManager.getCache(CacheConstant.VERIFY_CODE_SEND_BEFORE);
        verifyCodeBefore.put(key, 1);
    }


    /**
     * 验证验证码,提供是否删除选项
     *
     * @param key        key
     * @param verifyCode 待验证的验证码
     * @param isDelete   是否删除验证码(只有验证成功且传入true才会删除)
     * @return true 正确 false 错误
     */
    public boolean validateVerifyCodeDelete(String key, String sender, String verifyCode, boolean isDelete) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) verifyCache.get(key).get();
        if (verifyCodeDto == null) {
            return false;
        }
        if (!sender.equals(verifyCodeDto.getSender())) {
            throw CommonErrorsEnum.ERROR_VERIFY_CODE_SENDER.exception();
        }
        if (!verifyCode.equals(verifyCodeDto.getVerifyCode())) {
            return false;
        }
        if (isDelete) {
            verifyCache.evict(key);
        }
        return true;
    }


    /**
     * 验证验证码,提供是否删除选项
     *
     * @param key        key
     * @param verifyCode 待验证的验证码
     * @param isDelete   是否删除验证码(只有验证成功且传入true才会删除)
     * @return true 正确 false 错误
     */
    public boolean validateEmailVerifyCodeDelete(String key, String sender, String verifyCode, boolean isDelete) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_EMAIL_CACHE);
        VerifyCodeDto verifyCodeDto = (VerifyCodeDto) verifyCache.get(key).get();
        if (verifyCodeDto == null) {
            return false;
        }
        if (!sender.equals(verifyCodeDto.getSender())) {
            throw CommonErrorsEnum.ERROR_VERIFY_CODE_SENDER.exception();
        }
        if (!verifyCode.equals(verifyCodeDto.getVerifyCode())) {
            return false;
        }
        if (isDelete) {
            verifyCache.evict(key);
        }
        return true;
    }

    /**
     * 显式---删除验证码
     *
     * @param key
     * @return
     */
    public boolean deleteVerifyCode(String key) {
        // 用于储存验证码
        Cache verifyCache = cacheManager.getCache(CacheConstant.VERIFY_CODE_CACHE);
        verifyCache.evict(key);
        return true;
    }
}
