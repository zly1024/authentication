package com.zly.oauth.user;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author zly
 * @date
 */
public class ValidatePhoneEmail {
    /**
     * 校验是否为合法手机号
     *
     * @param phoneNumber 手机号
     * @return 手机号是否合法?true:false
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        Pattern p = Pattern.compile("^((13[0-9])|14[5|7]|(15[0-3|5-9])|(17[0|3|6-8])|(18[0-9])|(170[0|1]))\\d{8}$");
        Matcher m = p.matcher(phoneNumber);
        return m.matches();
    }


    public static boolean isEmail(String email) {
        Pattern p = Pattern.compile("^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$");
        Matcher m = p.matcher(email);
        return m.matches();
    }
}
