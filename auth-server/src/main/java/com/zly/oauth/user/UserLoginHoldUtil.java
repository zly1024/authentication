package com.zly.oauth.user;

import com.zly.common.domain.usr.CiipUsrUsers;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

/**
 * 用户登录Hold判断工具类
 * @author zly
 */
public class UserLoginHoldUtil {

    private static boolean isExpired(Date startDate, Date endDate) {
        Date now = new Date();
        if ((startDate != null && startDate.getTime() > now.getTime()) ||
                (endDate != null && endDate.getTime() < now.getTime())) {
            return true;
        }
        return false;
    }

    /**
     * 判断用户是否可以登录
     *   >> 返回true，可以登录;
     *   >> 返回false，不能登录，账号异常（用户过期、用户已锁住，黑名单等）
     *
     * @param ciipUsrUsers
     * @return
     */
    public static boolean canUserLogin(CiipUsrUsers ciipUsrUsers) {
        if (ciipUsrUsers == null) {
            return false;
        }
        if ("A".equals(ciipUsrUsers.getAccountStatusCode()) && !isExpired(ciipUsrUsers.getStartDate(), ciipUsrUsers.getEndDate())) {
            return true;
        }
        return false;
    }

    /**
     * 判断用户是否可以登录
     *
     * @param userDetails
     * @return
     */
    public static boolean canUserLoginByAuth(UserDetails userDetails) {
        if (userDetails.isAccountNonExpired() && userDetails.isAccountNonLocked() &&
                userDetails.isEnabled() && userDetails.isCredentialsNonExpired()) {
            return true;
        }
        return false;
    }
}
