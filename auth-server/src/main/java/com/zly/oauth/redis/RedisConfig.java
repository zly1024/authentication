/*
 * #{copyright}#
 */
package com.zly.oauth.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zly.common.cache.HashRedisCacheManager;
import com.zly.common.redis.CompressJackson2JsonRedisSerializer;
import com.zly.common.redis.RedisCacheUtil;
import com.zly.common.redis.RedisTemplateHolder;
import com.zly.common.redis.lock.RedissonLockHelper;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 全局缓存配置.
 * @author zly
 */
@Configuration
@EnableCaching
@ConditionalOnProperty(name = "global.cache.host")
@EnableConfigurationProperties(RedisCacheProperties.class)
public class RedisConfig extends CachingConfigurerSupport {

    @Autowired
    private RedisCacheProperties redisCacheProperties;

    @Bean
    public RedisCacheUtil redisCacheUtil(RedisTemplate redisTemplate) {
        return new RedisCacheUtil(redisTemplate);
    }

    @Bean
    public CacheManager cacheManager(RedisTemplate redisTemplate) {
        if (redisTemplate == null) {
            return null;
        }
        HashRedisCacheManager redisCacheManager = new HashRedisCacheManager(redisTemplate);
        redisCacheManager.setDefaultExpiration(redisCacheProperties.getExpire());
        redisCacheManager.setExpires(redisCacheProperties.getExpires());
        return redisCacheManager;
    }

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson() {
        Config config = new Config();
        config.useSingleServer().
                setAddress(redisCacheProperties.getHost() + ":" + redisCacheProperties.getPort()).
                setDatabase(redisCacheProperties.getDatabase()).setPassword(redisCacheProperties.getPassword());
        return Redisson.create(config);
    }

    @Bean
    public RedissonLockHelper redissonLockHelper() {
        RedissonLockHelper helper = new RedissonLockHelper(redisson());
        return helper;
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(){
        // global缓存采用另外的配置,不与session公用redis db
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxWaitMillis(redisCacheProperties.getMaxWaitMillis());
        jedisPoolConfig.setMaxIdle(redisCacheProperties.getMaxIdle());
        jedisPoolConfig.setMaxTotal(redisCacheProperties.getMaxTotal());

        JedisConnectionFactory factory = new JedisConnectionFactory(jedisPoolConfig);
        factory.setHostName(redisCacheProperties.getHost());
        factory.setPort(redisCacheProperties.getPort());
        factory.setDatabase(redisCacheProperties.getDatabase());
        factory.setPassword(redisCacheProperties.getPassword());
        factory.afterPropertiesSet();


        //redisTemplate默认使用了JDK的序列化机制，json乱码，修改默认序列化器
        //stringRedisTemplate默认修改了序列化器
        RedisTemplate template = new RedisTemplate();
        CompressJackson2JsonRedisSerializer jackson2JsonRedisSerializer = new CompressJackson2JsonRedisSerializer(Object.class);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        template.setConnectionFactory(factory);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
        template.setKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();

        RedisTemplateHolder.holdRedisTemplate(template);

        return template;
    }
}
