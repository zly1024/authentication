/*
 * #{copyright}#
 */
package com.zly.oauth.config;

import com.zly.common.security.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zly 基于数据库验证账户密码
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final  String SERVICE_NAME = "http://localhost:10000/zly/user";

    //构造方法引入restTemplate
    private RestTemplate restTemplate;

    public UserDetailsServiceImpl(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) {
        // TODO: 根据不同平台传递不同参数
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("userName", userName);
        return restTemplate.getForObject(SERVICE_NAME + "/user/oAuth/get", User.class, urlVariables);
    }
}
