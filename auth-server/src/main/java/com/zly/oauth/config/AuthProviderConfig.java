/*
 * #{copyright}#
 */
package com.zly.oauth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.client.RestTemplate;

/**
 * 将userDetailsService放入验证逻辑类中
 * @author zly
 */
@Configuration
public class AuthProviderConfig {


    @Bean(name = "userDetailsService")
    public UserDetailsService userDetailsService(RestTemplate restTemplate) {
        return new UserDetailsServiceImpl(restTemplate);
    }

    @Bean
    public AuthenticationProvider userAuthProvider(UserDetailsService userDetailsService) {
        return new UserAuthProvider(userDetailsService);
    }

}
