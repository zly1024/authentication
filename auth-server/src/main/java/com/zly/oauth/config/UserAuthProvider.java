/*
 * #{copyright}#
 */
package com.zly.oauth.config;

import com.zly.common.util.HashUtils;
import com.zly.common.util.PasswordUtil;
import com.zly.oauth.user.UserLoginHoldUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 验证逻辑
 * @author zly
 */
public class UserAuthProvider implements AuthenticationProvider {

    private static Logger logger = LoggerFactory.getLogger(UserAuthProvider.class);

    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    private UserDetailsService userDetailsService;

    /**
     * 某些异常情况，界面传入的是原始密码，需要经过处理
     * @param password
     * @return
     */
    private String passwordMd5Encode(String password) {
        try {
            return HashUtils.getMD5(password);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return password;
        }
    }

    public UserAuthProvider(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName();
        String password = (String) authentication.getCredentials();
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
        if (userDetails == null) {
            logger.debug("Authentication failed: user doesn't exists!");
            throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        } else {
            String userPassWord = userDetails.getPassword();
            if (userPassWord.equals(password) || userPassWord.equals(PasswordUtil.encodePassword(password, userName)) ||
                    userPassWord.equals(PasswordUtil.encodePassword(passwordMd5Encode(password), userName))) {
                //校验用户是否已过期或被锁住等
                if (UserLoginHoldUtil.canUserLoginByAuth(userDetails)) {
                    return new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
                } else {
                    logger.debug("Authentication failed: user cannot login!");
                    throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
                }
            } else {
                logger.debug("Authentication failed: password does not match stored value!");
                throw new BadCredentialsException(this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
            }
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
