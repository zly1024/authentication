/*
 * #{copyright}#
 */
package com.zly.oauth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.transaction.annotation.Transactional;

/**
 * 解决高并发下，申请token时出现的
 * DuplicateKeyException 问题.
 * @author zly
 */
public class TokenServices extends DefaultTokenServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenServices.class);

    @Override
    @Transactional
    public OAuth2AccessToken createAccessToken(OAuth2Authentication authentication) throws AuthenticationException {
        try {
            return super.createAccessToken(authentication);
        }catch (DuplicateKeyException dke) {
            LOGGER.debug(String.format("Duplicate user found for %s",authentication.getUserAuthentication().getPrincipal()));
            return super.getAccessToken(authentication);
        }catch (Exception ex) {
            LOGGER.debug(String.format("Exception while creating access token %s",ex));
        }
        return null;
    }
}
