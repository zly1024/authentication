/*
 * #{copyright}#
 */
package com.zly.oauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

/**
 * @author zly
 */
@Configuration
@EnableWebSecurity
//指定的类加载完了再加载本类
@AutoConfigureAfter(AuthProviderConfig.class)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${security.success.defaultTargetUrl:}")
    private String defaultTargetUrl;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SimpleUrlLogoutSuccessHandler logoutSuccessHandler = new SimpleUrlLogoutSuccessHandler();
        logoutSuccessHandler.setTargetUrlParameter("redirect_uri");

        SavedRequestAwareAuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
        successHandler.setTargetUrlParameter("redirect_uri");
        successHandler.setDefaultTargetUrl(defaultTargetUrl);

        http.httpBasic().disable().csrf().disable().logout().logoutSuccessHandler(logoutSuccessHandler).and().formLogin().loginPage("/login").successHandler(successHandler).defaultSuccessUrl(defaultTargetUrl).permitAll().and().authorizeRequests()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/register", "/usr/verifyUserName", "/usr/sendVerifyCode", "/usr/register").permitAll()
                .antMatchers("/fp_phone", "/fp_reset", "/usr/fp/verifyPhoneEffect", "/usr/fp/resetPwd").permitAll()
                .antMatchers("/MP_verify_RIPzMyhi7TAhrr1q.txt").permitAll()   //保证微信内不提示安全验证信息
                .anyRequest().authenticated();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

}
