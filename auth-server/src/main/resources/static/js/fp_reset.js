/**
 * 重制新密码 fp_reset.html 引用的js文件
 *
 * Created by zly on 2017/12/16.
 */

//弹出错误提示
function showErrorOrOk(msg) {
    if (msg.length > 0) {
        $("#showErrorMsg").text("× " + msg);
        $("#showError").show();
    } else {
        $("#showErrorMsg").text("");
        $("#showError").hide();
    }
}

//校验密码（密码复杂度: 6-20位，字母大小写、数字、特殊字符中任意两个组合）
function checkPasswordComplexity(password) {
    if (password.length < 6 || password.length > 20) {
        return 0;
    }
    var count = 0;
    if (password.match(/([a-z])+/)) {
        count++;
    }
    if (password.match(/([0-9])+/)) {
        count++;
    }
    if (password.match(/([A-Z])+/)) {
        count++;
    }
    if (password.match(/[^a-zA-Z0-9]+/)) {
        count++;
    }
    if (count >= 2) {
        //校验通过
        return true;
    } else {
        return false;
    }
}

/**
 * 设置3s后自动跳转到登录页面
 */
var goToWaitSeconds = 3;  //设置3秒后自动消失
function goToLoginUrl(account) {
    if (goToWaitSeconds == 0) {
        $("body").mLoading("hide");//隐藏loading组件
        goToWaitSeconds = 3;

        //跳转到登录页面
        $(location).attr('href', '/login');
        return;
    } else {
        var showText = "密码修改成功，" + goToWaitSeconds + "s后将自动跳转到登录页面...";
        //显示modal的提示
        $("body").mLoading({
            text: showText,//加载文字，默认值：加载中...
            icon: "",//加载图标，默认值：一个小型的base64的gif图片
            html: false,//设置加载内容是否是html格式，默认值是false
            content: "",//忽略icon和text的值，直接在加载框中显示此值
            mask: true//是否显示遮罩效果，默认显示
        });

        goToWaitSeconds--;
    }
    setTimeout(function () {
        goToLoginUrl(account)
    }, 1000); //每过一秒调用一次autoHiddenTips方法
}


$(function () {

    $.ajaxSetup({
        cache: true
    });
    $.getScript('js/jquery.md5.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });
    $.getScript('js/common.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });
    $.getScript('js/jquery.mloading.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });

    //======================
    // 注册: 表单提交验证
    //======================
    $("form").submit(function (event) {
        event.preventDefault();

        var password = $.trim($('input[name=password]').val());
        var confirmPassword = $.trim($('input[name=confirmPassword]').val());

        //密码校验
        if (password.length == 0) {
            showErrorOrOk("登录密码不能为空!");
            $('input[name=password]').focus();
            return false;
        } else if (!checkPasswordComplexity(password)) {
            showErrorOrOk("登录密码长度为6-20位，且包括大小写字母、数字、特殊字符中任意两种!");
            $('input[name=password]').focus();
            return false;
        }

        if (confirmPassword.length == 0) {
            showErrorOrOk("确认登录密码不能为空!");
            $('input[name=confirmPassword]').focus();
            return false;
        }
        if (password.length != confirmPassword.length || password != confirmPassword) {
            showErrorOrOk("登录密码和确认登录密码不一致!");
            $('input[name=confirmPassword]').focus();
            return false;
        }

        //验证sign签名值
        var sourceAccount = getUrlParam("sourceAccount");
        var type = getUrlParam("type");
        var signVal = getUrlParam("sign");
        if (sourceAccount.length == 0 || type.length == 0 || signVal.length != 15) {
            showErrorOrOk("系统检测到该页面签名值错误，为了账户安全，请重新进行手机号验证!");
            return false;
        }

        //密码加密后传输
        var passwordMd5 = $.md5(password);
        var confirmPasswordMd5 = $.md5(confirmPassword);

        //修改密码
        var data = doPasswordResetAction(sourceAccount, type, passwordMd5, confirmPasswordMd5, signVal);

        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            return false;
        } else {
            //设置为隐藏，且提示信息为空
            showErrorOrOk("");

            goToLoginUrl();  //3s后跳转到登录页面
        }

        return true;
    });


//======================
// Ajax 请求修改密码
//======================
    function doPasswordResetAction(sourceAccount, type, password, confirmPassword, sign) {
        var errorFlag = false;
        var errorMsg = "";

        $.ajax({
            url: "/usr/fp/resetPwd",
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                "sourceAccount": sourceAccount,
                "type": type,
                "password": password,
                "confirmPassword": confirmPassword,
                "sign": sign
            },
            success: function (rst) {
                if (rst.code != "success") {
                    errorFlag = true;
                    errorMsg = "重置密码失败: " + rst.msg;
                }
            },
            error: function (err) {
                errorFlag = true;
                errorMsg = '网络出现点小问题: ' + err;
            }
        });

        return {errorFlag: errorFlag, errorMsg: errorMsg};
    }

})