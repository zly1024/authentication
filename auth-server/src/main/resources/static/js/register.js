/**
 * 注册页面 register.html 引用的js文件
 *
 * Created by zly on 2017/12/16.
 */

//弹出错误提示
function showErrorOrOk(msg) {
    if (msg.length > 0) {
        $("#showErrorMsg").text("× " + msg);
        $("#showError").show();
    } else {
        $("#showErrorMsg").text("");
        $("#showError").hide();
    }
}

//校验普通的用户名
function checkNormalUserName(userName) {
    var regName = /^[a-zA-Z0-9_]\w{3,17}$/;  //用户名
    if (regName.test(userName)) {
        return true;
    }
    return false;
}

//校验密码（密码复杂度: 6-20位，字母大小写、数字、特殊字符中任意两个组合）
function checkPasswordComplexity(password) {
    if (password.length < 6 || password.length > 20) {
        return 0;
    }
    var count = 0;
    if (password.match(/([a-z])+/)) {
        count++;
    }
    if (password.match(/([0-9])+/)) {
        count++;
    }
    if (password.match(/([A-Z])+/)) {
        count++;
    }
    if (password.match(/[^a-zA-Z0-9]+/)) {
        count++;
    }
    if (count >= 2) {
        //校验通过
        return true;
    } else {
        return false;
    }
}


/**
 * 短信验证码
 */
var COUNT_DOWN = 60;  //短信后倒计时
function setSendTime(obj) {
    if (COUNT_DOWN == 0) {
        $('input[name=phone]').attr("readonly", false);
        $(obj).attr("disabled", false);
        $(obj).attr("mark", "1");
        $(obj).html("获取验证码");
        COUNT_DOWN = 60;
        return;
    } else {
        $('input[name=phone]').attr("readonly", true);
        $(obj).attr("disabled", true);
        $(obj).attr("mark", "0");
        $(obj).html("重新发送(" + COUNT_DOWN + ")");
        COUNT_DOWN--;
    }
    setTimeout(function () {
        setSendTime(obj)
    }, 1000);
}

/**
 * 设置3s后自动消失短信验证码发送成功的tips
 */
var waitSeconds = 5;  //设置5秒后自动消失
function setCloseTips(msg) {
    if (waitSeconds == 0) {
        $("#showSendTipsMsg").text("");
        $("#showSendTips").hide();
        waitSeconds = 5;
        return;
    } else {
        $("#showSendTipsMsg").text(msg);
        $("#showSendTips").show();
        waitSeconds--;
    }
    setTimeout(function () {
        setCloseTips(msg)
    }, 1000); //每过一秒调用一次autoHiddenTips方法
}

/**
 * 设置3s后自动跳转到登录页面
 */
var goToWaitSeconds = 3;  //设置3秒后自动消失
function goToLoginUrl(account) {
    if (goToWaitSeconds == 0) {
        $("body").mLoading("hide");//隐藏loading组件
        goToWaitSeconds = 3;

        //跳转到登录页面
        $(location).attr('href', '/login');
        return;
    } else {
        var showText = "恭喜您，用户注册成功，" + goToWaitSeconds + "s后将自动跳转到登录页面...";
        //显示modal的提示
        $("body").mLoading({
            text: showText,//加载文字，默认值：加载中...
            icon: "",//加载图标，默认值：一个小型的base64的gif图片
            html: false,//设置加载内容是否是html格式，默认值是false
            content: "",//忽略icon和text的值，直接在加载框中显示此值
            mask: true//是否显示遮罩效果，默认显示
        });

        goToWaitSeconds--;
    }
    setTimeout(function () {
        goToLoginUrl(account)
    }, 1000); //每过一秒调用一次autoHiddenTips方法
}


$(function () {

    $.ajaxSetup({
        cache: true
    });
    $.getScript('js/jquery.md5.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });
    $.getScript('js/common.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });

    //======================
    // 同意协议
    //======================
    $('#agreeSvcAgreement').change(function () {
        if ($("input[type='checkbox']").is(':checked')) {
            $('#registerSubmitBtn').removeAttr('disabled');
        }
        else {
            $('#registerSubmitBtn').attr('disabled', true);
        }
    });

    //======================
    // 验证用户名（光标离开）
    //======================
    $('input[name=username]').blur(function () {
        var userName = $.trim($('input[name=username]').val());

        if (userName.length > 0 && validateUserNameField(userName)) {
            //校验通过，设置为隐藏，且提示信息为空
            showErrorOrOk("");
        }
    });

    //======================
    // 验证手机号（光标离开）
    //======================
    $('input[name=phone]').blur(function () {
        var phone = $.trim($('input[name=phone]').val());

        if (phone.length > 0 && validatePhoneField(phone)) {
            //校验通过，设置为隐藏，且提示信息为空
            showErrorOrOk("");

            //设置发送验证码可用
            if (COUNT_DOWN == 60) {
                $('#sendVerifyCodeBtn').attr("disabled", false);
            }
        } else {
            $('#sendVerifyCodeBtn').attr("disabled", true);
        }
    });

    //手机号变更校验
    $('input[name=phone]').bind('input propertychange', function () {
        var phone = $.trim($('input[name=phone]').val());

        if (phone.length == 11 && validatePhoneField(phone)) {
            //校验通过，设置为隐藏，且提示信息为空
            showErrorOrOk("");

            //设置发送验证码可用
            if (COUNT_DOWN == 60) {
                $('#sendVerifyCodeBtn').attr("disabled", false);
            }
        } else {
            $('#sendVerifyCodeBtn').attr("disabled", true);
        }
    });

    //======================
    // 注册: 表单提交验证
    //======================
    $("form").submit(function (event) {
        event.preventDefault();

        //var userName = $.trim($('input[name=username]').val());  //用户名暂不提供，使用手机号注册，注释之
        var phone = $.trim($('input[name=phone]').val());
        var password = $.trim($('input[name=password]').val());
        var confirmPassword = $.trim($('input[name=confirmPassword]').val());
        var verifyCode = $.trim($('input[name=verifyCode]').val());

        //用户名校验
        //if (!validateUserNameField(userName)) {
        //    return false;
        //}

        //手机号校验
        if (!validatePhoneField(phone)) {
            return false;
        }

        //密码校验
        if (password.length == 0) {
            showErrorOrOk("登录密码不能为空!");
            $('input[name=password]').focus();
            return false;
        } else if (!checkPasswordComplexity(password)) {
            showErrorOrOk("登录密码长度为6-20位，且包括大小写字母、数字、特殊字符中任意两种!");
            $('input[name=password]').focus();
            return false;
        }

        if (confirmPassword.length == 0) {
            showErrorOrOk("确认登录密码不能为空!");
            $('input[name=confirmPassword]').focus();
            return false;
        }
        if (password.length != confirmPassword.length || password != confirmPassword) {
            showErrorOrOk("登录密码和确认登录密码不一致!");
            $('input[name=confirmPassword]').focus();
            return false;
        }

        //验证码校验
        if (verifyCode.length != 4) {
            showErrorOrOk("请输入4位短信验证码!");
            $('input[name=verifyCode]').focus();
            return false;
        }

        //显示modal的提示
        $("body").mLoading({
            text: "正在注册用户，请稍后...",//加载文字，默认值：加载中...
            icon: "",//加载图标，默认值：一个小型的base64的gif图片
            html: false,//设置加载内容是否是html格式，默认值是false
            content: "",//忽略icon和text的值，直接在加载框中显示此值
            mask: true//是否显示遮罩效果，默认显示
        });

        //密码加密后传输
        try {
            var passwordMd5 = $.md5(password);
            var confirmPasswordMd5 = $.md5(confirmPassword);

            //注册
            var userName = null;
            var email = null;
            var sign = null;
            var type = PHONE_TYPE;
            var data = doRegisterAction(userName, passwordMd5, confirmPasswordMd5, phone, email, verifyCode, type, sign);

            $("body").mLoading("hide");//隐藏loading组件

            if (data.errorFlag) {
                showErrorOrOk(data.errorMsg);
                return false;
            } else {
                //设置为隐藏，且提示信息为空
                showErrorOrOk("");

                goToLoginUrl(phone);  //3s后跳转到登录页面
            }
        } catch (e) {
            showErrorOrOk("JS加载异常，密码控件可能损坏，请刷新页面后重试! (错误代码:" + e + ")");
            return false;
        }

        return true;
    });

//======================
// 发送短信验证码
//======================
    $("#sendVerifyCodeBtn").click(function () {
        var phone = $.trim($('input[name=phone]').val());

        if (!validatePhoneField(phone)) {
            return false;
        }

        var errorFlag = false;
        var mark = $("#sendVerifyCodeBtn").attr("mark");  //控制标识
        if (mark == "1") {
            var _self = this;
            //调用API发送验证码
            $.ajax({
                url: "/usr/sendVerifyCode",
                type: "POST",
                dataType: "json",
                async: false,
                data: {
                    "receiver": phone,
                    "type": PHONE_TYPE,
                    "actionFlag": REGISTER_ACTION
                },
                success: function (rst) {
                    if (rst.code == "success") {
                        showErrorOrOk("");

                        setSendTime(_self);  //设置发送按钮倒计时60s

                        var tips = "验证码已发送至尾号" + phone.substr(phone.length - 4, 4) + "的手机号，5分钟内输入有效。";
                        setCloseTips(tips);

                        errorFlag = false;
                    } else {
                        showErrorOrOk(rst.msg);
                        errorFlag = true;
                    }
                },
                error: function (err) {
                    showErrorOrOk("网络出现点小问题: " + err);
                    errorFlag = true;
                }
            });
        }
        if (errorFlag)
            return false;

        return true;
    });

    function validateUserNameField(userName) {
        //用户名校验
        if (userName.length == 0) {
            showErrorOrOk("用户名不能为空!");
            $('input[name=username]').focus();
            return false;
        } else if (userName.indexOf("@") > 0) {
            //可能是邮箱注册，验证邮箱
            if (!checkEmail(userName)) {
                showErrorOrOk("邮箱格式不正确!");
                $('input[name=username]').focus();
                return false;
            } else if (userName.length < 5 || userName.length > 32) {
                showErrorOrOk("邮箱不合法，长度仅支持5-32位!");
                $('input[name=username]').focus();
                return false;
            }
        } else if (userName.length < 4 || userName.length > 18 ||
            (!checkPhone(userName) && !checkNormalUserName(userName))) {
            showErrorOrOk("用户名长度4-18位，且必须为字母、数字、下划线字符!");
            $('input[name=username]').focus();
            return false;
        }

        //校验用户名是否已存在?
        var data = doValidateUserNameByApi(userName, USERNAME_TYPE, REGISTER_ACTION);
        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            $('input[name=username]').focus();
            return false;
        }
        return true;
    }

    function validatePhoneField(phone) {
        if (phone.length != 11) {
            showErrorOrOk("请输入11位合法的手机号!");
            $('input[name=phone]').focus();
            return false;

        } else if (!checkPhone(phone)) {
            showErrorOrOk("手机号码有误，请重新输入!");
            $('input[name=phone]').focus();
            return false;
        }

        //校验用户名是否已存在（手机号作为用户名) ?
        var data = doValidateUserNameByApi(phone, PHONE_TYPE, REGISTER_ACTION);
        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            $('input[name=phone]').focus();
            return false;
        }

        return true;
    }

//======================
// Ajax 请求注册(提交注册信息)
//======================
    function doRegisterAction(userName, password, confirmPassword, phone, email, verifyCode, type, sign) {
        var errorFlag = false;
        var errorMsg = "";

        $.ajax({
            url: "/usr/register",
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                "userName": userName,
                "password": password,
                "confirmPassword": confirmPassword,
                "phone": phone,
                "email": email,
                "verifyCode": verifyCode,
                "type": type,
                "sign": sign
            },
            success: function (rst) {
                if (rst.code != "success") {
                    errorFlag = true;
                    errorMsg = "注册用户失败: " + rst.msg;
                }
            },
            error: function (err) {
                errorFlag = true;
                errorMsg = '网络出现点小问题: ' + err;
            }
        });

        return {errorFlag: errorFlag, errorMsg: errorMsg};
    }

})