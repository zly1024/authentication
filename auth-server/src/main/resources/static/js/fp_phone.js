/**
 * 找回密码(验证手机号) fp_phone.html 引用的js文件
 *
 * Created by zly on 2017/12/16.
 */

//弹出错误提示
function showErrorOrOk(msg) {
    if (msg.length > 0) {
        $("#showErrorMsg").text("× " + msg);
        $("#showError").show();
    } else {
        $("#showErrorMsg").text("");
        $("#showError").hide();
    }
}

/**
 * 短信验证码
 */
var COUNT_DOWN = 60;  //短信后倒计时
function setSendTime(obj) {
    if (COUNT_DOWN == 0) {
        $('input[name=phone]').attr("readonly", false);
        $(obj).attr("disabled", false);
        $(obj).attr("mark", "1");
        $(obj).html("获取验证码");
        COUNT_DOWN = 60;
        return;
    } else {
        $('input[name=phone]').attr("readonly", true);
        $(obj).attr("disabled", true);
        $(obj).attr("mark", "0");
        $(obj).html("重新发送(" + COUNT_DOWN + ")");
        COUNT_DOWN--;
    }
    setTimeout(function () {
        setSendTime(obj)
    }, 1000);
}

/**
 * 设置3s后自动消失短信验证码发送成功的tips
 */
var waitSeconds = 5;  //设置5秒后自动消失
function setCloseTips(msg) {
    if (waitSeconds == 0) {
        $("#showSendTipsMsg").text("");
        $("#showSendTips").hide();
        waitSeconds = 5;
        return;
    } else {
        $("#showSendTipsMsg").text(msg);
        $("#showSendTips").show();
        waitSeconds--;
    }
    setTimeout(function () {
        setCloseTips(msg)
    }, 1000); //每过一秒调用一次autoHiddenTips方法
}


$(function () {

    $.ajaxSetup({
        cache: true
    });
    $.getScript('js/jquery.md5.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });
    $.getScript('js/common.js').done(function () {
    }).fail(function () {
        showErrorOrOk("加载js文件失败，为了不影响功能的使用，请尝试刷新页面!");
    });

    //======================
    // 验证手机号（光标离开）
    //======================
    $('input[name=phone]').blur(function () {
        var phone = $.trim($('input[name=phone]').val());

        if (phone.length > 0 && validatePhoneField(phone)) {
            //校验通过，设置为隐藏，且提示信息为空
            showErrorOrOk("");

            //设置发送验证码可用
            if (COUNT_DOWN == 60) {
                $('#sendVerifyCodeBtn').attr("disabled", false);
            }
        } else {
            $('#sendVerifyCodeBtn').attr("disabled", true);
        }
    });

    //手机号变更校验
    $('input[name=phone]').bind('input propertychange', function () {
        var phone = $.trim($('input[name=phone]').val());

        if (phone.length == 11 && validatePhoneField(phone)) {
            //校验通过，设置为隐藏，且提示信息为空
            showErrorOrOk("");

            //设置发送验证码可用
            if (COUNT_DOWN == 60) {
                $('#sendVerifyCodeBtn').attr("disabled", false);
            }
        } else {
            $('#sendVerifyCodeBtn').attr("disabled", true);
        }
    });

    //======================
    // 下一步 [button]
    //======================
    $("form").submit(function (event) {
        event.preventDefault();

        var phone = $.trim($('input[name=phone]').val());
        var verifyCode = $.trim($('input[name=verifyCode]').val());

        //手机号校验
        if (!validatePhoneField(phone)) {
            return false;
        }

        //验证码校验
        if (verifyCode.length != 4) {
            showErrorOrOk("请输入4位短信验证码!");
            $('input[name=verifyCode]').focus();
            return false;
        }

        var email = null;
        var type = PHONE_TYPE;
        var data = doFpValidatePhoneAndNext(phone, email, verifyCode, type);
        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            return false;
        } else {
            //设置为隐藏，且提示信息为空
            showErrorOrOk("");

            //跳转到设置密码界面
            var params = "sourceAccount=" + phone + "&type=" + PHONE_TYPE + "&sign=" + data.sign;
            $(location).attr('href', '/fp_reset?' + params);
        }

        return true;
    });

//======================
// 发送短信验证码
//======================
    $("#sendVerifyCodeBtn").click(function () {
        var phone = $.trim($('input[name=phone]').val());

        if (!validatePhoneField(phone)) {
            return false;
        }

        var errorFlag = false;
        var mark = $("#sendVerifyCodeBtn").attr("mark");  //控制标识
        if (mark == "1") {
            var _self = this;
            //调用API发送验证码
            $.ajax({
                url: "/usr/sendVerifyCode",
                type: "POST",
                dataType: "json",
                async: false,
                data: {
                    "receiver": phone,
                    "type": PHONE_TYPE,
                    "actionFlag": CHK_EXISTS_ACTION
                },
                success: function (rst) {
                    if (rst.code == "success") {
                        showErrorOrOk("");

                        setSendTime(_self);  //设置发送按钮倒计时60s

                        var tips = "验证码已发送至尾号" + phone.substr(phone.length - 4, 4) + "的手机号，5分钟内输入有效。";
                        setCloseTips(tips);

                        errorFlag = false;
                    } else {
                        showErrorOrOk(rst.msg);
                        errorFlag = true;
                    }
                },
                error: function (err) {
                    showErrorOrOk("网络出现点小问题: " + err);
                    errorFlag = true;
                }
            });
        }
        if (errorFlag)
            return false;

        return true;
    });

    function validatePhoneField(phone) {
        if (phone.length != 11) {
            showErrorOrOk("请输入11位合法的手机号!");
            $('input[name=phone]').focus();
            return false;

        } else if (!checkPhone(phone)) {
            showErrorOrOk("手机号码有误，请重新输入!");
            $('input[name=phone]').focus();
            return false;
        }

        //校验用户名是否已存在（手机号作为用户名) ?
        var data = doValidateUserNameByApi(phone, PHONE_TYPE, CHK_EXISTS_ACTION);
        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            $('input[name=phone]').focus();
            return false;
        }

        return true;
    }

//======================
// Ajax 请求验证手机和验证码，点击下一步时触发
//======================
    function doFpValidatePhoneAndNext(phone, email, verifyCode, type) {
        var errorFlag = false;
        var errorMsg = "";
        var signVal = "";

        $.ajax({
            url: "/usr/fp/verifyPhoneEffect",
            type: "POST",
            dataType: "json",
            async: false,
            data: {
                "phone": phone,
                "email": email,
                "verifyCode": verifyCode,
                "type": type
            },
            success: function (rst) {
                if (rst.code == "success") {
                    signVal = rst.data;  //服务器端返回的签名值，用于下一步的密码修改校验
                } else {
                    errorFlag = true;
                    errorMsg = "校验失败: " + rst.msg;
                }
            },
            error: function (err) {
                errorFlag = true;
                errorMsg = '网络出现点小问题: ' + err;
            }
        });

        return {errorFlag: errorFlag, errorMsg: errorMsg, sign: signVal};
    }

})