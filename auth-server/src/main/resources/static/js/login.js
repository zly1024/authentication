/**
 * 登录页面 login.html 引用的js文件
 *
 * Created by zly on 2017/12/16.
 */

//弹出错误提示
function showErrorOrOk(msg) {
    //throw new TypeError(msg);
    if (msg.length > 0) {
        $("#showErrorMsg").text("× " + msg);
        $("#showError").show();
    } else {
        $("#showErrorMsg").text("");
        $("#showError").hide();
    }
}

$(function () {

    $.ajaxSetup({
        cache: true
    });

    // 登陆错误
    var url = location.href;
    if (url.indexOf("error") > -1) {
        showErrorOrOk("用户名或密码错误！");
    }


    //======================
    // 验证用户名（光标离开）
    //======================
    $('input[name=username]').blur(function () {
        var userName = $.trim($('input[name=username]').val());

        if (userName.length > 0) {
            var data = doValidateUserNameByApi(userName, USERNAME_TYPE, CHK_EXISTS_ACTION);
            if (data.errorFlag) {
                showErrorOrOk(data.errorMsg);
                // $('input[name=username]').focus();
                return false;
            }
            //设置为隐藏，且提示信息为空
            showErrorOrOk("");
        }
    });

    //======================
    // 处理密码（光标离开）
    //======================
    $('input[name=passwordText]').blur(function () {
        var passwordText = $.trim($('input[name=passwordText]').val());

        if (passwordText.length > 0 && passwordText.length != 32) {
            var passwordMd5 = $.md5(passwordText);
            $('input[name=password]').attr("value", passwordMd5);
            //console.log("password(blur) =>" + passwordMd5);
        } else if (passwordText.length == 32) {
            //记住密码的情况
            $('input[name=password]').attr("value", passwordText);
            //console.log("password(blur)2 =>" + passwordText);
        }
    });

    // 表单提交验证
    $("form").submit(function (event) {
        var userName = $.trim($('input[name=username]').val());
        var password = $.trim($('input[name=passwordText]').val());
        var passwordMd5Text = $.trim($('input[name=password]').val());

        if (userName.length == 0) {
            showErrorOrOk("请输入用户名!");
            $('input[name=username]').focus();
            return false;
        }
        if (password.length == 0) {
            showErrorOrOk("请输入登录密码!");
            $('input[name=passwordText]').focus();
            return false;
        }

        //校验用户名是否已存在?
        var data = doValidateUserNameByApi(userName, USERNAME_TYPE, CHK_EXISTS_ACTION);
        if (data.errorFlag) {
            showErrorOrOk(data.errorMsg);
            $('input[name=username]').focus();
            return false;
        }

        //设置提交的密码为加密后的密码
        if (password.length == 32) {
            $('input[name=password]').attr("value", password);
        } else {
            if (passwordMd5Text.length != 32) {
                try {
                    var passwordMd5 = $.md5(password);
                    $('input[name=password]').attr("value", passwordMd5);
                    console.log("password(submit) =>" + passwordMd5);
                    if (password.length > 0 && passwordMd5.length != 32) {
                        //MD后是32位的
                        showErrorOrOk("JS加载异常，密码控件可能损坏，请刷新页面后重试!");
                        return false;
                    }
                } catch (e) {
                    showErrorOrOk("JS加载异常，密码控件可能损坏，请刷新页面后重试! (错误代码:" + e + ")");
                    return false;
                }
            }
        }

        //设置passwordText不提交
        //$('input[name=passwordText]').attr("disabled", true);
        $('input[name=passwordText]').removeAttr("id");
        $('input[name=passwordText]').removeAttr("name");

        //设置为隐藏，且提示信息为空
        showErrorOrOk("");

        return true;
    });
});