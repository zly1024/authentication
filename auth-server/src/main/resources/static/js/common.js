/**
 * Created by zly on 2017/12/19.
 */

//常量定义
var USERNAME_TYPE = "USERNAME";
var PHONE_TYPE = "PHONE";
var EMAIL_TYPE = "EMAIL";

//校验用户方法执行的动作
var CHK_EXISTS_ACTION = "CHK_EXISTS";
var REGISTER_ACTION = "REGISTER";


//校验手机号
function checkPhone(phone) {
    //var regName = /^1(3|4|5|7|8)\d{9}$/;
    //var regName = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;
    var regName = /^(((13[0-9])|14[5|7]|(15[0-3|5-9])|(17[0|3|6-8])|(18[0-9])|(170[0|1]))+\d{8})$/;
    if (regName.test(phone)) {
        return true;
    }
    return false;
}

//验证邮箱
function checkEmail(email) {
    var regName = /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
    if (regName.test(email)) {
        return true;
    }
    return false;
}

//======================
// 验证用户名
//======================
function doValidateUserNameByApi(userName, type, actionFlag) {
    var errorFlag = false;
    var errorMsg = "";

    $.ajax({
        url: "/usr/verifyUserName",
        type: "POST",
        dataType: "json",
        async: false,
        data: {
            "userName": userName,
            "type": type,
            "actionFlag": actionFlag
        },
        success: function (rst) {
            if (rst.code != "success") {
                errorFlag = true;
                errorMsg = rst.msg;
            }
        },
        error: function (err) {
            errorFlag = true;
            errorMsg = "网络出现点小问题: " + err;
        }
    });
    return {errorFlag: errorFlag, errorMsg: errorMsg};
}


//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null && r != "") {
        return decodeURI(r[2]);
    } else {
        return ""; //返回参数值
    }
}