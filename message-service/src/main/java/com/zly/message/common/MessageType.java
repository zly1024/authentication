package com.zly.message.common;

/**
 * @author zly
 * @date 2017/10/24.
 */
public class MessageType {
    /**
     * 邮件
     */
    public static final String MAIL = "MAIL";
    /**
     * 短信
     */
    public static final String SMS = "SMS";
    /**
     * 消息推送
     */
    public static final String PUSH = "PUSH";
    /**
     * 订单付款
     */
    public static final String ORD_PAYMENT = "ORD_PAYMENT";
    /**
     * 商家收款
     */
    public static final String ORG_RECEIPT = "ORG_RECEIPT";
    /**
     * 看板至订单的同步
     */
    public static final String BOARD2ORD_SYNC = "BOARD2ORD_SYNC";

    /**
     * 菜品月销量同步（支付完成后）
     */
    public static final String ITEM_MSQ_SYNC = "ITEM_MSQ_SYNC";
}
