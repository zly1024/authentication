/*
 * #{copyright}#
 */
package com.zly.message.common;


/**
 * @author zly
 */
public class Constant {

    public static final String MESSAGE_STATUS_INIT = "INIT";

    public static final String MESSAGE_STATUS_SUCCESS = "SUCCESS";

    public static final String MESSAGE_STATUS_ERROR = "ERROR";

    public static final int MESSAGE_TO_SEND_ONCE = 20;

    public static final String MESSAGE_TYPE_MAIL = MessageType.MAIL;
    public static final String MESSAGE_TYPE_SMS = MessageType.SMS;
    public static final String MESSAGE_TYPE_PUSH = MessageType.PUSH;

    public static final String MESSAGE_TYPE_TO = "TO";

    public static final String MESSAGE_TYPE_CC = "CC";

    public static final String MESSAGE_TYPE_BCC = "BCC";
    public static final Integer DEFAULT_VERSIONS = 0;
}
