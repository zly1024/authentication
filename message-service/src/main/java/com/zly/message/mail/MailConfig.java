/*
 * #{copyright}#
 */
package com.zly.message.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zly
 */
@Component
public class MailConfig {

    /**
     * 邮件发送时参数，阿里云邮件服务器，采用ssl加密方式发送
     */


    /**
     * 其他配置（系统错误异常相关配置）
     */
    @Value("${sys.support.env}")
    private String currentEnv;

    @Value("${sys.support.error.enabled}")
    private boolean enableSendErrorFlag;

    @Value("#{'${sys.support.error.mailReceivers}'.split(';')}")
    List<String> sysErrMailReceiverList;

    @Value("#{'${sys.support.error.smsReceivers}'.split(';')}")
    List<String> sysErrSmsReceiverList;

    public String getCurrentEnv() {
        return currentEnv;
    }

    public void setCurrentEnv(String currentEnv) {
        this.currentEnv = currentEnv;
    }

    public boolean isEnableSendErrorFlag() {
        return enableSendErrorFlag;
    }

    public void setEnableSendErrorFlag(boolean enableSendErrorFlag) {
        this.enableSendErrorFlag = enableSendErrorFlag;
    }

    public List<String> getSysErrMailReceiverList() {
        return sysErrMailReceiverList;
    }

    public void setSysErrMailReceiverList(List<String> sysErrMailReceiverList) {
        this.sysErrMailReceiverList = sysErrMailReceiverList;
    }

    public List<String> getSysErrSmsReceiverList() {
        return sysErrSmsReceiverList;
    }

    public void setSysErrSmsReceiverList(List<String> sysErrSmsReceiverList) {
        this.sysErrSmsReceiverList = sysErrSmsReceiverList;
    }
}
