package com.zly.message.comfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lingyu.zhou
 * @title: MqConfig
 * @description: 交换机配置
 * @date 2019/4/21 2:12
 */
@Configuration
public class MqConfig {
    /**
     * 短信发送queue
     */
    public static final String MESSAGE_SEND_QUEUE="queue.event.message.send";

    public static final String MESSAGE_SEND_EXCHANGE="exchange.event.message.send";

    public static final String MESSAGE_SEND_KEY = "routeKey.event.message.send";


    /**
     * Direct模式 交换机Exchange
     */
    @Bean(name = "stockUpadte")
    public Queue stockUpadte() {
        return new Queue(MESSAGE_SEND_QUEUE, true);
    }

    /**
     *@Description: 定义交换器
     *
     */
    @Bean(name = "sms_message")
    public TopicExchange exchange(){
        return new TopicExchange(MESSAGE_SEND_EXCHANGE);
    }

    /**
     *@Description: 交换机与消息队列进行绑定 队列messages绑定交换机with topic.messages
     *
     */
    @Bean
    Binding bindingExchangeMessages(@Qualifier("stockUpadte") Queue queueMessages, @Qualifier("sms_message") TopicExchange exchange){
        return BindingBuilder.bind(queueMessages).to(exchange).with(MESSAGE_SEND_KEY);
    }
}
