/*
 * #{copyright}#
 */
package com.zly.message.sender.task.mail;

import com.zly.common.domain.comm.CiipCommAttachment;
import com.zly.common.domain.comm.CiipCommReceiver;
import com.zly.common.dto.MessageDto;
import com.zly.common.util.StringBuilderUtil;
import com.zly.message.common.Constant;
import com.zly.message.sender.task.AbstractScheduleMessageSender;
import com.zly.message.service.MessageService;
import com.zly.message.service.MessageTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.UrlResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StringUtils;

import javax.mail.internet.MimeMessage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zly
 */
public class MailScheduleMessageSender extends AbstractScheduleMessageSender {

    private static Logger logger = LoggerFactory.getLogger(MailScheduleMessageSender.class);

    private JavaMailSender mailSender;

    private StringBuffer receiverErrorMsg = new StringBuffer("");  //校验收件人错误消息

    public MailScheduleMessageSender(MessageDto messageDto, MessageService messageService, MessageTemplateService messageTemplateService, JavaMailSender mailSender) {
        super(messageDto, messageService, messageTemplateService);
        this.mailSender = mailSender;
    }

    /**
     * 校验发送邮件的消息内容
     * @param message
     * @return
     */
    private boolean validateMessage(MessageDto message) {
        if (StringUtils.isEmpty(message.getSender())) {
            message.setErrorMsg("sender cannot be null;");
            return false;
        }
        if (!MailUtils.checkEmail(message.getSender())) {
            message.setErrorMsg("Invalid sender;");
            return false;
        }
        return true;
    }

    @Override
    public MessageDto sendMessage(MessageDto message) {
        try {
            logger.debug("==========发送邮件: { Entering sendMessage() } ==========", message.getId());

            //检查发送邮件的消息内容
            if (!validateMessage(message)) {
                throw new IllegalArgumentException(message.getErrorMsg());
            }

            MimeMessage mimeMessage = mailSender.createMimeMessage();

            MimeMessageHelper helper = null;
            helper = new MimeMessageHelper(mimeMessage, true);
            helper.setSubject(message.getSubject());
            helper.setText(message.getContent());
            helper.setFrom(message.getSender());

            List<String> to = new ArrayList<>();
            List<String> cc = new ArrayList<>();
            List<String> bcc = new ArrayList<>();

            for (CiipCommReceiver receiver : message.getReceivers()) {
                //校验收件人地址
                if (StringUtils.isEmpty(receiver.getReceiverAddress()) || !MailUtils.checkEmail(receiver.getReceiverAddress())) {
                    receiverErrorMsg.append("Invalid ReceiverAddress[" + receiver.getReceiverAddress() + "];");
                    continue;
                }

                switch (receiver.getSendType()) {
                    case Constant.MESSAGE_TYPE_TO:
                        to.add(receiver.getReceiverAddress());
                        break;
                    case Constant.MESSAGE_TYPE_CC:
                        cc.add(receiver.getReceiverAddress());
                        break;
                    case Constant.MESSAGE_TYPE_BCC:
                        bcc.add(receiver.getReceiverAddress());
                        break;
                    default:
                        break;
                }
            }
            if (to.size() > 0) {
                String[] toArray = new String[to.size()];
                helper.setTo(to.toArray(toArray));
            }
            if (cc.size() > 0) {
                String[] ccArray = new String[cc.size()];
                helper.setCc(cc.toArray(ccArray));
            }
            if (bcc.size() > 0) {
                String[] bccArray = new String[bcc.size()];
                helper.setBcc(bcc.toArray(bccArray));
            }

            if (message.getAttachments() != null) {
                for (CiipCommAttachment attachment : message.getAttachments()) {
                    try {
                        URL url = new URL(attachment.getAttachmentUrl());
                        helper.addAttachment(attachment.getAttachmentName(), new UrlResource(url));
                    } catch (Exception e) {
                        logger.error("附件未找到!", e);
                        message.setErrorMsg(StringBuilderUtil.append(message.getErrorMsg(), ",附件:", attachment.getAttachmentName(), "不存在!"));
                        continue;
                    }
                }
            }

            mailSender.send(mimeMessage);
            message.setStatus(Constant.MESSAGE_STATUS_SUCCESS);
            message.setErrorMsg(receiverErrorMsg.toString().length() > 1 ? receiverErrorMsg.toString() : null);
            message.setSendTime(new Date());

        } catch (IllegalArgumentException iae) {
            logger.error("发送邮件失败，消息参数值校验不通过!", iae);
            message.setStatus(Constant.MESSAGE_STATUS_ERROR);
        } catch (Exception e) {
            logger.error("发送邮件失败，未知异常!", e);
            message.setStatus(Constant.MESSAGE_STATUS_ERROR);
            message.setErrorMsg(e.getMessage());
        }

        logger.debug("========== { Leaving sendMessage() } ==========", message.getId());

        return message;
    }
}
