package com.zly.message.sender.task.mail;

import java.util.regex.Pattern;

/**
 * Created by zly on 2017/9/13.
 */
public class MailUtils {

    /**
     * 匹配返回true，不匹配返回false
     * @param email
     * @return
     */
    public static boolean checkEmail(String email) {
        String regex = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$";
        return Pattern.matches(regex, email);
    }

}
