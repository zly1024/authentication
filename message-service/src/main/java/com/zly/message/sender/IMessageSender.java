/*
 * #{copyright}#
 */
package com.zly.message.sender;

import com.zly.common.dto.MessageDto;

/**
 * @author zly
 */
public interface IMessageSender extends Runnable {

    public MessageDto sendMessage(MessageDto messageDto);

}
