/*
 * #{copyright}#
 */
package com.zly.message.sender.task.sms;

import com.zly.common.domain.comm.CiipCommReceiver;
import com.zly.common.dto.MessageDto;
import com.zly.message.common.Constant;
import com.zly.message.sender.task.AbstractScheduleMessageSender;
import com.zly.message.service.MessageService;
import com.zly.message.service.MessageTemplateService;
import com.zly.message.sms.SingleSendSms;
import com.zly.message.sms.SmsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @author zly
 */
public class SmsScheduleMessageSender extends AbstractScheduleMessageSender {

    private static Logger logger = LoggerFactory.getLogger(SmsScheduleMessageSender.class);

    private SmsConfig smsConfig;

    public SmsScheduleMessageSender(MessageDto messageDto, MessageService messageService, MessageTemplateService messageTemplateService, SmsConfig smsConfig) {
        super(messageDto, messageService, messageTemplateService);
        this.smsConfig = smsConfig;
    }

    @Override
    public MessageDto sendMessage(MessageDto message) {
        //Map<String, String> urlVariables = new HashMap<String, String>();
        StringBuffer receiverList = new StringBuffer("");

        try {
            logger.debug("**********发送短信: {}**********", message.getId());

            for(CiipCommReceiver receiver : message.getReceivers()) {
                if (receiverList != null && !"".equals(receiverList.toString())) {
                    //多个手机号可以用英文逗号分割
                    receiverList.append("," + receiver.getReceiverAddress());
                } else {
                    receiverList.append(receiver.getReceiverAddress());
                }
            }

            // 调用短信发送API
            SingleSendSms sms = new SingleSendSms(smsConfig.getSmsHost(), smsConfig.getSmsActionPath(),
                    smsConfig.getSmsAppCode(), smsConfig.getSmsAppKey(), smsConfig.getSmsAppSecret(),
                    smsConfig.getSmsFreeSignName(),
                    getSmsTemplateCode() /*smsConfig.getSmsTemplateCode()*/ );
            sms.sendMsg2(message.getId(), receiverList.toString(), message.getContent());
            logger.debug("***调用短信接口返回结果***: " + sms.toString());

            if (sms.getRetSuccessFlag()) {
                message.setStatus(Constant.MESSAGE_STATUS_SUCCESS);
                message.setErrorMsg(sms.getRetMessage());
            } else {
                message.setStatus(Constant.MESSAGE_STATUS_ERROR);
                message.setErrorMsg(sms.getRetMessage());
            }
            message.setSendTime(new Date());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("***调用发送SMS短信消息接口异常:", e.getMessage());
            message.setStatus(Constant.MESSAGE_STATUS_ERROR);
            message.setErrorMsg(e.getMessage());
        }
        return message;
    }

    // 短信发送模版code
    private String getSmsTemplateCode() {
        if (this.messageTemplate != null && !StringUtils.isEmpty(this.messageTemplate.getSmsTemplateCode())) {
            return this.messageTemplate.getSmsTemplateCode();
        } else {
            return smsConfig.getSmsTemplateCode();  //配置文件默认，优先从表获取
        }
    }
}
