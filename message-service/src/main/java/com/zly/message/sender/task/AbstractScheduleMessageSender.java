/*
 * #{copyright}#
 */
package com.zly.message.sender.task;

import com.zly.common.domain.comm.CiipCommMessageTemplate;
import com.zly.common.dto.MessageDto;
import com.zly.message.common.Constant;
import com.zly.message.sender.IMessageSender;
import com.zly.message.service.MessageService;
import com.zly.message.service.MessageTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zly
 */
public abstract class AbstractScheduleMessageSender implements IMessageSender {

    private static Logger logger = LoggerFactory.getLogger(AbstractScheduleMessageSender.class);

    private static final int MAX_TRY_TIME = 3;

    private static final int DEFAULT_SLEEP_TIME = 1000;  //1s

    protected CiipCommMessageTemplate messageTemplate;  //消息模版

    private MessageDto message;

    private MessageService messageService;
    private MessageTemplateService messageTemplateService;

    private AtomicInteger tryTime = new AtomicInteger(1);
    private Integer currentRetryTimes = 0;


    public AbstractScheduleMessageSender(MessageDto messageDto, MessageService messageService, MessageTemplateService messageTemplateService) {
        this.message = messageDto;
        this.messageService = messageService;
        this.messageTemplateService = messageTemplateService;
    }

    @Override
    public void run() {

        if (message.getId() == null) {
            try {
                message = messageService.saveMessage(message);
            } catch (Exception e) {
                logger.error("==========保存消息异常==========", e);
                return;
            }
        }

        logger.debug("==========开始发送消息: {}==========", message.getId());

        //获取消息模版内容
        initMessageTemplate();

        // 发送消息
        // 默认失败 等待1秒，重试3次
        currentRetryTimes = 0;
        while ( tryTime.getAndIncrement() <= MAX_TRY_TIME) {
            logger.debug("开始第 " + currentRetryTimes + " 次发送: ");
            message = this.sendMessage(message);
            logger.debug("  [" + currentRetryTimes + "]-->message.getStatus(" + message.getId() + ") = " + message.getStatus());
            if (!Constant.MESSAGE_STATUS_SUCCESS.equals(message.getStatus())) {
                currentRetryTimes++;
                try {
                    Thread.sleep(DEFAULT_SLEEP_TIME);
                } catch (InterruptedException e) {
                    logger.error("==========异常==========", e);
                }
            } else {
                tryTime.set(MAX_TRY_TIME + 1);  //发送成功则不再重试
            }

        }

        // 保存消息
        try {
            message.setSendTime(new Date());
            message.setRetryTimes(currentRetryTimes);
            messageService.saveMessageStatus(message);
            logger.debug("==========发送消息结束: {}, {}, {}==========", message.getId(), message.getStatus(), message.getErrorMsg());
        } catch (Exception e) {
            logger.error("==========保存消息异常==========", e);
            return;
        }
    }

    //根据MessageDto的messageTemplateCode获取系统定义的消息模版
    private void initMessageTemplate() {
        if (!StringUtils.isEmpty(message.getMessageTemplateCode())) {
            CiipCommMessageTemplate messageTemplateQuery = new CiipCommMessageTemplate();
            messageTemplateQuery.setEnabledFlag("Y");
            messageTemplateQuery.setMessageType(message.getMessageType());
            messageTemplateQuery.setMessageTemplateCode(message.getMessageTemplateCode());  //消息模版

            this.messageTemplate = messageTemplateService.selectOne(messageTemplateQuery);
        }
    }
}
