/*
 * #{copyright}#
 */
package com.zly.message.sms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author zly
 * @date
 */
public class SingleSendSms {

    private static Logger logger = LoggerFactory.getLogger(SingleSendSms.class);

    private /*final static*/ String APP_CODE = "";
    private /*final static*/ String APP_KEY = ""; //AppKey从控制台获取
    private /*final static*/ String APP_SECRET = ""; //AppSecret从控制台获取
    private /*final static*/ String SIGN_NAME = ""; // 签名名称从控制台获取，必须是审核通过的
    private /*final static*/ String TEMPLATE_CODE = ""; //模板CODE从控制台获取，必须是审核通过的
    private /*final static*/ String HOST = ""; //API域名从控制台获取
    private /*final static*/ String PATH = "";

    private final static int HTTP_OK = 200;
    private final static String RET_TRUE = "true";
    private final static String RET_FALSE = "false";
    private final static String RET_CODE_OK = "OK";
    private final static String ERROR_KEY = "errorMessage";  //返回错误的key

    // 接口返回状态 & 信息
    private boolean retSuccessFlag = false;  //true & false
    private String retMessage = "";


    public SingleSendSms(String host, String path, String appCode, String appKey, String appSecret, String signName, String templateCode) {
        this.HOST = host;
        this.PATH = path;
        this.APP_CODE = appCode;
        this.APP_KEY = appKey;
        this.APP_SECRET = appSecret;
        this.SIGN_NAME = signName;
        this.TEMPLATE_CODE = templateCode;
    }

    /**
     *
     * @param messageId  消息表ID
     * @param phoneNum  目标手机号，多个手机号可以逗号分隔;
     * @param params  短信模板中的变量，数字必须转换为字符串，如短信模板中变量为${no}",则参数params的值为{"no":"123456"}
     */
    public void sendMsg2(String messageId, String phoneNum, String params) {
        if (phoneNum == null || "".equals(phoneNum.trim())) {
            setRetMessage("校验失败:手机号不能为空!");
            return;
        }

        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化ascClient需要的几个参数
        final String product = APP_CODE;  //短信API产品名称
        final String domain = HOST;  //短信API产品域名
        //替换成你的AK
        final String accessKeyId = APP_KEY;  //你的accessKeyId,参考本文档步骤2
        final String accessKeySecret = APP_SECRET;  //你的accessKeySecret，参考本文档步骤2

        try {
            logger.info("--------->阿里云短信接口发送短信");
            //初始化ascClient,暂时不支持多region
            DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            IAcsClient client = new DefaultAcsClient(profile);

            //组装请求对象
            CommonRequest request = new CommonRequest();
            request.setProtocol(ProtocolType.HTTPS);
            request.setMethod(MethodType.POST);
            request.setDomain("dysmsapi.aliyuncs.com");
            request.setVersion("2017-05-25");
            request.setAction("SendSms");
            request.putQueryParameter("RegionId", "cn-hangzhou");
            //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为20个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
            request.putQueryParameter("PhoneNumbers", phoneNum);
            request.putQueryParameter("SignName", SIGN_NAME);
            request.putQueryParameter("TemplateCode", TEMPLATE_CODE);
            request.putQueryParameter("OutId", messageId);
            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            request.putQueryParameter("TemplateParam", params);
            try {
                CommonResponse response = client.getCommonResponse(request);
                logger.info(response.getData());
                this.retSuccessFlag = true;
                setRetMessage(response.getData());
            } catch (ServerException e) {
                setRetMessage("调用SingleSendSms.sendMsg2()发送SMS消息异常: " + e.getMessage());
                e.printStackTrace();
            } catch (ClientException e) {
                setRetMessage("调用SingleSendSms.sendMsg2()发送SMS消息异常: " + e.getMessage());
                e.printStackTrace();
            }

        } catch (Exception e) {
            setRetMessage("调用SingleSendSms.sendMsg2()发送SMS消息异常: " + e.getMessage());
            logger.debug(getRetMessage());
        }

    }

    private CommonResponse querySendDetails(String bizId) throws ClientException,ServerException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化Client,暂不支持region化
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", APP_KEY, APP_SECRET);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("QuerySendDetails");
        request.putQueryParameter("PhoneNumber", "1111");
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.putQueryParameter("SendDate", ft.format(new Date()));
        request.putQueryParameter("PageSize", "10");
        request.putQueryParameter("CurrentPage", "1");
        request.putQueryParameter("BizId", bizId);
        CommonResponse response = acsClient.getCommonResponse(request);
        logger.info(response.getData());
        return response;
    }

    private Map<String, String> ReadResponseBodyContent(String body) {
        Map<String, String> map = new HashMap<String, String>();
        try {
            JSONObject jsonObject = JSON.parseObject(body);
            if (null != jsonObject) {
                for (Entry<String, Object> entry : jsonObject.entrySet()) {
                    map.put(entry.getKey(), entry.getValue().toString());
                }
            }
            if (RET_FALSE.equals(map.get("success"))) {
                map.put(ERROR_KEY, map.get("message"));
            }
        } catch (Exception e) {
            map.put(ERROR_KEY, "调用ReadResponseBodyContent异常: " + body + e.toString());
        }
        return map;
    }

    public boolean getRetSuccessFlag() {
        return retSuccessFlag;
    }

    public String getRetMessage() {
        return retMessage;
    }

    public void setRetMessage(String retMessage) {
        this.retMessage = retMessage;
    }

    @Override
    public String toString() {
        return "SingleSendSms{" +
                "APP_KEY='" + APP_KEY + '\'' +
                ", APP_SECRET='" + APP_SECRET + '\'' +
                ", SIGN_NAME='" + SIGN_NAME + '\'' +
                ", TEMPLATE_CODE='" + TEMPLATE_CODE + '\'' +
                ", HOST='" + HOST + '\'' +
                ", PATH='" + PATH + '\'' +
                ", retSuccessFlag='" + retSuccessFlag + '\'' +
                ", retMessage='" + retMessage + '\'' +
                '}';
    }

}
