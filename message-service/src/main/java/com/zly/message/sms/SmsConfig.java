/*
 * #{copyright}#
 */
package com.zly.message.sms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author zly
 * @date
 */
@Component
public class SmsConfig {

    /*
    * 发送SMS短信需要的参数，从配置文件读取
    * */
    @Value("${spring.sms.host}")
    private String smsHost;
    @Value("${spring.sms.actionPath}")
    private String smsActionPath;
    @Value("${spring.sms.appCode}")
    private String smsAppCode;
    @Value("${spring.sms.appKey}")
    private String smsAppKey;
    @Value("${spring.sms.appSecret}")
    private String smsAppSecret;
    @Value("${spring.sms.smsFreeSignName}")
    private String smsFreeSignName;
    @Value("${spring.sms.smsTemplateCode}")
    private String smsTemplateCode;

    public String getSmsHost() {
        return smsHost;
    }

    public void setSmsHost(String smsHost) {
        this.smsHost = smsHost;
    }

    public String getSmsActionPath() {
        return smsActionPath;
    }

    public void setSmsActionPath(String smsActionPath) {
        this.smsActionPath = smsActionPath;
    }

    public String getSmsAppCode() {
        return smsAppCode;
    }

    public void setSmsAppCode(String smsAppCode) {
        this.smsAppCode = smsAppCode;
    }

    public String getSmsAppKey() {
        return smsAppKey;
    }

    public void setSmsAppKey(String smsAppKey) {
        this.smsAppKey = smsAppKey;
    }

    public String getSmsAppSecret() {
        return smsAppSecret;
    }

    public void setSmsAppSecret(String smsAppSecret) {
        this.smsAppSecret = smsAppSecret;
    }

    public String getSmsFreeSignName() {
        return smsFreeSignName;
    }

    public void setSmsFreeSignName(String smsFreeSignName) {
        this.smsFreeSignName = smsFreeSignName;
    }

    public String getSmsTemplateCode() {
        return smsTemplateCode;
    }

    public void setSmsTemplateCode(String smsTemplateCode) {
        this.smsTemplateCode = smsTemplateCode;
    }
}
