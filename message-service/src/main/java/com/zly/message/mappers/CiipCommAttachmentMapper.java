package com.zly.message.mappers;

import com.zly.common.domain.comm.CiipCommAttachment;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author Administrator
 */
public interface CiipCommAttachmentMapper extends Mapper<CiipCommAttachment> {
}