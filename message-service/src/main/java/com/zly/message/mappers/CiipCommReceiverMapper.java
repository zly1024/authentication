package com.zly.message.mappers;

import com.zly.common.domain.comm.CiipCommReceiver;
import tk.mybatis.mapper.common.Mapper;

public interface CiipCommReceiverMapper extends Mapper<CiipCommReceiver> {
}