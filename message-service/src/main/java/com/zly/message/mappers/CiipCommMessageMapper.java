package com.zly.message.mappers;


import com.zly.common.domain.comm.CiipCommMessage;
import tk.mybatis.mapper.common.Mapper;

public interface CiipCommMessageMapper extends Mapper<CiipCommMessage> {
}