/*
 * #{copyright}#
 */
package com.zly.message.mq;

import com.zly.common.dto.MessageDto;
import com.zly.message.comfig.MqConfig;
import com.zly.message.common.Constant;
import com.zly.message.sender.IMessageSender;
import com.zly.message.sender.task.mail.MailScheduleMessageSender;
import com.zly.message.sender.task.sms.SmsScheduleMessageSender;
import com.zly.message.service.MessageService;
import com.zly.message.service.MessageTemplateService;
import com.zly.message.sms.SmsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * @author zly
 */
@Component
public class MessageConsumer {
    private final static Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageTemplateService messageTemplateService;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SmsConfig smsConfig;

    @Autowired
    @Qualifier("taskScheduler")
    private TaskExecutor taskExecutor;

    @RabbitListener(queues = MqConfig.MESSAGE_SEND_QUEUE)
    public void receiveMessage(MessageDto messageDto) throws InterruptedException {
        logger.debug("Receive Email Message : {}", messageDto);

        IMessageSender messageSender = null;
        if (Constant.MESSAGE_TYPE_SMS.equals(messageDto.getMessageType())) {
            messageSender = new SmsScheduleMessageSender(messageDto, messageService, messageTemplateService, smsConfig);
        } else {
            messageSender = new MailScheduleMessageSender(messageDto, messageService, messageTemplateService, mailSender);
        }
        taskExecutor.execute(messageSender);
    }

}
