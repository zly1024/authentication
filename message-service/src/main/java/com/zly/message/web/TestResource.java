package com.zly.message.web;

import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lingyu.zhou@hand-china.com
 * @title: TestResource
 * @description: 测试服务间token传递
 * @date 2019/4/23 10:12
 */
@RequestMapping
@RestController
public class TestResource extends BaseController{

    @RequestMapping(value = "/message/test/get",method = RequestMethod.POST)
    public ResponseEntity<String> get(){
        return success("调用成功");
    }

}
