/*
 * #{copyright}#
 */
package com.zly.message.web;

import com.zly.common.domain.comm.CiipCommReceiver;
import com.zly.common.dto.MessageDto;
import com.zly.common.util.BeanUtil;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.message.comfig.MqConfig;
import com.zly.message.common.Constant;
import com.zly.message.mail.MailConfig;
import com.zly.message.sender.task.mail.MailUtils;
import com.zly.message.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * @author zly
 */
@RestController
public class MessageResource extends BaseController {
    private Logger logger = (Logger) LoggerFactory.getLogger(MessageResource.class);

    @Value("${spring.mail.defaultSender}")
    private String defaultSender;

    @Autowired
    MailConfig sysSupportConfig;

    @Autowired
    private MessageService messageService;

    @Autowired
    private RabbitMessagingTemplate rabbitMessagingTemplate;

    @RequestMapping(value = "/message/send", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> sendMsg(@RequestBody MessageDto messageDto) {
        if (StringUtils.isEmpty(messageDto.getMessageType())) {
            return fail("消息发送类型(参数MessageType值)不能为空!");
        }

        if (Constant.MESSAGE_TYPE_MAIL.equals(messageDto.getMessageType())) {
            if (StringUtils.isEmpty(messageDto.getSender())) {
                messageDto.setSender(defaultSender);
            }
            //校验发件人是否正确
            if (!MailUtils.checkEmail(messageDto.getSender())) {
                return fail("非法的消息发件人(参数Sender值)!");
            }
        }

        if (messageDto.getReceivers() == null || messageDto.getReceivers().size() == 0) {
            return fail("收件人列表(参数Receivers)不能为空!");
        }

        messageDto = messageService.saveMessage(messageDto);

        try {
            rabbitMessagingTemplate.convertAndSend(MqConfig.MESSAGE_SEND_EXCHANGE,MqConfig.MESSAGE_SEND_KEY, messageDto);
        } catch (AmqpException e) {
            logger.error("send mq error:"+e.getMessage());
            return fail("send mq error:"+e.getMessage());
        }

        return success(messageDto.getId());
    }
/*
    @RequestMapping(value = "/message/push", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> pushMsg(@RequestBody MessageDto messageDto) {
        messageDto.setMessageType(Constant.MESSAGE_TYPE_PUSH);
        if (StringUtils.isEmpty(messageDto.getSaveMsgFlag()) || !"N".equalsIgnoreCase(messageDto.getSaveMsgFlag())) {
            messageService.saveMessage(messageDto);
        }
        rabbitMessagingTemplate.convertAndSend("amq.topic","topic.event.message.push", messageDto);
        return success("success!");
    }*/

    /**
     * 此方法专用于发送系统错误类的消息到指定账户（请勿用于其他正常业务用途）
     *
     * @param messageDto
     * @return
     */
    @RequestMapping(value = "/message/sysError/send", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> sendSysErrorMsg(@RequestBody MessageDto messageDto) {

        try {
            //是否开启发送功能
            if (sysSupportConfig.isEnableSendErrorFlag()) {
                //=======================
                //(1)发送邮件
                //=======================
                MessageDto mailMessageDto = BeanUtil.copyBean(messageDto, MessageDto.class);
                sendSysErrorByEmail(mailMessageDto);

                //(2)发送短信
                //暂时不提供
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success("success");
    }

    private ResponseEntity sendSysErrorByEmail(MessageDto messageDto) {
        if (sysSupportConfig.getSysErrMailReceiverList() != null && !sysSupportConfig.getSysErrMailReceiverList().isEmpty()) {
            messageDto.setMessageType(Constant.MESSAGE_TYPE_MAIL);
            if (StringUtils.isEmpty(messageDto.getSender())) {
                messageDto.setSender(defaultSender);
            }
            //校验发件人是否正确
            if (!MailUtils.checkEmail(messageDto.getSender())) {
                return fail("非法的消息发件人(参数Sender值)!");
            }

            //邮件主题添加环境信息
            messageDto.setSubject(messageDto.getSubject() + " - " + sysSupportConfig.getCurrentEnv());

            List<CiipCommReceiver> receiverList = messageDto.getReceivers();
            if (receiverList == null) {
                receiverList = new ArrayList<>();
            }
            //接收人
            for (String receiver : sysSupportConfig.getSysErrMailReceiverList()) {
                if (!StringUtils.isEmpty(receiver) && !"NONE".equalsIgnoreCase(receiver.trim()) &&
                        MailUtils.checkEmail(receiver.trim())) {
                    CiipCommReceiver ciipCommReceiver = new CiipCommReceiver();
                    ciipCommReceiver.setSendType("TO");
                    ciipCommReceiver.setReceiverAddress(receiver.trim());
                    receiverList.add(ciipCommReceiver);
                }
            }

            if (receiverList != null && !receiverList.isEmpty()) {
                messageDto.setReceivers(receiverList);
                messageDto = messageService.saveMessage(messageDto);

                //rabbitMessagingTemplate.convertAndSend("queue.event.message.send", messageDto);
            }
        }

        return success("success");
    }
}
