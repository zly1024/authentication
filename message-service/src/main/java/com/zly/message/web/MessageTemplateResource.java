package com.zly.message.web;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.comm.CiipCommMessageTemplate;
import com.zly.common.web.BaseController;
import com.zly.common.web.ResponseEntity;
import com.zly.message.service.MessageTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/messageTemplate")
public class MessageTemplateResource extends BaseController {
    @Autowired
    private MessageTemplateService messageTemplateService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<CiipCommMessageTemplate> get(String id) {
        return success(messageTemplateService.get(id));
    }

    @RequestMapping(value = "/selectOne", method = RequestMethod.GET)
    public ResponseEntity<CiipCommMessageTemplate> selectOne(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return success(messageTemplateService.selectOne(ciipCommMessageTemplate));
    }

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public ResponseEntity<PageInfo<CiipCommMessageTemplate>> query(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return success(messageTemplateService.find(ciipCommMessageTemplate));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<List<CiipCommMessageTemplate>> list(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return success(messageTemplateService.findList(ciipCommMessageTemplate));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<CiipCommMessageTemplate> save(@RequestBody CiipCommMessageTemplate ciipCommMessageTemplate) {
        return success(messageTemplateService.save(ciipCommMessageTemplate));
    }
}