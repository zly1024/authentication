package com.zly.message.service.impl;

import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zly.common.domain.BaseDomain;
import com.zly.common.domain.comm.CiipCommMessageTemplate;
import com.zly.message.mappers.CiipCommMessageTemplateMapper;
import com.zly.message.service.MessageTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * date 2017-11-01
 * description CiipCommMessageTemplateservice
 */
@Service
public class MessageTemplateServiceImpl implements MessageTemplateService {
    @Autowired
    private CiipCommMessageTemplateMapper ciipCommMessageTemplateMapper;

    @Override
    public List<CiipCommMessageTemplate> findList(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return ciipCommMessageTemplateMapper.select(ciipCommMessageTemplate);
    }

    @Override
    public CiipCommMessageTemplate selectOne(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return ciipCommMessageTemplateMapper.selectOne(ciipCommMessageTemplate);
    }

    @Override
    public CiipCommMessageTemplate get(String id) {
        CiipCommMessageTemplate ciipCommMessageTemplate = new CiipCommMessageTemplate();
        ciipCommMessageTemplate.setId(id);
        return ciipCommMessageTemplateMapper.selectByPrimaryKey(ciipCommMessageTemplate);
    }

    @Override
    public List<CiipCommMessageTemplate> findAll() {
        return ciipCommMessageTemplateMapper.selectAll();
    }

    @Override
    public PageInfo<CiipCommMessageTemplate> find(final CiipCommMessageTemplate ciipCommMessageTemplate) {
        if (ciipCommMessageTemplate.getPageNum() == null) {
            ciipCommMessageTemplate.setPageNum(BaseDomain.DEFALUT_PAGE_NUM);
        }
        if (ciipCommMessageTemplate.getPageSize() == null) {
            ciipCommMessageTemplate.setPageSize(BaseDomain.DEFALUT_PAGE_SIZE);
        }
        return PageHelper.startPage(ciipCommMessageTemplate.getPageNum(), ciipCommMessageTemplate.getPageSize()).setOrderBy(ciipCommMessageTemplate.getOrderBy()).doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                ciipCommMessageTemplateMapper.select(ciipCommMessageTemplate);
            }
        });
    }

    @Override
    public CiipCommMessageTemplate save(CiipCommMessageTemplate ciipCommMessageTemplate) {
        if (StringUtils.isEmpty(ciipCommMessageTemplate.getId())) {
            ciipCommMessageTemplateMapper.insert(ciipCommMessageTemplate);
        } else {
            ciipCommMessageTemplateMapper.updateByPrimaryKeySelective(ciipCommMessageTemplate);
        }
        return ciipCommMessageTemplate;
    }

    @Override
    public int delete(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return ciipCommMessageTemplateMapper.delete(ciipCommMessageTemplate);
    }
}