/*
 * #{copyright}#
 */
package com.zly.message.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.SqlUtil;
import com.zly.common.domain.comm.CiipCommAttachment;
import com.zly.common.domain.comm.CiipCommMessage;
import com.zly.common.domain.comm.CiipCommReceiver;
import com.zly.common.dto.MessageDto;
import com.zly.message.common.Constant;
import com.zly.message.mappers.CiipCommAttachmentMapper;
import com.zly.message.mappers.CiipCommMessageMapper;
import com.zly.message.mappers.CiipCommReceiverMapper;
import com.zly.message.service.MessageService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zly
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private CiipCommMessageMapper messageMapper;

    @Autowired
    private CiipCommReceiverMapper receiverMapper;

    @Autowired
    private CiipCommAttachmentMapper attachmentMapper;

    @Override
    public MessageDto saveMessage(MessageDto dto) {
        dto.setStatus(Constant.MESSAGE_STATUS_INIT);
        dto.setRetryTimes(0);

        if (dto.getSendTime() == null) {
            dto.setSendTime(new Date());
        }
        if (dto.getSender() == null) {
            dto.setSender("");
        }

        int result = messageMapper.insert(dto);

        if(result > 0) {
            Date now = new Date();
            List<CiipCommReceiver> receivers = dto.getReceivers();
            if (receivers != null && receivers.size() > 0) {
                for (CiipCommReceiver receiver : receivers) {
                    receiver.setMessageId(dto.getId());
                    if (receiver.getSendType() == null) {
                        receiver.setSendType(Constant.MESSAGE_TYPE_TO);
                    }
                    receiver.setCreationDate(now);
                    receiver.setLastUpdateDate(now);
                    receiver.setVersions(Constant.DEFAULT_VERSIONS);
                    receiverMapper.insert(receiver);
                }
            }

            List<CiipCommAttachment> attachments = dto.getAttachments();
            if (attachments != null && attachments.size() > 0) {
                for (CiipCommAttachment attachment : attachments) {
                    attachment.setMessageId(dto.getId());
                    attachment.setCreationDate(now);
                    attachment.setLastUpdateDate(now);
                    attachment.setVersions(Constant.DEFAULT_VERSIONS);
                    attachmentMapper.insert(attachment);
                }
            }
        }

        return dto;
    }

    @Override
    public List<MessageDto> findMessagesToSend(MessageDto dto) {
        // 一次获取有限数量的消息
        PageHelper.startPage(1, Constant.MESSAGE_TO_SEND_ONCE);

        List<CiipCommMessage> messages = messageMapper.select(dto);

        SqlUtil.clearLocalPage();

        List<MessageDto> messageDtos = new ArrayList<>();
        for(CiipCommMessage message : messages) {
            MessageDto messageDto = new MessageDto();
            BeanUtils.copyProperties(message, messageDto);

            CiipCommReceiver receiverSearch = new CiipCommReceiver();
            receiverSearch.setMessageId(message.getId());
            List<CiipCommReceiver> receivers = receiverMapper.select(receiverSearch);
            messageDto.setReceivers(receivers);

            CiipCommAttachment attachmentSearch = new CiipCommAttachment();
            attachmentSearch.setMessageId(message.getId());
            List<CiipCommAttachment> attachments = attachmentMapper.select(attachmentSearch);
            messageDto.setAttachments(attachments);

            messageDtos.add(messageDto);
        }
        return messageDtos;
    }

    @Override
    @Transactional
    public int saveMessageStatus(MessageDto dto) {
        return messageMapper.updateByPrimaryKeySelective(dto);
    }
}
