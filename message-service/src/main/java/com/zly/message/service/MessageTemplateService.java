package com.zly.message.service;

import com.github.pagehelper.PageInfo;
import com.zly.common.domain.comm.CiipCommMessageTemplate;

import java.util.List;

/**
 * date 2017-11-01
 * description CiipCommMessageTemplateservice
 */
public interface MessageTemplateService {
    /**
     * 根据条件查询一个
     *
     * @param ciipCommMessageTemplate
     * @return CiipCommMessageTemplate
     */
    public CiipCommMessageTemplate selectOne(CiipCommMessageTemplate ciipCommMessageTemplate);

    /**
     * 根据id查询
     *
     * @param id
     * @return CiipCommMessageTemplate
     */
    public CiipCommMessageTemplate get(String id);

    /**
     * 查询符合条件的数据,不分页
     *
     * @param ciipCommMessageTemplate
     * @return List<CiipCommMessageTemplate>
     */
    public List<CiipCommMessageTemplate> findList(CiipCommMessageTemplate ciipCommMessageTemplate);

    /**
     * 查询所有数据
     *
     * @return List<CiipCommMessageTemplate>
     */
    public List<CiipCommMessageTemplate> findAll();

    /**
     * 查询符合条件的数据,并且分页,默认pageNum=1,pageSize=100
     *
     * @param ciipCommMessageTemplate
     * @return PageInfo<CiipCommMessageTemplate>
     */
    public PageInfo<CiipCommMessageTemplate> find(CiipCommMessageTemplate ciipCommMessageTemplate);

    /**
     * 保存
     *
     * @param ciipCommMessageTemplate
     * @return CiipCommMessageTemplate
     */
    public CiipCommMessageTemplate save(CiipCommMessageTemplate ciipCommMessageTemplate);

    /**
     * 删除符合条件的数据
     *
     * @param ciipCommMessageTemplate
     * @return int 删除条数
     */
    public int delete(CiipCommMessageTemplate ciipCommMessageTemplate);
}