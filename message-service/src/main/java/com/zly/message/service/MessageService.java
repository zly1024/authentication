/*
 * #{copyright}#
 */
package com.zly.message.service;

import com.zly.common.dto.MessageDto;

import java.util.List;

/**
 * @author zly
 */
public interface MessageService {

    MessageDto saveMessage(MessageDto dto);

    List<MessageDto> findMessagesToSend(MessageDto dto);

    int saveMessageStatus(MessageDto dto);
}
