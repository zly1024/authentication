create table ciip_comm.ciip_comm_message_template
(
  id varchar(50) not null comment '消息模版ID',
  message_type varchar(10) NOT NULL COMMENT '消息类型: MAIL,SMS',
  message_template_code varchar(50) not null comment '消息模版代码CODE，系统自定义的模版代码',
  template_name varchar(240) not null comment '模版名称',
  template_usage_catg varchar(80) default null comment '模版使用分类，如：验证码、系统通知等',
  template_type varchar(80) default null comment '模版类型： SMS 或 标记html还是txt的邮件等',
  sms_template_code varchar(50) default null comment '阿里云-短信模版定义code(当类型为SMS时必填)',
  mail_subject varchar(255) default null COMMENT '邮件主题(当类型为MAIL时有值)',
  mail_content LONGTEXT default null COMMENT '内容(当类型为MAIL时有值)',
  enabled_flag varchar(1) default 'Y' comment '启用',
  start_date datetime default null comment '生效日期',
  end_date datetime default null comment '失效日期',
  description varchar(240) default null comment '描述',
  creation_date datetime,
  created_by varchar(50),
  last_update_date datetime,
  last_updated_by varchar(50),
  last_updated_login varchar(50),
  versions int(11)
) comment '消息模版表' ROW_FORMAT=DYNAMIC default charset utf8mb4;;

alter table ciip_comm.ciip_comm_message_template add primary key(id);

create index ciip_comm_message_template_u1 on ciip_comm.ciip_comm_message_template(message_template_code, message_type);
create index ciip_comm_message_template_u2 on ciip_comm.ciip_comm_message_template(template_name, message_type);