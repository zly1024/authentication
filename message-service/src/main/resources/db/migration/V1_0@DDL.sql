
CREATE DATABASE IF NOT EXISTS ciip_comm DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

create table ciip_comm.ciip_comm_file
(
  id varchar(50) not null comment '文件ID',
  file_name varchar(256) not null comment '文件名',
  file_path varchar(256) not null comment '文件路径',
  file_dir varchar(256) not null comment '文件路径',
  content_type varchar(50) default null comment '文件类型',
  file_size bigint comment '文件大小',
  creation_date datetime,
  created_by varchar(50),
  last_update_date datetime,
  last_updated_by varchar(50),
  last_updated_login varchar(50),
  versions int(11),
  attribute_catrgory varchar(30),
  attribute1 varchar(240),
  attribute2 varchar(240),
  attribute3 varchar(240),
  attribute4 varchar(240),
  attribute5 varchar(240),
  attribute6 varchar(240),
  attribute7 varchar(240),
  attribute8 varchar(240),
  attribute9 varchar(240),
  attribute10 varchar(240),
  attribute11 varchar(240),
  attribute12 varchar(240),
  attribute13 varchar(240),
  attribute14 varchar(240),
  attribute15 varchar(240)
) comment '文件表' ROW_FORMAT=DYNAMIC default charset utf8mb4;;

alter table ciip_comm.ciip_comm_file add primary key(id);

create index ciip_comm_file_n1 on ciip_comm.ciip_comm_file(file_dir, file_name);