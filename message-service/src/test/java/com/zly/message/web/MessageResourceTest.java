package com.zly.message.web;

import com.zly.message.comfig.MqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @author zly
 * @title: MessageResourceTest
 * @description: TODO
 * @date 2019/4/21 13:19
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageResourceTest {

    @Autowired
    RabbitMessagingTemplate rabbitMessagingTemplate;
    @Test
    public void test1(){
        rabbitMessagingTemplate.convertAndSend(MqConfig.MESSAGE_SEND_EXCHANGE,MqConfig.MESSAGE_SEND_KEY,"哈哈");
    }
}