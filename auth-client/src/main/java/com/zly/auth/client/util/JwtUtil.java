/*
 * #{copyright}#
 */
package com.zly.auth.client.util;

import com.alibaba.fastjson.JSON;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.jwt.crypto.sign.SignatureVerifier;
import org.springframework.security.jwt.crypto.sign.Signer;
import org.springframework.util.Assert;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

/**
 * JWt工具.
 * @author zly
 */
public class JwtUtil {

    private Signer signer;

    private SignatureVerifier verifier;

    public JwtUtil(KeyPair keyPair) {
        PrivateKey privateKey = keyPair.getPrivate();
        Assert.state(privateKey instanceof RSAPrivateKey, "KeyPair must be an RSA ");
        signer = new RsaSigner((RSAPrivateKey) privateKey);
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        verifier = new RsaVerifier(publicKey);
    }

    public String encode(Map<String, Object> content) {
        String jsonStr = JSON.toJSONString(content);
        return JwtHelper.encode(jsonStr, signer).getEncoded();
    }

    public Map<String, Object> decode(String token) {
        Jwt jwt = JwtHelper.decodeAndVerify(token, verifier);
        String content = jwt.getClaims();
        Map<String, Object> map = (Map<String, Object>) JSON.parse(content);
        return map;
    }

}
