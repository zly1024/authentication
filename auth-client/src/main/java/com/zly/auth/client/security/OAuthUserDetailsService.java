/*
 * #{copyright}#
 */
package com.zly.auth.client.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zly
 */
public class OAuthUserDetailsService implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(OAuthUserDetailsService.class);

    @Autowired
    private RestTemplate restTemplate;
    @Override
    public UserDetails loadUserByUsername(String userName) {
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("userName", userName);
        UserDetails userDetails = restTemplate.getForObject("http://localhost:10000/zly/user/user/oAuth/get", User
                        .class,
                urlVariables);
        return userDetails;
    }
}