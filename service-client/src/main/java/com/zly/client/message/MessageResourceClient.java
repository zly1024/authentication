/*
 * #{copyright}#
 */
package com.zly.client.message;

import com.alibaba.fastjson.JSON;
import com.zly.client.constants.ServiceNameConstant;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.dto.MessageDto;
import com.zly.common.dto.PushEvent;
import com.zly.common.web.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 消息client.
 *
 * @author zly
 */
@Component
public class MessageResourceClient extends AbstractResourceClient<ResponseEntity> {

    private static Logger logger = LoggerFactory.getLogger(MessageResourceClient.class);

   /* @Autowired
    private RabbitMessagingTemplate rabbitMessagingTemplate;*/


    @Override
    protected String getServiceName() {
        return ServiceNameConstant.MESSAGE_SERVICE;
    }

    /**
     * 系统错误消息发送专用，无需指定接收人信息（请勿用作其他业务逻辑）
     *
     * @param messageDto
     * @return
     */
    public ResponseEntity<String> sendSysErrorMsg(MessageDto messageDto) {
        return this.postForObject("/message/sysError/send", messageDto, ResponseEntity.class);
    }

    public ResponseEntity<String> sendMsg(MessageDto messageDto) {
        return this.postForObject("/message/send", messageDto, ResponseEntity.class);
    }

    public ResponseEntity<String> pushMsg(MessageDto messageDto) {
        return this.postForObject("/message/push", messageDto, ResponseEntity.class);
    }

   /* public ResponseEntity<String> pushMsgByHttp(PushEvent pushEvent) {
        logger.debug("~~~~~~ Entering pushMsgByHttp ~~~~~~");
        return this.pushMsg(convertToMessageDto(pushEvent));
    }

    public ResponseEntity<String> pushMsgByHttp(PushEvent pushEvent, boolean saveMessageFlag) {
        logger.debug("~~~~~~ Entering pushMsgByHttp ~~~~~~");
        return this.pushMsg(convertToMessageDto(pushEvent, saveMessageFlag));
    }

    public void pushMsgByMQ(PushEvent pushEvent) {
        logger.debug("~~~~~~ Entering pushMsgByMQ [exchange: \"amq.topic\" routingKey: \"topic.event.message.push\"] ~~~~~~");
        try {
            this.rabbitMessagingTemplate.convertAndSend("amq.topic", "topic.event.message.push", convertToMessageDto(pushEvent, false));
        } catch (Exception e) {
            //如果直接发MQ失败，走一次http
            this.pushMsgByHttp(pushEvent);
        }
        logger.debug("~~~~~~ Leaving pushMsgByMQ ~~~~~~");
    }

    private MessageDto convertToMessageDto(PushEvent pushEvent) {
        return convertToMessageDto(pushEvent, true);
    }

    private MessageDto convertToMessageDto(PushEvent pushEvent, boolean saveMessageFlag) {
        MessageDto messageDto = new MessageDto();
        messageDto.setSubject(pushEvent.getEvent());
        messageDto.setContent(JSON.toJSONString(pushEvent));
        messageDto.setSender("Push Engine[" + pushEvent.getSendFrom() + "]");
        List<String> receivers = pushEvent.getReceivers();
        List<CiipCommReceiver> receiverList = receivers.stream().map(receiver -> {
            CiipCommReceiver ciipCommReceiver = new CiipCommReceiver();
            ciipCommReceiver.setReceiverAddress(receiver);
            ciipCommReceiver.setSendType("TO");
            return ciipCommReceiver;
        }).collect(Collectors.toList());
        messageDto.setReceivers(receiverList);
        messageDto.setSaveMsgFlag(saveMessageFlag ? "Y" : "N");

        return messageDto;
    }*/

}
