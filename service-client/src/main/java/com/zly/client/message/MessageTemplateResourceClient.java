package com.zly.client.message;

import com.github.pagehelper.PageInfo;
import com.zly.client.constants.ServiceNameConstant;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.domain.comm.CiipCommMessageTemplate;
import com.zly.common.web.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zly
 */
@Component
public class MessageTemplateResourceClient extends AbstractResourceClient<ResponseEntity> {
    @Override
    protected String getServiceName() {
        return ServiceNameConstant.MESSAGE_SERVICE;
    }

    public static final String GET = "/messageTemplate/get";
    public static final String LIST = "/messageTemplate/list";
    public static final String QUERY = "/messageTemplate/query";
    public static final String SAVE = "/messageTemplate/save";
    public static final String SELECT_ONE = "/messageTemplate/selectOne";

    public ResponseEntity<CiipCommMessageTemplate> get(String id) {
        Map<String, String> urlParams = new HashMap<String,String>(1);
        urlParams.put("id", id);
        return successBean(
                (Map<String, Object>) getForObject(GET, ResponseEntity.class, urlParams).getData(),
                CiipCommMessageTemplate.class);
    }

    public ResponseEntity<List<CiipCommMessageTemplate>> list(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return successList(
                (List<Map<String, Object>>) getForObject(LIST, ResponseEntity.class, ciipCommMessageTemplate).getData(),
                CiipCommMessageTemplate.class);
    }

    public ResponseEntity<PageInfo<CiipCommMessageTemplate>> query(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return successPageInfo(
                (Map<String, Object>) getForObject(QUERY, ResponseEntity.class, ciipCommMessageTemplate).getData(),
                CiipCommMessageTemplate.class);
    }

    public ResponseEntity<CiipCommMessageTemplate> save(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return successBean(
                (Map<String, Object>) postForObject(SAVE, ciipCommMessageTemplate, ResponseEntity.class).getData(),
                CiipCommMessageTemplate.class);
    }

    public ResponseEntity<CiipCommMessageTemplate> selectOne(CiipCommMessageTemplate ciipCommMessageTemplate) {
        return successBean(
                (Map<String, Object>) getForObject(SELECT_ONE, ResponseEntity.class, ciipCommMessageTemplate).getData(),
                CiipCommMessageTemplate.class);
    }
}