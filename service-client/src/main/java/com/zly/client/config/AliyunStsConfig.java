package com.zly.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author : zly
 * @date   : 2017/10/11.
 * @description :
 */
//@Configuration
public class AliyunStsConfig {

    @Value("${aliyun.sts.apiVersion:}")
    private String apiVersion;
    @Value("${aliyun.sts.ram.accessKeyId:}")
    private String accessKeyId;
    @Value("${aliyun.sts.ram.accessKeySecret:}")
    private String accessKeySecret;
    @Value("${aliyun.sts.ram.roleArn:}")
    private String roleArn;
    @Value("${aliyun.sts.roleSessionName:}")
    private String roleSessionName;
    @Value("${aliyun.sts.durationSeconds:}")
    private String durationSeconds;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getRoleArn() {
        return roleArn;
    }

    public void setRoleArn(String roleArn) {
        this.roleArn = roleArn;
    }

    public String getRoleSessionName() {
        return roleSessionName;
    }

    public void setRoleSessionName(String roleSessionName) {
        this.roleSessionName = roleSessionName;
    }

    public String getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(String durationSeconds) {
        this.durationSeconds = durationSeconds;
    }
}
