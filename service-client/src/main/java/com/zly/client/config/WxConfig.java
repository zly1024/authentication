package com.zly.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author : zly
 * @date : 2017/9/26.
 * @description :
 */
//@Configuration
public class WxConfig {

    @Value("${wx.auth.tokenUrl:}")
    private String tokenUrl;
    @Value("${wx.auth.appId:}")
    private String appId;
    @Value("${wx.auth.secret:}")
    private String secret;
    @Value("${wx.auth.refreshTokenUrl:}")
    private String refreshTokenUrl;
    @Value("${wx.auth.userInfUrl:}")
    private String userInfUrl;
    @Value("${wx.auth.prePayUrl:}")
    private String prePayUrl;
    @Value("${wx.auth.mchId:}")
    private String mchId;
    @Value("${wx.auth.apiKey:}")
    private String key;
    @Value("${wx.auth.svcProviderMchId:}")
    private String svcProviderMchId;
    @Value("${wx.auth.svcProviderApiKey:}")
    private String svcProviderApiKey;
    @Value("${wx.auth.orderQuery:}")
    private String orderQuery;
    @Value("${wx.auth.notifyUrl:}")
    private String notifyUrl;
    @Value("${wx.auth.ticketUrl:}")
    private String ticketUrl;
    @Value("${wx.auth.esudianTokenUrl:}")
    private String esudianTokenUrl;
    /**
     * 决定支付使用哪个环境dev-sandbox / prod
     */
    @Value("${wx.auth.payEnv:}")
    private String payEnv;
    @Value("${wx.sandbox.paySignKeyUrl:}")
    private String paySignKeyUrl;
    @Value("${wx.sandbox.payTotalFee:}")
    /**
     * 沙箱支付金额
     */
    private String sandboxPayTotalFee;

    public String getEsudianTokenUrl() {
        return esudianTokenUrl;
    }

    public void setEsudianTokenUrl(String esudianTokenUrl) {
        this.esudianTokenUrl = esudianTokenUrl;
    }

    public String getTicketUrl() {
        return ticketUrl;
    }

    public void setTicketUrl(String ticketUrl) {
        this.ticketUrl = ticketUrl;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getRefreshTokenUrl() {
        return refreshTokenUrl;
    }

    public void setRefreshTokenUrl(String refreshTokenUrl) {
        this.refreshTokenUrl = refreshTokenUrl;
    }

    public String getUserInfUrl() {
        return userInfUrl;
    }

    public void setUserInfUrl(String userInfUrl) {
        this.userInfUrl = userInfUrl;
    }

    public String getPrePayUrl() {
        return prePayUrl;
    }

    public void setPrePayUrl(String prePayUrl) {
        this.prePayUrl = prePayUrl;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSvcProviderMchId() {
        return svcProviderMchId;
    }

    public void setSvcProviderMchId(String svcProviderMchId) {
        this.svcProviderMchId = svcProviderMchId;
    }

    public String getSvcProviderApiKey() {
        return svcProviderApiKey;
    }

    public void setSvcProviderApiKey(String svcProviderApiKey) {
        this.svcProviderApiKey = svcProviderApiKey;
    }

    public String getOrderQuery() {
        return orderQuery;
    }

    public void setOrderQuery(String orderQuery) {
        this.orderQuery = orderQuery;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getPayEnv() {
        return payEnv;
    }

    public void setPayEnv(String payEnv) {
        this.payEnv = payEnv;
    }

    public String getPaySignKeyUrl() {
        return paySignKeyUrl;
    }

    public void setPaySignKeyUrl(String paySignKeyUrl) {
        this.paySignKeyUrl = paySignKeyUrl;
    }

    public String getSandboxPayTotalFee() {
        return sandboxPayTotalFee;
    }

    public void setSandboxPayTotalFee(String sandboxPayTotalFee) {
        this.sandboxPayTotalFee = sandboxPayTotalFee;
    }
}
