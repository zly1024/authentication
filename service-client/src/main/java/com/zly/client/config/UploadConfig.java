package com.zly.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author zly
 * @date 2017/10/9.
 */
@Configuration
@Deprecated
public class UploadConfig {

    @Value("${spring.storage.serviceRequestUrl:}")
    private String serviceRequestUrl;

    public String getServiceRequestUrl() {
        return serviceRequestUrl;
    }

    public void setServiceRequestUrl(String serviceRequestUrl) {
        this.serviceRequestUrl = serviceRequestUrl;
    }
}
