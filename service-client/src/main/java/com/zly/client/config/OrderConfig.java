package com.zly.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : zly
 * @date   : 2017/7/29.
 * @description :
 */
//@Configuration
public class OrderConfig {

    @Value("#{'${esudian.order.status.going}'.split(',')}")
    private List<String> goingOrderStatus = new ArrayList<>();

    @Value("#{'${esudian.item.categoryType.specialTypes}'.split(',')}")
    private List<String> specialCategType = new ArrayList<>();

    public List<String> getGoingOrderStatus() {
        return goingOrderStatus;
    }

    public void setGoingOrderStatus(List<String> goingOrderStatus) {
        this.goingOrderStatus = goingOrderStatus;
    }

    public List<String> getSpecialCategType() {
        return specialCategType;
    }

    public void setSpecialCategType(List<String> specialCategType) {
        this.specialCategType = specialCategType;
    }
}
