package com.zly.client.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : zly
 * @date   : 2017/9/26.
 * @description :
 */
//@Configuration
public class AlipayConfig {

    @Value("${alipay.appId:}")
    private String appId;
    @Value("${alipay.baseUrl:}")
    private String baseUrl;
    @Value("${alipay.pay.notifyUrl:}")
    private String notifyUrl;
    @Value("${alipay.privateKey:}")
    private String privateKey;
    @Value("${alipay.alipayPublicKey:}")
    private String alipayPublicKey;
    @Value("${alipay.payEnv:}")
    private String payEnv;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getAlipayPublicKey() {
        return alipayPublicKey;
    }

    public void setAlipayPublicKey(String alipayPublicKey) {
        this.alipayPublicKey = alipayPublicKey;
    }

    public String getPayEnv() {
        return payEnv;
    }

    public void setPayEnv(String payEnv) {
        this.payEnv = payEnv;
    }

    @Bean(name = "alipayClient")
    public AlipayClient alipayClient() {
        return new DefaultAlipayClient(baseUrl, appId, privateKey, "json", "utf-8", alipayPublicKey, "RSA2");
    }
}
