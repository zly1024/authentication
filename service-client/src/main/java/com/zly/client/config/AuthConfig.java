package com.zly.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author : zly
 * @date   : 2017/9/26.
 * @description :
 */
@Configuration
public class AuthConfig {

    @Value("${security.client.access-token-uri:}")
    private String tokenUrl;
    @Value("${security.client.client-id:}")
    private String clientId;
    @Value("${security.client.client-secret:}")
    private String secret;

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}