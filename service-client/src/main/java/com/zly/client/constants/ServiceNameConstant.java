package com.zly.client.constants;

/**
 * @author : zly
 * @date   : 2017/7/21.
 * @description :
 */
public class ServiceNameConstant {
    public static final String USER_SERVICE = "http://localhost:10000/zly/user";
    public static final String MESSAGE_SERVICE = "http://localhost:7002";

}
