package com.zly.client.constants;

import com.zly.common.exception.MultiLangException;

/**
 * @author zly
 * @title: UserErrorsEnum
 * @description:
 * @date 2019/4/20 18:46
 */
public enum  UserErrorsEnum {
    /**
     * 错误的部门所属来源
     */
    ERROR_DEPT_SOURCE_TYPE("s10001", "msg.error.dept_source_ref_type_error"),
    /**
     * 部门名称为空
     */
    ERROR_DEPT_NAME_IS_NULL("s1002", "msg.error.dept_name_is_null"),
    /**
     * 分类名称为空
     */
    ERROR_CATEGORY_NAME_IS_NULL("s1005", "msg.error.category_name_is_null"),
    /**
     * 分类默认烹饪部门为空
     */
    ERROR_CATEGORY_DEFAULT_COOKING_DEPT_ID_IS_NULL("s1006", "msg.error.category_default_cooking_dept_id_is_null"),
    /**
     * 菜品名称为空
     */
    ERROR_MENU_ITEM_NAME_IS_NULL("s1009", "msg.error.menu_item_name_is_null"),
    /**
     * 菜品价格为空
     */
    ERROR_MENU_ITEM_PRICE_IS_NULL("s1012", "msg.error.menu_item_price_is_null"),
    /**
     * 商家名称为控
     */
    ERROR_COMPANY_NAME_IS_NULL("s1014", "msg.error.company_name_is_null"),
    /**
     * 营业执照为空
     */
    ERROR_BUSINESS_LICENSE_IS_NULL("s1015", "msg.error.business_license_is_null"),
    /**
     * 组织机构代码证为空
     */
    ERROR_ORG_CODE_CERTIFICATE_IS_NULL("s1016", "msg.error.org_code_certificate_is_null"),
    /**
     * 税号为空
     */
    ERROR_BUSINESS_TAX_NUMBER_IS_NULL("s1017", "msg.error.business_tax_number_is_null"),
    /**
     * 主要联系人为空
     */
    ERROR_PRIMARY_CONTACTER_IS_NULL("s1020", "msg.error.primary_contacter_is_null"),
    /**
     * 主要联系人联系方式为空
     */
    ERROR_PRIMARY_CONTACT_NUM_IS_NULL("s1021", "msg.error.primary_contact_num_is_null"),
    /**
     * 注册地址为空
     */
    ERROR_REGISTERED_ADDRESS_IS_NULL("s1022", "msg.error.registered_address_is_null"),
    /**
     * 经营范围为空
     */
    ERROR_BUSINESS_SCOPE_IS_NULL("s1024", "msg.error.business_scope_is_null"),
    /**
     * 所属来源错误
     */
    ERROR_SOURCE_REF_CODE_IS_NULL("s1025", "msg.error.source_ref_code_is_null"),
    /**
     * 员工类型为空
     */
    ERROR_EMPLOYEE_TYPE_CODE_IS_NULL("s1027", "msg.error.employee_type_code_is_null"),
    /**
     * 员工姓名为空
     */
    ERROR_EMPLOYEE_NAME_IS_NULL("s1029", "msg.error.employee_name_is_null"),
    /**
     * 员工职位为空
     */
    ERROR_POSITION_IS_NULL("s1030", "msg.error.position_is_null"),
    /**
     * 员工性别为空
     */
    ERROR_GENDER_IS_NULL("s1032", "msg.error.gender_is_null"),
    /**
     * 员工电话为空
     */
    ERROR_TELEPHONE_NUM_IS_NULL("s1034", "msg.error.telephone_num_is_null"),
    /**
     * 邮箱为空
     */
    ERROR_EMAIL_IS_NULL("s1035", "msg.error.email_is_null"),
    /**
     * 排序序号为空
     */
    ERROR_CATEGORY_CATEGORY_SORT_IS_NULL("s1036", "msg.error.category_sort_is_null"),
    /**
     * 错误的手机号码
     */
    ERROR_FORMAT_PHONE_NUMBER("s1038", "msg.error.phone_number_is_error"),
    /**
     * 验证码已发送
     */
    VERIFY_CODE_HAS_SEND("s1039", "msg.error.verify_code_has_send"),
    /**
     * 餐桌编号为空
     */
    ERROR_TABLE_NUM_IS_NULL("s1040", "msg.error.table_num_is_null"),
    /**
     * 餐桌类型为空
     */
    ERROR_TABLE_TYPE_CODE_IS_NULL("s1041", "msg.error.table_type_code_is_null"),
    /**
     * 餐桌容纳人数为空
     */
    ERROR_HOLD_PERSONS_IS_NULL("s1042", "msg.error.hold_persons_is_null"),
    /**
     * 餐桌状态为空
     */
    ERROR_TABLE_STATUS_CODE_IS_NULL("s1048", "msg.error.table_status_code_is_null"),
    /**
     * 餐桌编号重复
     */
    ERROR_TABLE_NUM_IS_REPEAT("s1049", "msg.error.table_num_is_repeat"),
    /**
     * 没有找到用户
     */
    ERROR_HAS_NO_FOUND_USER("s1051", "msg.error.has_no_found_user"),
    /**
     * 错误的购物车版本
     */
    ERROR_CART_VERSION_NUMBER("s1052", "msg.error.cart_version_number"),

    /**
     * 所属商家为空
     */
    ERROR_COMPANY_ID_IS_NULL("s10053", "msg.error.company_id_is_null"),
    /**
     * 所属门店为空
     */
    ERROR_STORE_ID_IS_NULL("s10054", "msg.error.store_id_is_null"),
    /**
     * 卡分类为空
     */
    ERROR_CARD_CATEGORY_CODE_IS_NULL("s10055", "msg.error.card_category_code_is_null"),
    /**
     * 卡类型名称为空
     */
    ERROR_CARD_TYPE_IS_NULL("s10056", "msg.error.card_type_is_null"),
    /**
     * 卡编号为空
     */
    ERROR_CARD_NO_IS_NULL("s10064", "msg.error.card_no_is_null"),
    /**
     * 卡编号重复
     */
    ERROR_CARD_NO_IS_REPEAT("s10068", "msg.error.card_no_is_repeat"),

    /**
     * 手机号为空
     */
    ERROR_MOBILE_PHONE_NUM_IS_NULL("s10070", "msg.error.mobile_phone_num_is_null"),
    /**
     * 商家识别号为空
     */
    ERROR_ID_NUMBER_IS_NULL("s10074", "msg.error.id_number_is_null"),

    /**
     * 卡ID为空
     */
    ERROR_CARD_ID_IS_NULL("s10078", "msg.error.card_id_is_null"),
    /**
     * 是否生效标识为空
     */
    ERROR_IS_ACTIVE_FLAG_IS_NULL("s10080", "msg.error.is_active_flag_is_null"),
    /**
     * 没有找到员工
     */
    ERROR_NOT_FOUND_EMP("S10082", "msg.error.not_found_emp"),

    /**
     * 菜品项为空,指组合菜品的子项id为空
     */
    ERROR_COMBO_ITEM_ID_IS_NULL("s10083", "msg.error.combo_item_id_is_null"),
    /**
     * 所属菜品为空,组合菜品的头id为空
     */
    ERROR_ITEM_ID_IS_NULL("s10084", "msg.error.item_id_is_null"),
    /**
     * 数量为空
     */
    ERROR_QUANTITY_IS_NULL("s10087", "msg.error.quantity_is_null"),

    /**
     * 找不到想要回复的评论
     */
    ERROR_STORE_REMARK_ID_IS_NULL("s10091", "msg.error.store_remark_id_is_null"),
    /**
     * 活动使用规则
     */
    ERROR_ORG_PROMOTION_USAGE_RULE_IS_NULL("s10094", "msg.error.org_promotion_usage_rules_is_null"),
    /**
     * 活动类型不匹配
     */
    ERROR_PROMOTION_TYPE_ID_IS_ATYPISM("s10095", "msg.error.promotion_type_id_is_atypism"),
    /**
     * 所有页都获取锁失败
     */
    ERROR_USER_RECEIVE_COUPON_GET_LOCK_IS_FALSE("s10096", "msg.error.user_receive_coupon_get_lock_is_false"),
    /**
     * 用户没有当前类型优惠券
     */
    ERROR_USER_HAS_NO_TYPE_COUPON("s10097", "msg.error.user_has_no_type_coupon"),
    /**
     * 订单为空
     */
    ERROR_ORDER_HEADER_ID_IS_NULL("s10098", "msg.error.order_head_id_is_null"),
    /**
     * 退款金额为空
     */
    ERROR_APPLY_REFUND_AMT_IS_NULL("s10099", "msg.error.apply_refund_amt_is_null"),
    /**
     * 订单错误
     */
    ERROR_ORDER_HEADER_ID_IS_ERROR("s10100", "msg.error.order_header_id_is_error"),
    /**
     * 菜品错误
     */
    ERROR_ITEM_ID_IS_ERROR("s10101", "msg.error.item_id_is_error"),
    /**
     * 保存数量有问题
     */
    ERROR_NUMBER_IS_ERROR("s10102", "msg.error.number_is_error"),
    /**
     * 餐桌编号重复
     */
    ERROR_TABLE_NUMBER_IS_REPEAT("s10103", "msg.error.table_number_is_repeat"),
    /**
     * 分配状态为空
     */
    ERROR_ASSIGNED_STATUS_CODE_IS_NULL("s10106", "msg.error.assigned_status_code_is_null"),
    /**
     * 卡号为空
     */
    ERROR_CARD_BASE_INFO_IS_NOT_EXIST("s10107", "msg.error.card_base_info_is_not_exist"),
    /**
     * 菜品名称重复
     */
    ERROR_MENU_ITEM_NAME_REPEAT("s10108", "msg.error.menu_item_name_repeat"),
    /**
     * 所指定的分类为空
     */
    ERROR_CATEGORY_ID_IS_NULL("s10109", "msg.error.category_id_is_null"),
    /**
     * 所指定的分类下有菜品
     */
    ERROR_CATEGORY_ID_EXISTS_ITEMS("s10110", "msg.error.category_id_exists_items"),
    /**
     * 卡类型无法被删除
     */
    ERROR_CARD_TYPE_CAN_NOT_DELETE("s10112", "msg.error.card_type_can_not_delete"),
    /**
     * 会员卡无法被删除
     */
    ERROR_CARD_CAN_NOT_DELETE("s10113", "msg.error.card_can_not_delete"),
    /**
     * 所指定员工为空
     */
    ERROR_EMPLOYEE_ID_IS_NULL("s10114", "msg.error.employee_is_null"),
    /**
     * 用户名为空
     */
    ERROR_USER_NAME_IS_NULL("s10115", "msg.error.user_name_is_null"),
    /**
     * 用户名重复
     */
    ERROR_USER_NAME_IS_REPEAT("s10116", "msg.erro.user_name_is_repeat"),
    /**
     * 门店编号为空
     */
    ERROR_STORE_NUMBER_IS_NULL("s10117", "msg.error.org.store_number_is_null"),
    /**
     * 门店名称为空
     */
    ERROR_STORE_NAME_IS_NULL("s10118", "msg.error.org.store_name_is_null"),
    /**
     * 门店主要联系人为空
     */
    ERROR_STORE_PRIMARY_CONTACTER_IS_NULL("s10119", "msg.error.org.store_primary_contacter_is_null"),
    /**
     * 门店主要联系人电话为空
     */
    ERROR_STORE_PRIMARY_CONTACT_NUM_IS_NULL("s10120", "msg.error.org.store_primary_contact_num_is_null"),
    /**
     * 门店地址为空
     */
    ERROR_STORE_ADDRESS_IS_NULL("s10121", "msg.error.org.store_address_is_null"),
    /**
     * 配置值为空
     */
    ERROR_MDM_PROFILE_OPTION_VALUE_IS_NULL("s10122", "msg.error.mdm.profile_option_value_is_null"),
    /**
     * 部门已被分类表-默认烹饪部门引用
     */
    ERROR_DEPT_REF_BY_CATEGORY("s10123", "msg.error.org.dept_ref_by_category"),
    /**
     * 该部门是父部门
     */
    ERROR_DEPT_REF_BY_PARENT_DEPT("s10124", "msg.error.org.dept_ref_by_parent_dept"),
    /**
     * 部门被菜品-烹饪部门引用
     */
    ERROR_DEPT_REF_BY_ITEM("s10125", "msg.error.org.dept_ref_by_item"),
    /**
     * 部门下有员工
     */
    ERROR_DEPT_REF_BY_EMPLOYEE("s10126", "msg.error.org.dept_ref_by_employee"),
    /**
     * 用户角色不存在
     */
    ERROR_USER_ROLE_IS_NOT_EXISTS("s10127", "msg.error.usr.user_role_is_not_exists"),
    /**
     * 密码为空
     */
    ERROR_PASSWORD_IS_NULL("s10128", "msg.error.usr.user_password_is_null"),
    /**
     * 登陆令牌为空
     */
    ERROR_USER_LOGIN_CODE_IS_NULL("s10129", "msg.error.usr.user_login_code_is_null"),
    /**
     * 刷新令牌时,传入空的刷新令牌
     */
    ERROR_USER_REFRESH_TOKEN_IS_NULL("s10130", "msg.error.usr.user_refresh_token_is_null"),
    /**
     * 图片为空
     */
    ERROR_PICTURE_IS_NULL("s10132", "msg.error.picture_is_null"),
    /**
     * 身份证正面照为空
     */
    ERROR_PICTURE_ID1_IS_NULL("s10133", "msg.error.picture_id1_is_null"),
    /**
     * 身份证背面照片为空
     */
    ERROR_PICTURE_ID2_IS_NULL("s10134", "msg.error.picture_id2_is_null"),
    /**
     * 营业执照为空
     */
    ERROR_PICTURE_BL_IS_NULL("s10135", "msg.error.picture_bl_is_null"),
    /**
     * 税务登记证为空
     */
    ERROR_PICTURE_TRC_IS_NULL("s10136", "msg.error.picture_trc_is_null"),
    /**
     * 组织机构代码证为空
     */
    ERROR_PICTURE_OCC_IS_NULL("s10137", "msg.error.picture_occ_is_null"),
    /**
     * 非法操作
     */
    ERROR_ILLEGAL_OPERATION("s10138", "msg.error.illegal_operation"),
    /**
     * 该营业执照已经被注册成为商家
     */
    ERROR_BL_COM_EXISTS("s10139", "msg.error.bl_company_exists"),
    /**
     * 该营业执照已经提交注册申请
     */
    ERROR_BL_COM_R_EXISTS("s10140", "msg.error.bl_com_r_exists"),
    /**
     * 验证码错误
     */
    ERROR_VERIFY_CODE_IS_ERROR("s10141", "msg.error.verify_code_is_error"),
    /**
     * 食品卫生许可证为空
     */
    ERROR_PICTURE_FHL_IS_NULL("s10144", "msg.error.picture_fhl_is_null"),
    /**
     * 法人代表为空
     */
    ERROR_LEGAL_PERSON_NAME_IS_NULL("s10145", "msg.error.legal_person_name_is_null"),
    /**
     * 银行开户城市为空
     */
    ERROR_BANK_CITY_IS_NULL("s10146", "msg.error.bank_city_is_null"),
    /**
     * 银行开户行为空
     */
    ERROR_BANK_NAME_IS_NULL("s10147", "msg.error.bank_name_is_null"),
    /**
     * 银行开户分行为空
     */
    ERROR_BANK_BRANCH_NAME_IS_NULL("s10148", "msg.error.bank_branch_name_is_null"),
    /**
     * 银行账号名称为空
     */
    ERROR_BANK_ACCOUNT_NAME_IS_NULL("s10149", "msg.error.bank_account_name_is_null"),
    /**
     * 银行账号为空
     */
    ERROR_BANK_ACCOUNT_NUM_IS_NULL("s10150", "msg.error.bank_account_num_is_null"),
    /**
     * 餐饮服务许可证为空
     */
    ERROR_PICTURE_FSI_IS_NULL("s10151", "msg.error.picture_fsi_is_null"),
    /**
     * 失效日期比开始生效日期早
     */
    ERROR_END_DATE_CANT_BEFORE_START_DATE("s10152", "msg.error.end_date_cant_before_start_date"),
    /**
     * 门店id 与 餐桌id不匹配
     */
    TABLE_NOT_MAP_STORE("s10154", "msg.error.table_not_map_store"),
    /**
     * 餐桌正在被使用
     */
    ERROR_TABLE_IS_USING_NOW("s10155", "msg.error.table_is_using_now"),
    /**
     * 服务已被完成
     */
    ERROR_SERVICE_BELL_HAS_COMPLETED("s10156", "msg.error.service_bell_has_completed"),

    /**
     * 付款方式为空
     */
    ERROR_PAY_METHOD_IS_NULL("s10157", "msg.error.pay_method_is_null"),

    /**
     * 刷新Token时错误
     */
    ERROR_USER_REFRESH_TOKEN_ERR("s10158", "msg.error.usr.user_refresh_token_error");


    private String errorCode;
    private String messageCode;

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public MultiLangException exception() {
        return new MultiLangException(errorCode, messageCode);
    }

    UserErrorsEnum(String errorCode, String messageCode) {
        this.errorCode = errorCode;
        this.messageCode = messageCode;
    }
}
