package com.zly.client.util;

import com.alibaba.fastjson.JSON;
import com.zly.client.config.AuthConfig;
import com.zly.common.dto.AuthRespDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zly
 */
@Component
public class AuthHelper extends RestTemplateHelper {
    @Autowired
    private AuthConfig authConfig;

    public AuthRespDto getAccessToken(String userName, String password) {
        Map<String, String> params = new HashMap<>(5);
        params.put("grant_type", "password");
        params.put("username", userName);
        params.put("password", password);
        params.put("client_id", authConfig.getClientId());
        params.put("client_secret", authConfig.getSecret());
        AuthRespDto authRespDto = new AuthRespDto(JSON.parseObject(restTemplate.postForObject(processUrl(authConfig.getTokenUrl(), params), new HttpHeaders(), String.class)));
        return authRespDto;
    }

    public AuthRespDto getAccessTokenByCode(String code, String redirectUrl) {
        Map<String, String> params = new HashMap<>(5);
        params.put("grant_type", "authorization_code");
        params.put("code", code);
        params.put("client_id", authConfig.getClientId());
        params.put("client_secret", authConfig.getSecret());
        params.put("redirect_uri", redirectUrl);
        AuthRespDto authRespDto = new AuthRespDto(JSON.parseObject(restTemplate.postForObject(getUrI(authConfig.getTokenUrl(), params), null, String.class)));
        return authRespDto;
    }

    private static URI getUrI(String url, Map<String, String> map) {
        StringBuffer sb = new StringBuffer(url + "?");
        map.forEach((s, s2) ->
                sb.append(s + "={" + s + "}&")
        );
        return UriComponentsBuilder.fromUriString(
                sb.toString().substring(0, sb.length() - 1)).build()
                .expand(map)
                .encode().toUri();
    }

    public AuthRespDto refreshAccessToken(String refreshToken) {
        Map<String, String> params = new HashMap<>(2);
        params.put("grant_type", "refresh_token");
        params.put("client_id", authConfig.getClientId());
        params.put("client_secret", authConfig.getSecret());
        params.put("refresh_token", refreshToken);
        AuthRespDto authRespDto = new AuthRespDto(JSON.parseObject(restTemplate.postForObject(processUrl(authConfig.getTokenUrl(), params), new HttpHeaders(), String.class)));
        return authRespDto;
    }

    public boolean revokeAccessToken(String authorization) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", authorization);
        HttpEntity<?> request = new HttpEntity<Object>(headers);
        restTemplate.exchange(authConfig.getTokenUrl() + "/", HttpMethod.DELETE, request, String.class);
        return true;
    }

}