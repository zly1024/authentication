/*
 * #{copyright}#
 */
package com.zly.client.util;

import com.github.pagehelper.PageInfo;
import com.zly.common.exception.MultiLangException;
import com.zly.common.util.BeanUtil;
import com.zly.common.util.StringBuilderUtil;
import com.zly.common.web.ResponseEntity;
import com.zly.common.web.ResponseState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Client抽象类.
 *
 * @author zly
 */
public abstract class AbstractResourceClient<T extends ResponseEntity> {

    @Autowired
    protected RestTemplate restTemplate;

    /**
     * 获取服务名称.
     * @return 获取服务名称.
     */
    protected abstract String getServiceName();

    private String processUrl(String url) {
        return StringBuilderUtil.append(getServiceName(), url);
    }

    protected T getForObject(String url, Class<T> responseType, Object... urlVariables) throws RestClientException {
        url = processUrl(url);
        T result = restTemplate.getForObject(url, responseType, urlVariables);
        check(result);
        return result;
    }

    protected T getForObject(String url, Class<T> responseType, Map<String, ?> urlVariables) throws RestClientException {
        url = processUrl(url);
        T result = restTemplate.getForObject(url, responseType, urlVariables);
        check(result);
        return result;
    }

    protected T getForObject(String url, ParameterizedTypeReference typeReference, Map<String, ?> urlVariables) throws RestClientException {
        url = processUrl(url);
        T result = (T)restTemplate.exchange(url, HttpMethod.GET,null,typeReference, urlVariables).getBody();
        check(result);
        return result;
    }

    protected T postForObject(String url, Object request, Class<T> responseType, Object... uriVariables) throws RestClientException {
        url = processUrl(url);
        T result = restTemplate.postForObject(url, request, responseType, uriVariables);
        check(result);
        return result;
    }

    protected T postForObject(String url, Object request, ParameterizedTypeReference typeReference, Object... uriVariables) throws RestClientException {
        url = processUrl(url);
        T result = (T) restTemplate.exchange(url, HttpMethod.POST, new HttpEntity(request), typeReference, uriVariables).getBody();
        check(result);
        return result;
    }

    protected T postForObject(String url, Object request, Class<T> responseType, Map<String, ?> uriVariables) throws RestClientException {
        url = processUrl(url);
        T result = restTemplate.postForObject(url, request, responseType, uriVariables);
        check(result);
        return result;
    }

    protected T postForObject(String url, Object request, ParameterizedTypeReference typeReference, Map<String, ?> uriVariables) throws RestClientException {
        url = processUrl(url);
        T result = (T) restTemplate.exchange(url, HttpMethod.POST, new HttpEntity(request), typeReference, uriVariables).getBody();
        check(result);
        return result;
    }

    /**
     * 检查服务返回的状态
     * @param result
     */
    private void check(T result) {
        if (!ResponseState.SUCCESS.getValue().equals(result.getCode())) {
            throw new MultiLangException(result.getCode(), result.getMsg());
        }
    }

    public ResponseEntity emptyResponse() {
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setCode(ResponseState.ERROR.getValue());
        responseEntity.setMsg("failed to call service : " + getServiceName());
        return responseEntity;
    }

    protected static <T> ResponseEntity<PageInfo<T>> successPageInfo(Map<String, Object> data, Class<T> tClass) {
        return success(BeanUtil.pageMap2pageBean(data, tClass));
    }

    protected static <T> ResponseEntity<List<T>> successList(List<Map<String, Object>> data, Class<T> tClass) {
        return success(BeanUtil.listMap2listBean(data, tClass));
    }

    protected static <T> ResponseEntity<T> successBean(Map<String, Object> data, Class<T> tClass) {
        return success(BeanUtil.map2Bean(data, tClass));
    }

    protected static <T> ResponseEntity<T> success(T data) {
        ResponseEntity<T> responseEntity = new ResponseEntity();
        responseEntity.setData(data);
        responseEntity.setCode(ResponseState.SUCCESS.getValue());
        return responseEntity;
    }

    protected static <T> String processGetUrl(String url, T data) {
        StringBuffer sb = new StringBuffer();
        Map<String, Object> map = BeanUtil.bean2UrlMap(data);
        Iterator<String> iter = map.keySet().iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            sb.append("&" + key + "=" + map.get(key));
        }
        return url + "?" + sb.substring(1);
    }

}
