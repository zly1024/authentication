package com.zly.client.usr;


import com.github.pagehelper.PageInfo;
import com.zly.client.constants.ServiceNameConstant;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.domain.usr.CiipUsrSysRoles;
import com.zly.common.web.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zly
 */
@Component
public class RoleResourceClient extends AbstractResourceClient<ResponseEntity> {


    public static final String GET = "/role/get";
    public static final String LIST = "/role/list";
    public static final String QUERY = "/role/query";
    public static final String SAVE = "/role/save";
    public static final String SAVE_SUMMARY = "/role/saveSummary";
    public static final String SELECT_ONE = "/role/selectOne";

    @Override
    protected String getServiceName() {
        return ServiceNameConstant.USER_SERVICE;
    }


    public ResponseEntity<CiipUsrSysRoles> get(String id) {
        Map<String, String> urlParams = new HashMap<>(1);
        urlParams.put("id", id);
        return successBean(
                (Map<String, Object>) getForObject(GET, ResponseEntity.class, urlParams).getData(),
                CiipUsrSysRoles.class);
    }


    public ResponseEntity<PageInfo<CiipUsrSysRoles>> query(CiipUsrSysRoles ciipUsrSysRoles) {
        return successPageInfo(
                (Map<String, Object>) getForObject(QUERY, ResponseEntity.class, ciipUsrSysRoles).getData(),
                CiipUsrSysRoles.class);
    }

    public ResponseEntity<CiipUsrSysRoles> save(CiipUsrSysRoles ciipUsrSysRoles) {
        return successBean(
                (Map<String, Object>) postForObject(SAVE, ciipUsrSysRoles, ResponseEntity.class).getData(),
                CiipUsrSysRoles.class);
    }

    public ResponseEntity<CiipUsrSysRoles> selectOne(CiipUsrSysRoles ciipUsrSysRoles) {
        return successBean(
                (Map<String, Object>) getForObject(SELECT_ONE, ResponseEntity.class, ciipUsrSysRoles).getData(),
                CiipUsrSysRoles.class);
    }

    public ResponseEntity<List<CiipUsrSysRoles>> list(CiipUsrSysRoles ciipUsrSysRoles) {
        return successList((List<Map<String, Object>>) getForObject(LIST, ResponseEntity.class, ciipUsrSysRoles).getData(), CiipUsrSysRoles.class);
    }


/*    *//**
     * 保存角色以及授权
     *
     * @param usrSysRoles
     * @return
     *//*
    public ResponseEntity<UsrSysRoles> saveSummary(UsrSysRoles usrSysRoles) {
        return successBean(
                (Map<String, Object>) postForObject(SAVE_SUMMARY, usrSysRoles, ResponseEntity.class).getData(),
                UsrSysRoles.class);
    }*/
}
