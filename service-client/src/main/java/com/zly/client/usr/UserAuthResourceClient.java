package com.zly.client.usr;

import com.github.pagehelper.PageInfo;
import com.zly.client.constants.ServiceNameConstant;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.domain.usr.CiipUsrSysUserAuth;
import com.zly.common.dto.BooleanObject;
import com.zly.common.web.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zly
 */
@Component
public class UserAuthResourceClient extends AbstractResourceClient<ResponseEntity> {
    @Override
    protected String getServiceName() {
        return ServiceNameConstant.USER_SERVICE;
    }

    public static final String GET = "/userAuth/get";
    public static final String LIST = "/userAuth/list";
    public static final String QUERY = "/userAuth/query";
    public static final String SAVE = "/userAuth/save";
    public static final String SELECT_ONE = "/userAuth/selectOne";
    public static final String DELETE = "/userAuth/delete";


    public ResponseEntity<CiipUsrSysUserAuth> get(String id) {
        Map<String, String> urlParams = new HashMap<>(1);
        urlParams.put("id", id);
        return successBean(
                (Map<String, Object>) getForObject(GET, ResponseEntity.class, urlParams).getData(),
                CiipUsrSysUserAuth.class);
    }

    public ResponseEntity<List<CiipUsrSysUserAuth>> list(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return successList(
                (List<Map<String, Object>>) getForObject(LIST, ResponseEntity.class, ciipUsrSysUserAuth).getData(),
                CiipUsrSysUserAuth.class);
    }

    public ResponseEntity<PageInfo<CiipUsrSysUserAuth>> query(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return successPageInfo(
                (Map<String, Object>) getForObject(QUERY, ResponseEntity.class, ciipUsrSysUserAuth).getData(),
                CiipUsrSysUserAuth.class);
    }

    public ResponseEntity<CiipUsrSysUserAuth> save(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return successBean(
                (Map<String, Object>) postForObject(SAVE, ciipUsrSysUserAuth, ResponseEntity.class).getData(),
                CiipUsrSysUserAuth.class);
    }

    public ResponseEntity<CiipUsrSysUserAuth> selectOne(CiipUsrSysUserAuth ciipUsrSysUserAuth) {
        return successBean(
                (Map<String, Object>) getForObject(SELECT_ONE, ResponseEntity.class, ciipUsrSysUserAuth).getData(),
                CiipUsrSysUserAuth.class);
    }

    public ResponseEntity<BooleanObject> delete(String id) {
        Map<String, String> map = new HashMap<>(1);
        map.put("id", id);
        return successBean((Map<String, Object>)
                getForObject(DELETE, ResponseEntity.class, map).getData(), BooleanObject.class);
    }
}