package com.zly.client.usr;

import com.github.pagehelper.PageInfo;
import com.zly.client.constants.CacheConstant;
import com.zly.client.constants.ServiceNameConstant;
import com.zly.client.util.AbstractResourceClient;
import com.zly.common.domain.usr.CiipUsrUsers;
import com.zly.common.dto.usr.UserRoleCode;
import com.zly.common.dto.usr.UsrUsers;
import com.zly.common.util.BeanUtil;
import com.zly.common.web.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : zly
 * @date : 2017/3/15.
 * @description :
 */
@Component
public class UserResourceClient extends AbstractResourceClient<ResponseEntity> {
    @Autowired
    private CacheManager cacheManager;

    @Override
    protected String getServiceName() {
        return ServiceNameConstant.USER_SERVICE;
    }

    public static final String DELETE = "/user/disable";


    /**
     * 查询单个用户
     *
     * @param id 主键
     * @return ResponseEntity&lt;CiipUsrUsers&gt;
     */
    public ResponseEntity<CiipUsrUsers> get(String id) {
        Cache cache = cacheManager.getCache(CacheConstant.USER_CACHE_NAME);
        CiipUsrUsers ciipUsrUsers = (CiipUsrUsers) cache.get(id).get();
        if (ciipUsrUsers == null) {
            Map<String, String> urlParams = new HashMap<>(1);
            urlParams.put("id", id);
            ciipUsrUsers = BeanUtil.map2Bean((Map<String, Object>)
                    this.getForObject("/user/get", ResponseEntity.class, urlParams).getData(), CiipUsrUsers.class);
            cache.put(id, ciipUsrUsers);
        }
        return success(ciipUsrUsers);
    }

    /**
     * 查询一个用户
     *
     * @param usrUsers
     * @return
     */
    public ResponseEntity<CiipUsrUsers> selectOne(CiipUsrUsers usrUsers) {
        return successBean((Map<String, Object>) this.getForObject("/user/selectOne", ResponseEntity.class, usrUsers).getData(), CiipUsrUsers.class);
    }

    /**
     * 查询多个用户
     * 需要传递分页信息到查询条件对象中
     *
     * @param ciipUsrUsers 查询条件
     * @return ResponseEntity&lt;PageInfo&lt;CiipUsrUsers&gt;&gt;
     */
    public ResponseEntity<PageInfo<CiipUsrUsers>> query(CiipUsrUsers ciipUsrUsers) {
        return successPageInfo((Map<String, Object>) this.getForObject("/user/query", ResponseEntity.class, ciipUsrUsers).getData(), CiipUsrUsers.class);
    }

    /**
     * 保存用户
     *
     * @param ciipUsrUsers ciipUsrUsers
     * @return ResponseEntity&lt;CiipUsrUsers&gt;
     */

    public ResponseEntity<CiipUsrUsers> insert(CiipUsrUsers ciipUsrUsers) {
        ResponseEntity<Map<String, Object>> mapResponseEntity = this.postForObject("/user/insert", ciipUsrUsers, ResponseEntity.class);
        if (!StringUtils.isEmpty(ciipUsrUsers.getId())) {
            Cache cache = cacheManager.getCache(CacheConstant.USER_CACHE_NAME);
            cache.evict(ciipUsrUsers.getId());
        }
        return successBean(mapResponseEntity.getData(), CiipUsrUsers.class);
    }

    /**
     * 保存用户
     *
     * @param ciipUsrUsers ciipUsrUsers
     * @return ResponseEntity&lt;CiipUsrUsers&gt;
     */

    public ResponseEntity<CiipUsrUsers> save(CiipUsrUsers ciipUsrUsers) {
        ResponseEntity<Map<String, Object>> mapResponseEntity = this.postForObject("/user/save", ciipUsrUsers, ResponseEntity.class);
        if (!StringUtils.isEmpty(ciipUsrUsers.getId())) {
            Cache cache = cacheManager.getCache(CacheConstant.USER_CACHE_NAME);
            cache.evict(ciipUsrUsers.getId());
        }
        return successBean(mapResponseEntity.getData(), CiipUsrUsers.class);
    }

    /**
     * 为用户增加默认 'USR' 角色
     *
     * @param userId    用户Id
     * @param roleCodes 角色Code数组
     * @return ResponseEntity&lt;UserRoleCode&gt;
     */
    public ResponseEntity<UserRoleCode> addUserRole(String userId, String... roleCodes) {
        StringBuffer sb = new StringBuffer();
        for (String r : roleCodes) {
            sb.append(r + ",");
        }

        UserRoleCode urlParams = new UserRoleCode();
        urlParams.setUserId(userId);
        urlParams.setRoleCodes(sb.substring(0, sb.length() - 1));
        return successBean((Map<String, Object>) this.postForObject("/user/addDefaultAuth", urlParams, ResponseEntity.class).getData(), UserRoleCode.class);
    }


    /**
     * 保存用户,以及授权
     *
     * @param usrUsers
     * @return
     */
    public ResponseEntity<UsrUsers> saveSummary(@RequestBody UsrUsers usrUsers) {
        ResponseEntity<Map<String, Object>> mapResponseEntity = this.postForObject("/user/saveSummary", usrUsers, ResponseEntity.class);
        if (!StringUtils.isEmpty(usrUsers.getId())) {
            Cache cache = cacheManager.getCache(CacheConstant.USER_CACHE_NAME);
            cache.evict(usrUsers.getId());
        }
        return successBean(mapResponseEntity.getData(), UsrUsers.class);
    }

    public ResponseEntity<CiipUsrUsers> delete(CiipUsrUsers usrUsers) {
        return successBean((Map<String, Object>) this.getForObject(DELETE, ResponseEntity.class, usrUsers).getData(), CiipUsrUsers.class);
    }


    public ResponseEntity approvalShopUser(CiipUsrUsers ciipUsrUsers) {
        return successBean((Map<String, Object>) this.getForObject(processGetUrl("/user/approvalShopUser", ciipUsrUsers), ResponseEntity.class).getData(), ResponseEntity.class);
    }
}
